/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_moto.h"
#include "tethys_laba.h"
#include "tethys_fingerprint.h"
#include "tethys_led.h"
#include "tethys_storage.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"
#include "tethys_battery.h"
#include "tethys_MFRC522.h"

bool gYiKaiSuo = false;
uint32_t gMotoAB = 0;//如果电机两个线焊反了则gMotoAB为1
static uint8_t gMotoOpenLockState = 0;
APP_TIMER_DEF(m_lock_timer_id);
extern uint8_t m_beacon_info[];
extern tethys_open_lock_history_t gOLH;
extern uint8_t gForbidden;
extern void ttp229_close(void);
extern bool isMacAuthorized(void);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
static void lock_timeout_handler(void *p_context);

/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/

void init_Moto(void)
{
    nrf_gpio_cfg_output(MOTO_A);
    nrf_gpio_cfg_output(MOTO_B);
    #if 0
    nrf_gpio_pin_set(MOTO_A);
    nrf_gpio_pin_set(MOTO_B);
    #else
    nrf_gpio_pin_clear(MOTO_A);
    nrf_gpio_pin_clear(MOTO_B);
    #endif
    app_timer_create(&m_lock_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     lock_timeout_handler);
    gMotoOpenLockState = 0;
    gYiKaiSuo = false;
}

static void moto_open_lock_cb(void)
{
    tethys_storage_save_open_lock_history();
}

void moto_open_lock(void * p_event_data, uint16_t event_size)
{
    if (gForbidden == 1)
    {
        playLaBa(LABA_4);
        nrf_delay_ms(500);
        playLaBa(LABA_5);
        return;
    }
    #if 1
    if (isMacAuthorized())
    {
        goto KaiSuoBa;
    }
    #endif
    if (gTethys == 0) // If we still not get the license.
    {
        if (gTethysOpenLockTimes > TETHYS_UN_LICENSE_OPEN_LOCK_TIMES)
        {
            playLaBa(LABA_DING_DONG);
            nrf_delay_ms(100);
            playLaBa(LABA_DING_DONG);
            tethys_storage_save_Open_Lock_Times();
            return;
        }
    }
KaiSuoBa:
    gYiKaiSuo = true;
    gMotoOpenLockState = 0;
    uint32_t *pData = (uint32_t*)p_event_data;
    gOLH.id = pData[0];
    gOLH.type = pData[1];
    gOLH.utc_second = pData[2];
    
    if (gMotoAB == 0)
    {
        nrf_gpio_pin_clear(MOTO_A);
        nrf_gpio_pin_set(MOTO_B);
    }
    else
    {
        nrf_gpio_pin_clear(MOTO_B);
        nrf_gpio_pin_set(MOTO_A);        
    }
    
    tethys_storage_save_Open_Lock_Times();
    app_timer_start(m_lock_timer_id, APP_TIMER_TICKS(498, 0), NULL);
    led_donghua_1(moto_open_lock_cb);
}

static void moto_close_lock(void)
{
    close_LEDS();
    if (gMotoAB == 0)
    {
        nrf_gpio_pin_set(MOTO_A);
        nrf_gpio_pin_clear(MOTO_B);
    }
    else
    {
        nrf_gpio_pin_set(MOTO_B);
        nrf_gpio_pin_clear(MOTO_A);
    }
    app_timer_start(m_lock_timer_id, APP_TIMER_TICKS(498, 0), NULL);
}

static void moto_gaozu_lock(void)
{
    nrf_gpio_pin_set(MOTO_A);
    nrf_gpio_pin_set(MOTO_B);
}

static void moto_shache_lock(void)
{
    nrf_gpio_pin_clear(MOTO_A);
    nrf_gpio_pin_clear(MOTO_B);
    app_timer_start(m_lock_timer_id, APP_TIMER_TICKS(5099, 0), NULL);
    playLaBa(LABA_YI_KAI_SUO);
    close_MFRC522();
    sleep_fingerprint();
}
static void moto_real_close_cb(void)
{
    gYiKaiSuo = false;
    //led_pwm_stop();
    #ifdef TETHYS_PWM
    led_pwm_stop_1();
    #endif
}

static void moto_real_close(void)
{
    ttp229_close();
    nrf_gpio_pin_clear(MOTO_A);
    nrf_gpio_pin_clear(MOTO_B);
    close_MFRC522();
    sleep_fingerprint();
    tethys_battery_calculate();
    playLaBa(LABA_YI_GUAN_SUO);
    led_donghua_1(moto_real_close_cb);
}

static void lock_timeout_handler(void *p_context)
{
    if (gMotoOpenLockState == 0)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)moto_shache_lock);
        gMotoOpenLockState = 1;
    }
    else if (gMotoOpenLockState == 1)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)moto_close_lock);
        gMotoOpenLockState = 2;
    }
    else
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)moto_real_close);
        gMotoOpenLockState = 0;
    }
}

void testMoto(void)
{
}

