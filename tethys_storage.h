#ifndef __TETHYS_STORAGE_H__
#define __TETHYS_STORAGE_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>
#include "pstorage.h"

#define TETHYS_UN_LICENSE_OPEN_LOCK_TIMES 101 //对于未注册的Tethys，只能开锁这么多次数
#define TETHYS_USERS_COUNT 100

typedef enum
{
    TETHYS_PSTORAGE_REGISTER_ADMIN,
    TETHYS_PSTORAGE_REGISTER_USER,
    TETHYS_PSTORAGE_REGISTER_ADMIN_EXTEND,
    TETHYS_PSTORAGE_REGISTER_USER_EXTEND,
    TETHYS_PSTORAGE_REGISTER_ADMIN_FP,
    TETHYS_PSTORAGE_REGISTER_USER_FP,
    TETHYS_PSTORAGE_REGISTER_ADMIN_RF,
    TETHYS_PSTORAGE_REGISTER_USER_RF,
    TETHYS_PSTORAGE_LICENSE,
    TETHYS_PSTORAGE_SAVE_BANGDING,
    TETHYS_PSTORAGE_SAVE_BANGDING_2,
    TETHYS_PSTORAGE_UPDATE_THE_PASSWORD,
    TETHYS_PSTORAGE_DELETE_BANGDING_ID,
    TETHYS_PSTORAGE_FACTORY_RESET,
    TETHYS_PSTORAGE_UPDATE_PEIDUI_PASSWORD,
    TETHYS_PSTORAGE_,
    TETHYS_PSTORAGE_NOTHING,
} TETHYS_PSTORAGE_STATE_t;

typedef void (*delete_one_fingerprint_cb)(uint8_t flag);

typedef struct
{
    uint8_t admin;// 0: admin, 1: common user
    uint8_t mode; // 0: password, 1: fingerprint, 2: card
    uint16_t id;  // 1 ~ TETHYS_USERS_COUNT
    uint8_t content[12];
} tethys_man_t;

typedef struct
{
    uint32_t id;
    uint32_t type; // 1: password, 2: fingerprint, 3: card, 4: bluetooth
    uint32_t utc_second;
} tethys_open_lock_history_t;

extern TETHYS_PSTORAGE_STATE_t g_Tethys_pstorage_state;
extern uint32_t gTethysUserCount;
extern uint16_t gTethysID[TETHYS_USERS_COUNT];
extern tethys_man_t gTethysUsers[TETHYS_USERS_COUNT];
extern uint32_t gTethys;
extern uint32_t gMin;
extern uint32_t gMax;
extern uint32_t gTethysOpenLockTimes;
extern pstorage_handle_t g_storage_handle_system_time;// 记录系统时间
extern pstorage_handle_t g_storage_handle_open_lock_history;// 记录开锁历史数据

/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void tethys_storage_init(void);
extern void tethys_storage_save_open_lock_history(void);
extern int8_t tethys_storage_save(tethys_man_t *pItem);
extern int8_t tethys_storage_if_can_forbidden(void);
extern int8_t tethys_storage_update(tethys_man_t *pItem);
extern int8_t tethys_storage_update_password(uint16_t id, uint8_t *pPassword);
extern int8_t tethys_storage_update_PeiDui_password(void);
extern int8_t tethys_storage_delete(uint16_t id);
extern int8_t tethys_factory_reset(void);
extern int8_t tethys_storage_delete_all_admin(delete_one_fingerprint_cb callback);
extern int8_t tethys_storage_delete_all_common_users(delete_one_fingerprint_cb callback);
extern int8_t tethys_storage_delete_all_common_users_card(void);
extern int8_t tethys_storage_delete_all_common_users_password(void);
extern int8_t tethys_storage_delete_all_common_users_fingerprint(delete_one_fingerprint_cb callback);
extern int16_t tethys_how_many_admin(void);
extern int16_t tethys_how_many_user(void);
extern uint16_t tethys_get_low_admin_id(void);
extern uint16_t tethys_get_low_user_id(void);
extern int8_t tethys_get_password(uint16_t id, uint8_t *buf);
extern int8_t tethys_is_password(uint8_t *buf);
extern uint8_t tethys_get_cur_pass_id(void);
extern int8_t tethys_is_the_password(uint16_t id, uint8_t *buf);
extern int8_t tethys_is_the_password_2(uint8_t len, uint8_t *buf);
extern int8_t tethys_is_admin_password(uint8_t *buf);
extern int8_t tethys_is_admin_fingerprint(uint8_t *buf);
extern int8_t tethys_is_admin_card(uint8_t *buf);
extern int8_t tethys_is_card(uint8_t *buf);
extern uint8_t tethys_get_cur_card_id(void);
extern int8_t tethys_is_number_used(uint16_t id);
extern int8_t tethys_is_admin_number_used(uint16_t id);
extern int8_t tethys_is_common_user_number_used(uint16_t id);
extern int8_t tethys_storage_save_Open_Lock_Times(void);
extern int8_t tethys_storage_save_license(void);
extern int8_t tethys_storage_save_bangding_id(uint8_t *pBuf, uint8_t len);
extern int8_t tethys_storage_delete_bangding_id(void);
extern int8_t tethys_storage_is_bangding(void);
extern int8_t tethys_storage_is_correct_id(uint8_t *pBuf, uint8_t len);
extern uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size);
#endif
