/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "mimas_run_in_ram.h"
#include "mimas_bsp.h"
#include "app_error.h"
#include "nrf_soc.h"
#include "app_scheduler.h"
#include "mimas_config.h"
#include "ble_types.h"
#include "ble_advdata.h"

//#define ADXL362_WORK_LED_ON
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
extern uint8_t m_beacon_info[];
extern uint16_t global_connect_handle;
extern int8_t g_device_tx_power;

extern void mimas_sleep_mode_enter(void);
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    //LOG_ENTER;
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

void mimas_loop(void)
{
    for (;;)
    {
        //LOG("fr%d\t", ti++);
        //LEDS_ON(LEDS_MASK);
        app_sched_execute();
        //nrf_drv_wdt_feed();
        power_manage();
        //LEDS_INVERT(LEDS_MASK);
        //LEDS_OFF(LEDS_MASK);
    }
}

#define NON_CONNECTABLE_ADV_INTERVAL    MSEC_TO_UNITS(100, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */
void advertising_init_beacon(void)
{
    //int8_t        tx_power_level = TX_POWER_LEVEL;
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t rspdata;//scan response data
    uint8_t       flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    ble_advdata_manuf_data_t manuf_specific_data;
    manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;
    manuf_specific_data.data.p_data = (uint8_t *) m_beacon_info;
    manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;

    // Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type             = BLE_ADVDATA_NO_NAME;
    advdata.flags                 = flags;
    //advdata.p_tx_power_level      = &tx_power_level;
    //advdata.include_appearance    = true;
    advdata.p_manuf_specific_data = &manuf_specific_data;

//    m_advertising_mode = BLE_FOREVER;

    memset(&rspdata, 0, sizeof(rspdata));
    rspdata.name_type = BLE_ADVDATA_FULL_NAME;
    rspdata.p_tx_power_level = &g_device_tx_power;
    
    err_code = ble_advdata_set(&advdata, &rspdata);
    APP_ERROR_CHECK(err_code);
#if 0
    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    //m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    m_adv_params.p_peer_addr = NULL;                             // Undirected advertisement.
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = NON_CONNECTABLE_ADV_INTERVAL;
    m_adv_params.timeout     = 0;
#endif
}

#ifdef ADXL362_AS_BUTTON_LONG_PUSH
static uint8_t gAdxl362State = 0;
static void ADXL362_set_default_mode(void)
{
    //printf("set default\r\n");
    ADXL362RegisterWrite(ADXL362_REG_POWER_CTL, 0x00);   // Standby
    uint8_t sendbuf[14] = {
        0xee,// 0x20, ADXL362_REG_THRESH_ACT_L
        0x02,  // 0x21, ADXL362_REG_THRESH_ACT_H : 3g
        0,  // 0x22, ADXL362_REG_TIME_ACT

        38,// 0x23, ADXL362_REG_THRESH_INACT_L
        0,  // 0x24, ADXL362_REG_THRESH_INACT_H
        30, // 0x25, ADXL362_REG_TIME_INACT_L
        0x0,  // 0x26, ADXL362_REG_TIME_INACT_H

        0x0F,// 0x27, ADXL362_REG_ACT_INACT_CTL : Default Mode
        0x00, // 0x28, ADXL362_REG_FIFO_CTL
        0x80, // 0x29, ADXL362_REG_FIFO_SAMPLES

        0x10, // 0x2A, ADXL362_REG_INTMAP1
        0x20, // 0x2B, ADXL362_REG_INTMAP2

        0x80, // 0x2C, ADXL362_REG_FILTER_CTL
        0x2e, // 0x2D, ADXL362_REG_POWER_CTL
    };
    ADXL362BurstWrite(ADXL362_REG_THRESH_ACT_L, 14, sendbuf);
}

static void ADXL362_set_loop_mode(void)
{
    //printf("set loop\r\n");
    ADXL362RegisterWrite(ADXL362_REG_POWER_CTL, 0x00);   // Standby
    uint8_t sendbuf[14] = {
        50,// 0x20, ADXL362_REG_THRESH_ACT_L
        0,  // 0x21, ADXL362_REG_THRESH_ACT_H : 200mg
        0,  // 0x22, ADXL362_REG_TIME_ACT

        38,// 0x23, ADXL362_REG_THRESH_INACT_L
        0,  // 0x24, ADXL362_REG_THRESH_INACT_H
        6, // 0x25, ADXL362_REG_TIME_INACT_L
        0x0,  // 0x26, ADXL362_REG_TIME_INACT_H

        0x3F,// 0x27, ADXL362_REG_ACT_INACT_CTL : Default Mode
        0x00, // 0x28, ADXL362_REG_FIFO_CTL
        0x80, // 0x29, ADXL362_REG_FIFO_SAMPLES

        0x10, // 0x2A, ADXL362_REG_INTMAP1
        0x20, // 0x2B, ADXL362_REG_INTMAP2

        0x80, // 0x2C, ADXL362_REG_FILTER_CTL
        0x2e, // 0x2D, ADXL362_REG_POWER_CTL
    };
    ADXL362BurstWrite(ADXL362_REG_THRESH_ACT_L, 14, sendbuf);
}

static void ADXL362_read_status(void)
{
    unsigned char retValue = ADXL362RegisterRead(ADXL362_REG_STATUS);
    //printf("status: 0x%x, adv:%d,c:\r\n", retValue, gADXL362_adv, gADXL362_adv_count);
    gADXL362_adv_count++;
    if (gADXL362_adv_count > 6)
    {
        if (gADXL362_adv == 1)
        {   
            g_timer_task = 1;
            bsp_indication_set(BSP_INDICATE_USER_STATE_0);
            sd_ble_gap_adv_stop();
            app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(2001, APP_TIMER_PRESCALER), NULL);
        }
    }
}

//Active 
static void ADXL362_int1_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //printf("............int1:pin = %d, action = %d, state=%d\r\n", pin, action,gAdxl362State);
    
    //LEDS_ON(BSP_LED_0_MASK);
    if (gAdxl362State == 0)
    {
        gAdxl362State = 1;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ADXL362_set_default_mode);
        
        m_beacon_info[BEACON_STATE_OFFSET] = 1;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    else
    {
        gAdxl362State++;
        if (gAdxl362State < 2)
        {
            return;
        }
        if (bsp_get_indication() == BSP_INDICATE_USER_STATE_0)
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ADXL362_set_loop_mode);
            return;
        }
        else
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ADXL362_read_status);
        }
        if (gADXL362_adv == 0)
        {
            gADXL362_adv = 1;
            gADXL362_adv_count = 0;
            app_timer_start(m_adxl362_adv_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
        }
    }
}

//Inactive
static void ADXL362_int2_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //printf("............int2:pin = %d, action = %d, state=%d\r\n", pin, action,gAdxl362State);
    LEDS_OFF(BSP_LED_0_MASK);

    if (gAdxl362State >= 1)
    {
        gAdxl362State = 0;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ADXL362_set_loop_mode);
    }
    else
    {
        m_beacon_info[BEACON_STATE_OFFSET] = 0;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
}
#else
//Active 
void ADXL362_int1_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    #ifdef ADXL362_WORK_LED_ON
    LEDS_ON(BSP_LED_0_MASK);
    #endif
    //uint8_t state = m_beacon_info[BEACON_STATE_OFFSET] & ~ACTIVE_INACTIVE_Msk;
    #if 1
    m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] | 
                                         (ACTIVE_INACTIVE_1 << ACTIVE_INACTIVE_Pos);
    #else
    m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] & ~ACTIVE_INACTIVE_Msk;
    #endif
    if (global_connect_handle == BLE_CONN_HANDLE_INVALID)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
}

//Inactive
void ADXL362_int2_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    #ifdef ADXL362_WORK_LED_ON
    LEDS_OFF(BSP_LED_0_MASK);
    #endif
    #if 1
    m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] & ~ACTIVE_INACTIVE_Msk;
    #else
    m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] | 
                                         (ACTIVE_INACTIVE_1 << ACTIVE_INACTIVE_Pos);
    #endif
    if (global_connect_handle == BLE_CONN_HANDLE_INVALID)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)mimas_sleep_mode_enter);
    }
}

void ADXL362_int2_pin_handler_toggle(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //LOG("............int2:pin = %d, action = %d ", pin, action);
    if (nrf_drv_gpiote_in_is_set(pin))
    {
        #ifdef ADXL362_WORK_LED_ON
        LEDS_ON(BSP_LED_0_MASK);
        #endif
        m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] | 
                                         (ACTIVE_INACTIVE_1 << ACTIVE_INACTIVE_Pos);
    }
    else
    {
        #ifdef ADXL362_WORK_LED_ON
        LEDS_OFF(BSP_LED_0_MASK);
        #endif
        m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] & ~ACTIVE_INACTIVE_Msk;
    }
    if (global_connect_handle == BLE_CONN_HANDLE_INVALID)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
}

#endif

