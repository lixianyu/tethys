/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_storage.h"
#include "tethys_laba.h"
#include "tethys_fingerprint.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_timer.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "nrf_drv_rng.h"
//#include "mimas_run_in_ram.h"
#include "config.h"
#include "mimas_log.h"
#include "mimas_ble_nus.h"
#include "tethys_ble_nus.h"
#include "mimas_config.h"
#include "bd_wall_clock_timer.h"

extern void save_wall_clock_just(void);
extern void advertising_init_beacon(void);

static void tethys_init_open_lock_times(void);
static void tethys_storage_save_open_lock_history_pos(void);
static void tethys_init_open_lock_history_pos(void);
static void tethys_storage_timer_handler(void *p_context);
static void tethys_handle_pstorage_update(uint32_t result);
static void tethys_handle_pstorage_update0(uint32_t result);
static void tethys_handle_pstorage_update1(uint32_t result);
static void tethys_handle_pstorage_update2(uint32_t result);
static void tethys_handle_pstorage_update3(uint32_t result);
static void tethys_handle_pstorage_update4(uint32_t result);
static void tethys_handle_pstorage_update5(uint32_t result);
static void tethys_handle_pstorage_update_open_lock_history(uint32_t result);
static void tethys_handle_pstorage_update_open_lock_history_pos(uint32_t result);
static void tethys_handle_pstorage_update_system_time_etc(uint32_t result);
static void tethys_handle_pstorage_update_open_lock_times(uint32_t result);
static void tethys_handle_pstorage_update_license(uint32_t result);
static void tethys_handle_pstorage_update_Elara(uint32_t result);


static void tethys_pstorage_callback_handler1(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler2(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler3(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler4(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler5(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_open_lock_history_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_open_lock_history_pos_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_system_time_etc_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler0(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_license_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_open_lock_times_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void tethys_pstorage_Elara_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
APP_TIMER_DEF(m_tethys_storage_time_id);
delete_one_fingerprint_cb gDeleteCB;
TETHYS_PSTORAGE_STATE_t g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
uint16_t g_ID;
pstorage_handle_t g_storage_lock_handle; //记录当前用户数
pstorage_handle_t g_storage_lock_handle0;//记录所有用户编号

//下面5个，记录所有用户的存储信息
pstorage_handle_t g_storage_lock_handle1;
pstorage_handle_t g_storage_lock_handle2;
pstorage_handle_t g_storage_lock_handle3;
pstorage_handle_t g_storage_lock_handle4;
pstorage_handle_t g_storage_lock_handle5;

pstorage_handle_t g_storage_handle_open_lock_history;// 记录开锁历史数据
pstorage_handle_t g_storage_handle_open_lock_history_pos;
pstorage_handle_t g_storage_handle_system_time;// 记录系统时间等等......

//下面3个，在OTA升级之后不会被bootloader删除
pstorage_handle_t g_storage_license_handle;
pstorage_handle_t g_storage_open_lock_times_handle;
pstorage_handle_t g_storage_Elara_handle; // reserve

//记住当前开锁历史记录的位置
uint32_t gOpenLockHistoryPos = 0;
static uint16_t gBlockNumberOpenLockHistoryPos = 0;
static uint16_t gOffsetNumberOpenLockHistoryPos = 0;
tethys_open_lock_history_t gOLH;
    
//当前用户数，1 ~ TETHYS_USERS_COUNT
uint32_t gTethysUserCount = 0;
//记录所有用户编号
uint16_t gTethysID[TETHYS_USERS_COUNT];

/*
 * 记录所有用户的存储信息
*/
tethys_man_t gTethysUsers[TETHYS_USERS_COUNT];

uint8_t gBangdingBuf[32];//记录绑定id

#define TETHYS_BLOCK_SIZE PSTORAGE_MIN_BLOCK_SIZE
#define TETHYS_BLOCK_COUNT 64

/*
 * The block number for 'g_storage_lock_handle'
 * Every block size is 256.
 * And there are 4 blocks. 
 */
#define BLOCK_NUMBER_HOW_MANY_USERS 0  //记录当前用户数
//#define BLOCK_NUMBER_LICENSE 1 //gTethys保存在这里
//#define BLOCK_NUMBER_OPEN_LOCK_TIMES 2 //开锁次数
#define BLOCK_NUMBER_BANG_DING_ID 3 //绑定id

#define OFFSET_YIHUO 28

/*Offset of block 4(BLOCK_NUMBER_BANG_DING_ID)
 * 把256再分成8份，每份32字节
 */
#define OFFSET_BANG_DING 0  // 16 bytes
#define OFFSET_PEI_DUI   32  // 6 bytes
#define OFFSET_FORBIDDEN   64  // 4 bytes

uint8_t p_buf252[256];
/*
 * 每次给客户提供firmware时，gMin和gMax都要做调整，(gMax - gMin + 1)是客户可注册的license数量。
 * 同时，服务器需添加以gMax为文件名的xml文件。比如gMax等于100，则文件名是100.xml，文件内容为：
   <?xml version="1.0" encoding="UTF-8"?>

   <tethys>

   <min>1</min>

   <max>100</max>

   <cur>1</cur>

   </tethys>
 * 客户通过App注册时，会得到100.xml文件中的cur值用于license值。若cur大于gMax时，则无法再注册了。
 * App会通过蓝牙把cur传给firmware，保存在gTethys变量中，并保存在pstorage里。然后App会把cur加1，再传回服务器。
 * xml文件中的min、max分别对应gMin、gMax变量。
 * gMin、gMax不保存在pstorage里。
 * 如果gTethys变量为0，则每开锁一次，gTethysOpenLockTimes加1，当开锁次数大于TETHYS_UN_LICENSE_OPEN_LOCK_TIMES时，
 * 则无法再开锁。gTethysOpenLockTimes需保存到pstorage中。
 */
uint32_t gMin = 1;
uint32_t gMax = 1000;
uint32_t gTethys = 0; // This is license. gMin <= gTethys <= gMax
uint32_t gTethysOpenLockTimes = 0; //当前已开锁次数
static uint16_t gBlockNumberOpenLockTimes = 0;
static uint16_t gOffsetNumberOpenLockTimes = 0;

/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
#define RANDOM_BUFF_SIZE 252 /**< Random numbers buffer size. */
void tethys_storage_init(void)
{
    app_timer_create(&m_tethys_storage_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     tethys_storage_timer_handler);

    pstorage_module_param_t param;
    param.block_size  = TETHYS_BLOCK_SIZE; //16 bytes
    param.block_count = TETHYS_BLOCK_COUNT;

    param.cb          = tethys_pstorage_callback_handler1;
    uint32_t err_code = pstorage_register(&param, &g_storage_lock_handle1);
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_callback_handler2;
    err_code = pstorage_register(&param, &g_storage_lock_handle2);
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_callback_handler3;
    err_code = pstorage_register(&param, &g_storage_lock_handle3);
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_callback_handler4;
    err_code = pstorage_register(&param, &g_storage_lock_handle4);
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_callback_handler5;
    err_code = pstorage_register(&param, &g_storage_lock_handle5);
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_open_lock_history_callback_handler;
    err_code = pstorage_register(&param, &g_storage_handle_open_lock_history);
    APP_ERROR_CHECK(err_code);
    param.cb = tethys_pstorage_open_lock_history_pos_callback_handler;
    err_code = pstorage_register(&param, &g_storage_handle_open_lock_history_pos);
    APP_ERROR_CHECK(err_code);
    param.cb = tethys_pstorage_system_time_etc_callback_handler;
    err_code = pstorage_register(&param, &g_storage_handle_system_time);
    APP_ERROR_CHECK(err_code);
    
    param.block_size  = 1024;//4
    param.block_count = 1;
    param.cb = tethys_pstorage_callback_handler0;
    err_code = pstorage_register(&param, &g_storage_lock_handle0);//记录所有用户编号
    APP_ERROR_CHECK(err_code);

    param.block_size  = 256;//4
    param.block_count = 4;
    param.cb = tethys_pstorage_callback_handler;
    err_code = pstorage_register(&param, &g_storage_lock_handle); //记录当前用户数
    APP_ERROR_CHECK(err_code);

    param.cb = tethys_pstorage_Elara_callback_handler;//留着备用
    err_code = pstorage_register(&param, &g_storage_Elara_handle);
    APP_ERROR_CHECK(err_code);
    
    param.block_size  = TETHYS_BLOCK_SIZE; //16 bytes
    param.block_count = TETHYS_BLOCK_COUNT;
    param.cb = tethys_pstorage_open_lock_times_callback_handler; // Tethys 出厂后开锁次数
    err_code = pstorage_register(&param, &g_storage_open_lock_times_handle);
    APP_ERROR_CHECK(err_code);

    param.block_size  = 256;//4
    param.block_count = 4;
    param.cb = tethys_pstorage_license_callback_handler;// License存储在这里
    err_code = pstorage_register(&param, &g_storage_license_handle);
    APP_ERROR_CHECK(err_code);

    uint32_t userCount = 0;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_load((uint8_t *)&userCount, &block_handle, 4, 0);
    if (userCount != 0xFFFFFFFF && userCount != 0)
    {
        gTethysUserCount = userCount;
        pstorage_size_t size = gTethysUserCount * 2;
        if ((size % 4) != 0)
        {
            size += 2;
        }
        pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
        pstorage_load((uint8_t *)gTethysID, &block_handle, size, 0);

        size = sizeof(tethys_man_t); // 16 bytes.
        for (int i = 0; i < gTethysUserCount; i++)
        {
            uint16_t curBlockNum = gTethysID[i] - 1;
            if (curBlockNum < 64)
            {
                pstorage_block_identifier_get(&g_storage_lock_handle1, curBlockNum, &block_handle);
            }
            else if (curBlockNum < 128)
            {
                pstorage_block_identifier_get(&g_storage_lock_handle2, curBlockNum - 64, &block_handle);
            }
            else if (curBlockNum < 192)
            {
                pstorage_block_identifier_get(&g_storage_lock_handle3, curBlockNum - 128, &block_handle);
            }
            else if (curBlockNum < 256)
            {
                pstorage_block_identifier_get(&g_storage_lock_handle4, curBlockNum - 192, &block_handle);
            }
            else if (curBlockNum < 320)
            {
                pstorage_block_identifier_get(&g_storage_lock_handle5, curBlockNum - 256, &block_handle);
            }
            pstorage_load((uint8_t *)&gTethysUsers[i], &block_handle, size, 0);
        }
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_BANG_DING_ID, &block_handle);
    pstorage_load(gBangdingBuf, &block_handle, 16, OFFSET_BANG_DING);
#ifdef TETHYS_PAIR
    pstorage_load(gPasskey, &block_handle, 6, OFFSET_PEI_DUI);
    uint8_t bufFF[6];
    memset(bufFF, 0xFF, 6);
    if (memcmp(bufFF, gPasskey, 6) == 0)
    {
        memcpy(gPasskey, "123456", 6);
    }
#endif
#if 0
    uint32_t OpenLockTimes = 0;
    pstorage_block_identifier_get(&g_storage_open_lock_times_handle, 0, &block_handle);
    pstorage_load((uint8_t *)&OpenLockTimes, &block_handle, 4, 0);
    if (OpenLockTimes != 0xFFFFFFFF)
    {
        gTethysOpenLockTimes = OpenLockTimes;
    }
    else
    {
        gTethysOpenLockTimes = 0;
    }
#else
    tethys_init_open_lock_times();
#endif

    uint32_t uLicense = 0;
    pstorage_block_identifier_get(&g_storage_license_handle, 0, &block_handle);
    pstorage_load((uint8_t *)&uLicense, &block_handle, 4, 0);
    if (uLicense != 0xFFFFFFFF)
    {
        uint32_t yihuo = 0;
        pstorage_load((uint8_t *)&yihuo, &block_handle, 4, OFFSET_YIHUO);
        gTethys = (uLicense ^ yihuo) - 0x05F5E100;//一亿我们不用了
    }
    else
    {
        gTethys = 0;
    }

    uint32_t uRandom = 0xA014FFB7;
    pstorage_load((uint8_t *)&uRandom, &block_handle, 4, 4);
    if (uRandom == 0xFFFFFFFF)
    {
        nrf_drv_rng_init(NULL);
        uint8_t p_buff[RANDOM_BUFF_SIZE];
        uint8_t length = 0;
        uint16_t idx = 0;
        while (true)
        {
            length = random_vector_generate(p_buff, RANDOM_BUFF_SIZE);
            if (idx + length <= 252)
            {
                memcpy(p_buf252 + idx + 4, p_buff, length);
            }
            else
            {
                uint8_t realCopyLen = 252 - idx;
                memcpy(p_buf252 + idx + 4, p_buff, realCopyLen);
                break;
            }
            idx += length;
        }
        p_buf252[4] = 0xA0;
        p_buf252[5] = 0x14;
        p_buf252[6] = 0xFF;
        p_buf252[7] = 0xB7;
        uint32_t license = 0x05F5E100;//一亿我们不用了
        #if 0
        uint32_t *pYH = (uint32_t*)&p_buf252[OFFSET_YIHUO];
        license = license ^ (*pYH);
        p_buf252[0] = license && 0xFF;
        p_buf252[1] = (license >> 8) && 0xFF;
        p_buf252[2] = (license >> 16) && 0xFF;
        p_buf252[3] = (license >> 24) && 0xFF;
        #else
        p_buf252[0] = (license & 0xFF) ^ p_buf252[OFFSET_YIHUO];
        p_buf252[1] = ((license >> 8) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 1];
        p_buf252[2] = ((license >> 16) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 2];
        p_buf252[3] = ((license >> 24) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 3];
        #endif
        pstorage_block_identifier_get(&g_storage_license_handle, 0, &block_handle);
        pstorage_store(&block_handle, p_buf252, 256, 0);
        nrf_drv_rng_uninit();
    }

    tethys_init_open_lock_history_pos();
}

static void tethys_init_open_lock_history_pos(void)
{
    pstorage_handle_t block_handle;
    uint32_t utcSecond = 0;
    uint16_t blockNB;
    uint16_t offsetNB;
    for (blockNB = 0; blockNB < 64; blockNB++)
    {
        for (offsetNB = 0; offsetNB < 16; offsetNB+=4)
        {
            pstorage_block_identifier_get(&g_storage_handle_open_lock_history_pos, blockNB, &block_handle);
            pstorage_load((uint8_t *)&utcSecond, &block_handle, 4, offsetNB);
            if (utcSecond == 0xFFFFFFFF)
            {
                goto OHLeaveMe;
            }
        }
    }
OHLeaveMe:
    gBlockNumberOpenLockHistoryPos = blockNB;
    gOffsetNumberOpenLockHistoryPos = offsetNB;
    if (blockNB == 0 && offsetNB == 0)
    {
        gOpenLockHistoryPos = 0;
    }
    else
    {
        if (offsetNB == 0)
        {
            blockNB--;
            offsetNB = 12;
        }
        else
        {
            offsetNB -= 4;
        }
        pstorage_block_identifier_get(&g_storage_handle_open_lock_history_pos, blockNB, &block_handle);
        pstorage_load((uint8_t *)&gOpenLockHistoryPos, &block_handle, 4, offsetNB);
    }
}

//void tethys_storage_save_open_lock_history(uint32_t id, uint32_t type, uint32_t utcSecond)
void tethys_storage_save_open_lock_history(void)
{
    #if 0
    gOLH.id = id;
    gOLH.type = type;
    gOLH.utc_second = utcSecond;
    #endif
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_open_lock_history, gOpenLockHistoryPos, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gOLH, sizeof(gOLH), 0);

    gOpenLockHistoryPos++;
    if (gOpenLockHistoryPos > 63)
    {
        gOpenLockHistoryPos = 0;
    }
    tethys_storage_save_open_lock_history_pos();
}

static void tethys_storage_save_open_lock_history_pos(void)
{
    pstorage_handle_t block_handle;
    if (gBlockNumberOpenLockHistoryPos == 63 && gOffsetNumberOpenLockHistoryPos == 12)
    {
        pstorage_clear(&g_storage_handle_open_lock_history_pos, 1024);
        return;
    }
    pstorage_block_identifier_get(&g_storage_handle_open_lock_history_pos, gBlockNumberOpenLockHistoryPos, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&gOpenLockHistoryPos, 4, gOffsetNumberOpenLockHistoryPos);
    gOffsetNumberOpenLockHistoryPos += 4;
    if (gOffsetNumberOpenLockHistoryPos > 12)
    {
        gOffsetNumberOpenLockHistoryPos = 0;
        gBlockNumberOpenLockHistoryPos++;
    }
}

static void tethys_storage_save_open_lock_history_pos_just(void)
{
    pstorage_handle_t block_handle;
    gBlockNumberOpenLockHistoryPos = 0;
    gOffsetNumberOpenLockHistoryPos = 0;
    pstorage_block_identifier_get(&g_storage_handle_open_lock_history_pos, gBlockNumberOpenLockHistoryPos, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&gOpenLockHistoryPos, 4, gOffsetNumberOpenLockHistoryPos);
    gOffsetNumberOpenLockHistoryPos += 4;
    if (gOffsetNumberOpenLockHistoryPos > 12)
    {
        gOffsetNumberOpenLockHistoryPos = 0;
        gBlockNumberOpenLockHistoryPos++;
    }
}

int8_t tethys_storage_save_license(void)
{
    pstorage_handle_t block_handle;
    uint32_t saveLicense = 0x05F5E100;//一亿我们不用了
    
    nrf_drv_rng_init(NULL);
    uint8_t p_buff[RANDOM_BUFF_SIZE];
    uint8_t length = 0;
    uint16_t idx = 0;
    while (true)
    {
        length = random_vector_generate(p_buff, RANDOM_BUFF_SIZE);
        if (idx + length <= 252)
        {
            memcpy(p_buf252 + idx + 4, p_buff, length);
        }
        else
        {
            uint8_t realCopyLen = 252 - idx;
            memcpy(p_buf252 + idx + 4, p_buff, realCopyLen);
            break;
        }
        idx += length;
    }
    p_buf252[4] = 0xA0;
    p_buf252[5] = 0x14;
    p_buf252[6] = 0xFF;
    p_buf252[7] = 0xB7;
    saveLicense += gTethys;
    p_buf252[0] = (saveLicense & 0xFF) ^ p_buf252[OFFSET_YIHUO];
    p_buf252[1] = ((saveLicense >> 8) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 1];
    p_buf252[2] = ((saveLicense >> 16) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 2];
    p_buf252[3] = ((saveLicense >> 24) & 0xFF) ^ p_buf252[OFFSET_YIHUO + 3];
    pstorage_block_identifier_get(&g_storage_license_handle, 0, &block_handle);
    g_Tethys_pstorage_state = TETHYS_PSTORAGE_LICENSE;
    pstorage_update(&block_handle, p_buf252, 256, 0);
    nrf_drv_rng_uninit();
    return 0;
}

static void tethys_init_open_lock_times(void)
{
    pstorage_handle_t block_handle;
    uint32_t openLockTimes = 0;
    uint16_t blockNB;
    uint16_t offsetNB;
    for (blockNB = 0; blockNB < 64; blockNB++)
    {
        for (offsetNB = 0; offsetNB < 16; offsetNB+=4)
        {
            pstorage_block_identifier_get(&g_storage_open_lock_times_handle, blockNB, &block_handle);
            pstorage_load((uint8_t *)&openLockTimes, &block_handle, 4, offsetNB);
            if (openLockTimes == 0xFFFFFFFF)
            {
                goto OHLeaveMe;
            }
        }
    }
OHLeaveMe:
    gBlockNumberOpenLockTimes = blockNB;
    gOffsetNumberOpenLockTimes = offsetNB;
    if (blockNB == 0 && offsetNB == 0)
    {
        gTethysOpenLockTimes = 0;
    }
    else
    {
        if (offsetNB == 0)
        {
            blockNB--;
            offsetNB = 12;
        }
        else
        {
            offsetNB -= 4;
        }
        pstorage_block_identifier_get(&g_storage_open_lock_times_handle, blockNB, &block_handle);
        pstorage_load((uint8_t *)&gTethysOpenLockTimes, &block_handle, 4, offsetNB);
    }
}

int8_t tethys_storage_save_Open_Lock_Times(void)
{
    pstorage_handle_t block_handle;
    gTethysOpenLockTimes++;

    if (gBlockNumberOpenLockTimes == 63 && gOffsetNumberOpenLockTimes == 12)
    {
        pstorage_clear(&g_storage_open_lock_times_handle, 1024);
        return 1;
    }
    pstorage_block_identifier_get(&g_storage_open_lock_times_handle, gBlockNumberOpenLockTimes, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&gTethysOpenLockTimes, 4, gOffsetNumberOpenLockTimes);
    gOffsetNumberOpenLockTimes += 4;
    if (gOffsetNumberOpenLockTimes > 12)
    {
        gOffsetNumberOpenLockTimes = 0;
        gBlockNumberOpenLockTimes++;
    }
    return 0;
}

static void tethys_storage_save_open_lock_times_just(void)
{
    pstorage_handle_t block_handle;
    gBlockNumberOpenLockTimes = 0;
    gOffsetNumberOpenLockTimes = 0;
    pstorage_block_identifier_get(&g_storage_open_lock_times_handle, gBlockNumberOpenLockTimes, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&gTethysOpenLockTimes, 4, gOffsetNumberOpenLockTimes);
    gOffsetNumberOpenLockTimes += 4;
    if (gOffsetNumberOpenLockTimes > 12)
    {
        gOffsetNumberOpenLockTimes = 0;
        gBlockNumberOpenLockTimes++;
    }
}

int8_t tethys_storage_save_bangding_id(uint8_t *pBuf, uint8_t len)
{
    pstorage_handle_t block_handle;
    memcpy(gBangdingBuf, pBuf, 16);
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_BANG_DING_ID, &block_handle);
    pstorage_update(&block_handle, gBangdingBuf, 16, OFFSET_BANG_DING);
    return 0;
}

int8_t tethys_storage_delete_bangding_id(void)
{
    g_Tethys_pstorage_state = TETHYS_PSTORAGE_DELETE_BANGDING_ID;
    pstorage_handle_t block_handle;
    memset(gBangdingBuf, 0xFF, 16);
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_BANG_DING_ID, &block_handle);
    pstorage_update(&block_handle, gBangdingBuf, 16, OFFSET_BANG_DING);
    return 0;
}

// return 1, 设备未绑定
int8_t tethys_storage_is_bangding(void)
{
    uint8_t bufFF[32];
    memset(bufFF, 0xFF, 16);
    if (memcmp(gBangdingBuf, bufFF, 16) == 0)
    {
        return 1;
    }
    return 0;
}

// return 0, It is the correct binding id.
int8_t tethys_storage_is_correct_id(uint8_t *pBuf, uint8_t len)
{
    if (memcmp(gBangdingBuf, pBuf, 16) == 0)
    {
        return 0;
    }
    return 1;
}

int8_t tethys_storage_save(tethys_man_t *pItem)
{
    gTethysID[gTethysUserCount] = pItem->id;
    gTethysUsers[gTethysUserCount].admin = pItem->admin;
    gTethysUsers[gTethysUserCount].mode = pItem->mode;
    gTethysUsers[gTethysUserCount].id = pItem->id;
    memcpy(gTethysUsers[gTethysUserCount].content, pItem->content, sizeof(pItem->content));

    gTethysUserCount++;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);

    uint16_t curBlockNum = pItem->id - 1;
    if (curBlockNum < 64)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle1, curBlockNum, &block_handle);
    }
    else if (curBlockNum < 128)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle2, curBlockNum - 64, &block_handle);
    }
    else if (curBlockNum < 192)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle3, curBlockNum - 128, &block_handle);
    }
    else if (curBlockNum < 256)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle4, curBlockNum - 192, &block_handle);
    }
    else if (curBlockNum < 320)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle5, curBlockNum - 256, &block_handle);
    }
    pstorage_update(&block_handle, (uint8_t *)&gTethysUsers[gTethysUserCount - 1], sizeof(tethys_man_t), 0);
    return 0;
}

int8_t tethys_storage_update_password(uint16_t id, uint8_t *pPassword)
{
    g_ID = id;
    g_Tethys_pstorage_state = TETHYS_PSTORAGE_UPDATE_THE_PASSWORD;
    tethys_man_t aman;
    memset(&aman, 0, sizeof(aman));
    aman.admin = id < 10 ? 0 : 1;
    aman.mode = 0;
    aman.id = id;
    memcpy(aman.content, pPassword, 6);
    tethys_storage_update(&aman);
    return 0;
}

#ifdef TETHYS_PAIR
int8_t tethys_storage_update_PeiDui_password(void)
{
    g_Tethys_pstorage_state = TETHYS_PSTORAGE_UPDATE_PEIDUI_PASSWORD;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_BANG_DING_ID, &block_handle);
    pstorage_update(&block_handle, gPasskey, 6, OFFSET_PEI_DUI);
    return 0;
}
#endif

// return 0: can forbidden
int8_t tethys_storage_if_can_forbidden(void)
{
    uint32_t uiForbidden = 0;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_BANG_DING_ID, &block_handle);
    pstorage_load((uint8_t*)&uiForbidden, &block_handle, 4, OFFSET_FORBIDDEN);
    if (uiForbidden == 0xFFFFFFFF)
    {
        return 0;
    }
    return 1;
}

int8_t tethys_storage_update(tethys_man_t *pItem)
{
    pstorage_handle_t block_handle;
    int i;
    for (i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].id == pItem->id)
        {
            gTethysUsers[i].id = pItem->id;
            gTethysUsers[i].admin = pItem->admin;
            gTethysUsers[i].mode = pItem->mode;
            memcpy(gTethysUsers[i].content, pItem->content, sizeof(pItem->content));
            break;
        }
    }
    if (i >= gTethysUserCount)
    {
        return -1;
    }

    uint16_t curBlockNum = pItem->id - 1;
    if (curBlockNum < 64)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle1, curBlockNum, &block_handle);
    }
    else if (curBlockNum < 128)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle2, curBlockNum - 64, &block_handle);
    }
    else if (curBlockNum < 192)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle3, curBlockNum - 128, &block_handle);
    }
    else if (curBlockNum < 256)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle4, curBlockNum - 192, &block_handle);
    }
    else if (curBlockNum < 320)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle5, curBlockNum - 256, &block_handle);
    }
    pstorage_update(&block_handle, (uint8_t *)&gTethysUsers[i], sizeof(tethys_man_t), 0);
    return 0;
}

int8_t tethys_storage_delete(uint16_t id)
{
    pstorage_handle_t block_handle;
    int i, j;
    bool isFingerprint = false;
    for (i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].id == id)
        {
            if (gTethysUsers[i].mode == 1)
            {
                isFingerprint = true;
            }
            break;
        }
    }
    if (i >= gTethysUserCount)
    {
        return -1;
    }
    if (isFingerprint)
    {
        _FG_REQ_ERASE_ONE(id, NULL);
        nrf_delay_ms(300);
    }
    if (i == (gTethysUserCount - 1)) // The last one
    {//既然是最后一个，gTethysID就不需保存了，因为下次系统重启时，只会取gTethysUserCount个出来
        gTethysID[i] = 0xFFFF;
        gTethysUserCount--;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);
        return 0;
    }
    for (j = i + 1; j < gTethysUserCount; j++,i++)
    {
        memcpy(&gTethysUsers[i], &gTethysUsers[j], sizeof(tethys_man_t));
        gTethysID[i] = gTethysID[j];
    }

    gTethysUserCount--;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    if (gTethysUserCount != 0)
    {
        pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
        pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
        if ((size % 4) != 0)
        {
            size += 2;
        }
        pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);
    }
    return 1;
}

static uint8_t gDeleteFingerprintCur = 0;
static uint8_t gDeleteFingerprintCounts = 0;
static uint16_t TethysID_FP[TETHYS_USERS_COUNT];
int8_t tethys_factory_reset(void)
{
    led_DH_factory_reset_start();
    g_Tethys_pstorage_state = TETHYS_PSTORAGE_FACTORY_RESET;
    pstorage_handle_t block_handle;
    gTethysUserCount = 0;
    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);
    //In function 'tethys_handle_pstorage_update', will delete all fingerprint.
    return 0;
}

int8_t tethys_storage_delete_all_admin(delete_one_fingerprint_cb callback)
{
    pstorage_handle_t block_handle;
    if (gTethysUserCount == 0)
    {
        return 0;
    }
    gDeleteCB = callback;

    gDeleteFingerprintCounts = 0;
    uint16_t TethysID[gTethysUserCount];
    tethys_man_t TethysUsers[gTethysUserCount];
    int j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 1)//找到所有普通用户
        {
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].mode == 1)//所有管理员中的指纹
        {
            TethysID_FP[gDeleteFingerprintCounts++] = gTethysID[i];
        }
    }
    if (j == 0)//说明没有普通用户
    {
        gTethysUserCount = 0;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

        // Let's begin delete the fingerprint one by one.
        if (gDeleteFingerprintCounts != 0)
        {
            nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
            gDeleteFingerprintCur = 0;
            app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
            return 1;
        }
        return 0;
    }
    if (j == gTethysUserCount)//说明全是普通用户，还尚未录入管理员
    {
        return 0;
    }

    gTethysUserCount = j;//Common users.
    j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        gTethysID[i] = TethysID[j];
        memcpy(&gTethysUsers[i] ,&TethysUsers[j], sizeof(tethys_man_t));
        j++;
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);

    // Let's begin delete the fingerprint one by one.
    if (gDeleteFingerprintCounts != 0)
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
        gDeleteFingerprintCur = 0;
        app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
        return 1;
    }
    return 0;
}

int8_t tethys_storage_delete_all_common_users(delete_one_fingerprint_cb callback)
{
    pstorage_handle_t block_handle;
    if (gTethysUserCount == 0)
    {
        return 0;
    }
    gDeleteCB = callback;

    gDeleteFingerprintCounts = 0;
    uint16_t TethysID[gTethysUserCount];
    tethys_man_t TethysUsers[gTethysUserCount];
    int j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0)//先找到所有管理员
        {
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].mode == 1)//所有普通用户中的指纹
        {
            TethysID_FP[gDeleteFingerprintCounts++] = gTethysID[i];
        }
    }
    if (j == 0)//说明没有管理员
    {
        gTethysUserCount = 0;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

        // Let's begin delete the fingerprint one by one.
        if (gDeleteFingerprintCounts != 0)
        {
            nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
            gDeleteFingerprintCur = 0;
            app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
            return 1;
        }
        return 0;
    }
    if (j == gTethysUserCount)//说明全是管理员
    {
        return 0;
    }

    gTethysUserCount = j;//admin users.
    j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        gTethysID[i] = TethysID[j];
        memcpy(&gTethysUsers[i] ,&TethysUsers[j], sizeof(tethys_man_t));
        j++;
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);

    // Let's begin delete the fingerprint one by one.
    if (gDeleteFingerprintCounts != 0)
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
        gDeleteFingerprintCur = 0;
        app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
        return 1;
    }
    return 0;
}

int8_t tethys_storage_delete_all_common_users_card(void)
{
    pstorage_handle_t block_handle;
    if (gTethysUserCount == 0)
    {
        return 0;
    }
    uint16_t TethysID[gTethysUserCount];
    tethys_man_t TethysUsers[gTethysUserCount];
    int j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0)//所有的管理员
        {
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].admin == 1 && (gTethysUsers[i].mode == 0 || gTethysUsers[i].mode == 1))
        {//所有普通用户里面的非卡片用户
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
    }
    if (j == 0)//说明没有管理员、普通用户的密码、指纹用户
    {
        gTethysUserCount = 0;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);
        return 0;
    }
    if (j == gTethysUserCount)//说明普通用户里没有卡片用户
    {
        return 0;
    }

    gTethysUserCount = j; //除去card普通用户之外的用户数
    j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        gTethysID[i] = TethysID[j];
        memcpy(&gTethysUsers[i] ,&TethysUsers[j], sizeof(tethys_man_t));
        j++;
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);
    return 0;
}

int8_t tethys_storage_delete_all_common_users_password(void)
{
    pstorage_handle_t block_handle;
    if (gTethysUserCount == 0)
    {
        return 0;
    }
    uint16_t TethysID[gTethysUserCount];
    tethys_man_t TethysUsers[gTethysUserCount];
    int j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0)//所有的管理员
        {
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].admin == 1 && (gTethysUsers[i].mode == 1 || gTethysUsers[i].mode == 2))
        {//所有普通用户里面的非密码用户
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
    }
    if (j == 0)//说明没有管理员、普通用户指纹、卡片用户
    {
        gTethysUserCount = 0;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);
        return 0;
    }
    if (j == gTethysUserCount)//说明普通用户里没有密码用户
    {
        return 0;
    }

    gTethysUserCount = j; //除去password普通用户之外的用户数
    j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        gTethysID[i] = TethysID[j];
        memcpy(&gTethysUsers[i] ,&TethysUsers[j], sizeof(tethys_man_t));
        j++;
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);
    return 0;
}

int8_t tethys_storage_delete_all_common_users_fingerprint(delete_one_fingerprint_cb callback)
{
    pstorage_handle_t block_handle;
    if (gTethysUserCount == 0)
    {
        return 0;
    }
    gDeleteCB = callback;

    gDeleteFingerprintCounts = 0;
    uint16_t TethysID[gTethysUserCount];
    tethys_man_t TethysUsers[gTethysUserCount];
    int j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0)//所有的管理员
        {
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].admin == 1 && (gTethysUsers[i].mode == 0 || gTethysUsers[i].mode == 2))
        {//所有普通用户里面的非指纹用户
            TethysID[j] = gTethysID[i];
            memcpy(&TethysUsers[j], &gTethysUsers[i], sizeof(tethys_man_t));
            j++;
        }
        else if (gTethysUsers[i].admin == 1 && gTethysUsers[i].mode == 1)//所有普通用户中的指纹
        {
            TethysID_FP[gDeleteFingerprintCounts++] = gTethysID[i];
        }
    }
    
    if (j == 0)//说明没有管理员、普通用户的密码、卡片用户
    {
        gTethysUserCount = 0;
        pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
        pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

        // Let's begin delete the fingerprint one by one.
        if (gDeleteFingerprintCounts != 0)
        {
            nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
            gDeleteFingerprintCur = 0;
            app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
            return 1;
        }
        return 0;
    }
    if (j == gTethysUserCount)//说明普通用户里没有指纹用户
    {
        return 0;
    }

    gTethysUserCount = j; //除去fingerprint普通用户之外的用户数
    j = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        gTethysID[i] = TethysID[j];
        memcpy(&gTethysUsers[i] ,&TethysUsers[j], sizeof(tethys_man_t));
        j++;
    }

    pstorage_block_identifier_get(&g_storage_lock_handle, BLOCK_NUMBER_HOW_MANY_USERS, &block_handle);
    pstorage_update(&block_handle, (uint8_t *)&gTethysUserCount, 4, 0);

    pstorage_block_identifier_get(&g_storage_lock_handle0, 0, &block_handle);
    pstorage_size_t size = gTethysUserCount * sizeof(pstorage_size_t);
    if ((size % 4) != 0)
    {
        size += 2;
    }
    pstorage_update(&block_handle, (uint8_t *)gTethysID, size, 0);

    // Let's begin delete the fingerprint one by one.
    if (gDeleteFingerprintCounts != 0)
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
        gDeleteFingerprintCur = 0;
        app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(1100, 0), NULL);
        return 1;
    }
    return 0;
}

int16_t tethys_how_many_admin(void)
{
    int16_t cot = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0)
        {
            cot++;
        }
    }
    return cot;
}

int16_t tethys_how_many_user(void)
{
    int16_t cot = 0;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 1)
        {
            cot++;
        }
    }
    return cot;
}

uint16_t tethys_get_low_admin_id(void)
{
    bool flag = false;
    for (uint16_t id = 1; id <= 9; id++)
    {
        flag = false;
        for (int i = 0; i < gTethysUserCount; i++)
        {
            if (id == gTethysUsers[i].id)
            {
                flag = true;
                break;
            }
        }
        if (flag == false)
        {
            return id;
        }
    }
    return 0;
}

uint16_t tethys_get_low_user_id(void)
{
    bool flag = false;
    for (uint16_t id = 10; id <= 100; id++)
    {
        flag = false;
        for (int i = 0; i < gTethysUserCount; i++)
        {
            if (id == gTethysUsers[i].id)
            {
                flag = true;
                break;
            }
        }
        if (flag == false)
        {
            return id;
        }
    }
    return 0;
}

static uint8_t m_cur_pass_id = 0;
int8_t tethys_is_password(uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].mode == 0)
        {
            if (strstr((char*)buf, (char*)gTethysUsers[i].content))
            {
                m_cur_pass_id = i;
                return 0;
            }
        }
    }
    return -1;
}

uint8_t tethys_get_cur_pass_id(void)
{
    return m_cur_pass_id;
}

int8_t tethys_get_password(uint16_t id, uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].id == id)
        {
            memcpy(buf, gTethysUsers[i].content, 6);
            return 0;
        }
    }
    return -1;
}

int8_t tethys_is_the_password(uint16_t id, uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].id == id)
        {
            if (strstr((char*)buf, (char*)gTethysUsers[i].content))
            {
                return 0;
            }
        }
    }
    return -1;
}

int8_t tethys_is_the_password_2(uint8_t len, uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].mode == 0)
        {
            //if (strstr((char*)buf, (char*)gTethysUsers[i].content))
            if (memcmp(buf, gTethysUsers[i].content, len) == 0)
            {
                m_cur_pass_id = i;
                return 0;
            }
        }
    }
    return -1;
}

int8_t tethys_is_admin_password(uint8_t *buf)
{
    bool isAdminPass = false;
    bool haveAdmin = false;
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0 && gTethysUsers[i].mode == 0)
        {
            haveAdmin = true;
            if (strstr((char*)buf, (char*)gTethysUsers[i].content))
            {
                isAdminPass = true;
                break;
            }
        }
    }
    if (isAdminPass)
    {
        return 0;
    }
    if (haveAdmin == false)
    {
        char defaultPass[16];
        strcpy(defaultPass, "123456");
        if (strstr((char*)buf, defaultPass))
        {
            return 0;
        }
    }
    return -1;
}

int8_t tethys_is_admin_fingerprint(uint8_t *buf)
{
    return -1;
}

int8_t tethys_is_admin_card(uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0 && gTethysUsers[i].mode == 2)
        {
            if (0 == memcmp(buf, gTethysUsers[i].content, 4))
            {
                return 0;
            }
        }
    }
    return -1;
}

static uint8_t m_cur_card_id;
int8_t tethys_is_card(uint8_t *buf)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].mode == 2)
        {
            if (0 == memcmp(buf, gTethysUsers[i].content, 4))
            {
                m_cur_card_id = i;
                return 0;
            }
        }
    }
    return -1;
}

uint8_t tethys_get_cur_card_id(void)
{
    return m_cur_card_id;
}

int8_t tethys_is_number_used(uint16_t id)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].id == id)
        {
            return 0;
        }
    }
    return -1;
}

int8_t tethys_is_admin_number_used(uint16_t id)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 0 && gTethysUsers[i].id == id)
        {
            return 0;
        }
    }
    return -1;
}

int8_t tethys_is_common_user_number_used(uint16_t id)
{
    for (int i = 0; i < gTethysUserCount; i++)
    {
        if (gTethysUsers[i].admin == 1 && gTethysUsers[i].id == id)
        {
            return 0;
        }
    }
    return -1;
}

static void tethys_pstorage_callback_handler1(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update1(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_callback_handler2(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update2(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_callback_handler3(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update3(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_callback_handler4(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update4(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_callback_handler5(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update5(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_open_lock_history_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_open_lock_history(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_open_lock_history_pos_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
            tethys_storage_save_open_lock_history_pos_just();
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_open_lock_history_pos(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}
static void tethys_pstorage_system_time_etc_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)save_wall_clock_just);
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_system_time_etc(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}
static void tethys_pstorage_callback_handler0(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update0(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_license_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_license(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_open_lock_times_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
            tethys_storage_save_open_lock_times_just();
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_open_lock_times(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void tethys_pstorage_Elara_callback_handler(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        tethys_handle_pstorage_update_Elara(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}


extern uint8_t g_pstorage_buffer[32];
extern ble_nus_t m_nus;
extern tethys_ble_nus_t m_tethys_nus;
static void tethys_handle_pstorage_update1(uint32_t result)
{
    switch (g_Tethys_pstorage_state)
    {
        case TETHYS_PSTORAGE_UPDATE_THE_PASSWORD:
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x09;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x09;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x04;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
            
        case TETHYS_PSTORAGE_REGISTER_ADMIN://admin
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x08;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x08;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_USER://common user
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x09;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x09;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_ADMIN_EXTEND://admin
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_USER_EXTEND://common user
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x02;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x02;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        default:
            break;
    }
}

static void tethys_handle_pstorage_update2(uint32_t result)
{
    switch (g_Tethys_pstorage_state)
    {
        case TETHYS_PSTORAGE_UPDATE_THE_PASSWORD:
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x09;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x09;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x04;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_ADMIN://admin
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x08;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x08;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_USER://common user
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x09;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0B;
                g_pstorage_buffer[2] = 0x09;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_ADMIN_EXTEND://admin
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_REGISTER_USER_EXTEND://common user
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x02;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x11;
                g_pstorage_buffer[2] = 0x02;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = g_ID;
                g_pstorage_buffer[5] = 0x05;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        default:
            break;
    }
}

static void tethys_handle_pstorage_update3(uint32_t result)
{

}

static void tethys_handle_pstorage_update4(uint32_t result)
{

}

static void tethys_handle_pstorage_update5(uint32_t result)
{

}
static void tethys_handle_pstorage_update_open_lock_history(uint32_t result)
{
}
static void tethys_handle_pstorage_update_open_lock_history_pos(uint32_t result)
{
}
static void tethys_handle_pstorage_update_system_time_etc(uint32_t result)
{
}

static void tethys_handle_pstorage_update0(uint32_t result)
{
    
}

static void tethys_handle_pstorage_update(uint32_t result)
{
    switch (g_Tethys_pstorage_state)
    {
        case TETHYS_PSTORAGE_SAVE_BANGDING:
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0C;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
                led_blink('*');
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0C;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x03;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
                led_blink('#');
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_DELETE_BANGDING_ID:
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0C;
                g_pstorage_buffer[2] = 0x03;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0C;
                g_pstorage_buffer[2] = 0x03;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x03;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_FACTORY_RESET:
            if (result == NRF_SUCCESS)
            {
                uint8_t id[32] = {0};
                memset(id, 0xFF, 16);
                g_Tethys_pstorage_state = TETHYS_PSTORAGE_SAVE_BANGDING_2;
                tethys_storage_save_bangding_id(id, 16);
            }
            else
            {
                uint8_t id[32] = {0};
                memset(id, 0xFF, 16);
                g_Tethys_pstorage_state = TETHYS_PSTORAGE_SAVE_BANGDING_2;
                tethys_storage_save_bangding_id(id, 16);
            }
            break;
        case TETHYS_PSTORAGE_SAVE_BANGDING_2:
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_ERASE_ALL);
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        case TETHYS_PSTORAGE_UPDATE_PEIDUI_PASSWORD:
            if (result == NRF_SUCCESS)
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0E;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x00;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
            }
            else
            {
                g_pstorage_buffer[0] = 0xEA;//L1_HEADER_iBEACON_MAGIC;
                g_pstorage_buffer[1] = 0x0E;
                g_pstorage_buffer[2] = 0x01;
                g_pstorage_buffer[3] = 0x01;
                g_pstorage_buffer[4] = 0x04;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;
        default:
            break;
    }
}

static void tethys_handle_pstorage_update_license(uint32_t result)
{
    switch (g_Tethys_pstorage_state)
    {
        case TETHYS_PSTORAGE_LICENSE:
            if (result == NRF_SUCCESS)
            {
                strcpy((char*)g_pstorage_buffer, "OKOK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 4);
                tethys_ble_nus_string_send(&m_tethys_nus, g_pstorage_buffer, 4);
                tethys_adv_algorithms_init();
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAILFAIL");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 8);
                tethys_ble_nus_string_send(&m_tethys_nus, g_pstorage_buffer, 8);
            }
            g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
            break;        
        default:
            break;
    }
}

static void tethys_handle_pstorage_update_open_lock_times(uint32_t result)
{
}

static void tethys_handle_pstorage_update_Elara(uint32_t result)
{
}

/** @brief Function for getting vector of random numbers.
 *
 * @param[out] p_buff                               Pointer to unit8_t buffer for storing the bytes.
 * @param[in]  length                               Number of bytes to take from pool and place in p_buff.
 *
 * @retval     Number of bytes actually placed in p_buff.
 */
uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size)
{
    uint8_t available;
    uint32_t err_code;
    err_code = nrf_drv_rng_bytes_available(&available);
    APP_ERROR_CHECK(err_code);
    uint8_t length = (size<available) ? size : available;
    err_code = nrf_drv_rng_rand(p_buff,length);
    APP_ERROR_CHECK(err_code);
    return length;
}

static void tethys_storage_delete_one_fingerprint_callback(uint8_t flag)
{
    gDeleteFingerprintCur++;
    if (gDeleteFingerprintCur < gDeleteFingerprintCounts)
    {
        app_timer_start(m_tethys_storage_time_id, APP_TIMER_TICKS(311, 0), NULL);
    }
    else
    {
        //gDeleteCB(0);
        if (gDeleteCB)
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)gDeleteCB);
        }
    }
}

static void tethys_storage_delete_one_fingerprint(void)
{
    _FG_REQ_ERASE_ONE(TethysID_FP[gDeleteFingerprintCur], tethys_storage_delete_one_fingerprint_callback);
}

static void tethys_storage_timer_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)tethys_storage_delete_one_fingerprint);
}
