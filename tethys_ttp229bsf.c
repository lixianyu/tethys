/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdlib.h>
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_timer.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"
#include "config.h"
#include "tethys_ttp229bsf.h"
#include "tethys_laba.h"
#include "tethys_mfrc522.h"
#include "tethys_storage.h"
#include "Tethys_led.h"
#include "tethys_fingerprint.h"
#include "tethys_moto.h"
#include "bd_wall_clock_timer.h"

extern bool gYiKaiSuo;
extern uint8_t m_beacon_info[];
extern void ttp229_get_data(void);
extern void ttp229_enable_int(void);
void ttp229_resolve_password(void);
void ttp229_return_last_menu(void);
static void ttp229_timeout(void);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
// frequency:1K ~ 512K, so 1.953125us ~ 1000us
#define TTP_F_SCL 3
#define TTP_PASSWORD_LENGTH 64
static uint8_t gTTP229State = 0;
static char gDefaultPassword[TTP_PASSWORD_LENGTH] = "123456";
char gInput[TTP_PASSWORD_LENGTH];
static char gFirstInputPassword[12];
uint8_t gInputIdx = 0;
uint16_t gNumber;
#ifdef TTP229BSF_LOGIC_0
APP_TIMER_DEF(m_ttp229bsf_timer_id);
static bool gm_ttp229bsf_timer_id = false;
#endif
static uint8_t gReEnterWhere = 0;//0, admin; 1, common user.
APP_TIMER_DEF(m_ttp229bsf_timeout_id);
setting_state_t gSettingState = TETHYS_SET_STATE_0;

APP_TIMER_DEF(m_ttp229bsf_re_enter_luru_admin_id);
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void testttp229bsf(void)
{

}

void ttp229_reset_input(void)
{
    memset(gInput, 0, sizeof(gInput));
    gInputIdx = 0;
}

void ttp229_close(void)
{
    app_timer_stop(m_ttp229bsf_timeout_id);
}

static uint8_t get_LaBa_address(int16_t num, uint8_t *pAddress)
{
    if (num == 0)
    {
        *pAddress = LABA_0;
        return 1;
    }
    if (num < 10)
    {
        *pAddress = num + 0x3E;
        return 1;
    }
    if (num < 20)
    {
        if (num == 10)
        {
            *pAddress = LABA_10;
            return 1;
        }
        else
        {
            num -= 10;
            *pAddress = LABA_10;
            *(pAddress + 1) = num + 0x3E;
            return 2;
        }
    }
    if (num < 30)
    {
        if (num == 20)
        {
            *pAddress = LABA_2;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 20;
            *pAddress = LABA_2;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 40)
    {
        if (num == 30)
        {
            *pAddress = LABA_3;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 30;
            *pAddress = LABA_3;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 50)
    {
        if (num == 40)
        {
            *pAddress = LABA_4;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 40;
            *pAddress = LABA_4;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 60)
    {
        if (num == 50)
        {
            *pAddress = LABA_5;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 50;
            *pAddress = LABA_5;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 70)
    {
        if (num == 60)
        {
            *pAddress = LABA_6;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 60;
            *pAddress = LABA_6;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 80)
    {
        if (num == 70)
        {
            *pAddress = LABA_7;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 70;
            *pAddress = LABA_7;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 90)
    {
        if (num == 80)
        {
            *pAddress = LABA_8;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 80;
            *pAddress = LABA_8;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
    if (num < 100)
    {
        if (num == 90)
        {
            *pAddress = LABA_9;
            *(pAddress + 1) = LABA_10;
            return 2;
        }
        else
        {
            num -= 90;
            *pAddress = LABA_9;
            *(pAddress + 1) = LABA_10;
            *(pAddress + 2) = num + 0x3E;
            return 3;
        }
    }
}

void ttp229bsf_detect_pin_handler_HIGHTOLOW(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    #ifdef TTP229BSF_LOGIC_0
    if (gTTP229State == 0)
    {
        //LEDS_ON(BSP_LED_4_MASK);
        gTTP229State = 1;
        //nrf_drv_gpiote_in_event_disable(TTP229BSF_SDO);
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ttp229_get_data);
    }
    #else
    if (gYiKaiSuo)
    {
        return;
    }
    if (gTTP229State == 0)
    {
        gTTP229State = 1;
        nrf_drv_gpiote_in_event_disable(TTP229BSF_SDO);
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ttp229_get_data);
    }
    #endif
}
#ifdef TTP229BSF_LOGIC_0
static void ttp229bsf_timeout_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ttp229_enable_int);
}
#endif

static void ttp229bsf_timeout_timeout_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ttp229_timeout);
}

static void re_enter_luru(void)
{
    sleep_fingerprint();
    close_MFRC522();
    gSettingState = TETHYS_SET_STATE_NOTHING;
    if (gReEnterWhere == 0)//admin
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1;
    }
    else
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_1;
    }
}

static void re_enter_admin_luru_handler(void *p_context)
{
    #if 1
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)re_enter_luru);
    #else
    gSettingState = TETHYS_SET_STATE_NOTHING;
    if (gReEnterWhere == 0)//admin
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1;
    }
    else
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_1;
    }
    #endif
}

void init_ttp229bsf(void)
{
    nrf_delay_ms(500);
    
    nrf_gpio_cfg_output(TTP229BSF_SCL);
    nrf_gpio_pin_set(TTP229BSF_SCL);
#if 1
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLUP;
    //in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_HITOLO;
    //in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    err_code = nrf_drv_gpiote_in_init(TTP229BSF_SDO, &in_config, ttp229bsf_detect_pin_handler_HIGHTOLOW);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
#endif
#ifdef TTP229BSF_LOGIC_0
    app_timer_create(&m_ttp229bsf_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     ttp229bsf_timeout_handler);
#endif
    app_timer_create(&m_ttp229bsf_timeout_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     ttp229bsf_timeout_timeout_handler);
    app_timer_create(&m_ttp229bsf_re_enter_luru_admin_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     re_enter_admin_luru_handler);
}

#ifdef TTP229BSF_LOGIC_0
void ttp229_enable_int(void)
{
    gTTP229State = 0;
    gm_ttp229bsf_timer_id = false;
    if (isLabaPlaying() == 0)
    {
        nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
    }
}
#endif

//After 20s ...
static void ttp229_timeout(void)
{
    gSettingState = TETHYS_SET_STATE_0;
    close_LEDS();
    sleep_fingerprint();
    memset(gInput, 0, sizeof(gInput));
    gInputIdx = 0;
    
    //playLaBa(LABA_DING_DONG);
    //closeAmplifier();
    close_MFRC522();
    nrf_gpio_pin_set(TTP229BSF_SCL);
    sd_nvic_critical_region_exit(0);
    nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
    gTTP229State = 0;
    #ifdef TETHYS_PWM
    led_pwm_stop_1();
    #endif
    led_DH_return_to_idle();
}

void ttp220_return_idle(uint32_t ms)
{
    app_timer_stop(m_ttp229bsf_timeout_id);
    app_timer_start(m_ttp229bsf_timeout_id, APP_TIMER_TICKS(ms, APP_TIMER_PRESCALER), NULL);
}

//where -- 0,admin; 1,common user.
void ttp229_return_re_enter_luru(uint8_t where, uint32_t ms)
{
    gReEnterWhere = where;
    //app_timer_stop(m_ttp229bsf_re_enter_luru_admin_id);
    gSettingState = TETHYS_SET_STATE_NOTHING;
    app_timer_start(m_ttp229bsf_re_enter_luru_admin_id, APP_TIMER_TICKS(ms, APP_TIMER_PRESCALER), NULL);
}

void ttp229_enable(void)
{
    #ifdef TTP229BSF_LOGIC_0
    if (gm_ttp229bsf_timer_id == false)
    {
        nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
    }
    #endif
}
void ttp229_disable(void)
{
    #ifdef TTP229BSF_LOGIC_0
    if (gm_ttp229bsf_timer_id == false)
    {
        nrf_drv_gpiote_in_event_disable(TTP229BSF_SDO);
    }
    #endif
}

int8_t ttp229_is_one_number_state(void)
{
    if (gSettingState == TETHYS_SET_STATE_SETTING_MAIN ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_3 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_3 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_3_2 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_3 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_3_2 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_3_3 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_4 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_4_1 ||
            gSettingState == TETHYS_SET_STATE_SETTING_MAIN_4_2)
    {
        return 0;
    }
    return -1;
}

int8_t ttp229_is_correct_state_for_fingerprint(void)
{
    if (gSettingState == TETHYS_SET_STATE_0
      ||gSettingState == TETHYS_SET_STATE_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_1_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_2_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_1_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_2_1
      ||gSettingState == TETHYS_SET_BLUETOOTH)
    {
        return 0;
    }
    return 1;
}

int8_t ttp229_is_correct_state_for_RC522(void)
{
    if (gSettingState == TETHYS_SET_STATE_0
      ||gSettingState == TETHYS_SET_STATE_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_1_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_2_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_1_1
      ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_2_1)
    {
        return 0;
    }
    return 1;
}

void ttp229_reset_timeout_timer(void)
{
    app_timer_stop(m_ttp229bsf_timeout_id);
    app_timer_start(m_ttp229bsf_timeout_id, APP_TIMER_TICKS(50000, APP_TIMER_PRESCALER), NULL);
}

void ttp229_get_data(void)
{
    #define AN_JIAN_YIN
    uint8_t i;
    uint16_t data = 0;
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);

    app_timer_stop(m_ttp229bsf_timeout_id);
    app_timer_start(m_ttp229bsf_timeout_id, APP_TIMER_TICKS(20000, APP_TIMER_PRESCALER), NULL);

    //nrf_delay_us(93); // DV
    //nrf_delay_us(10); // Tw
    #if 0
    while (!nrf_gpio_pin_read(TTP229BSF_SDO))
    {
    }
    nrf_delay_us(10);
    #endif

    for(i = 0; i < 16; i++)
    {
        data = data >> 1;
        nrf_gpio_pin_clear(TTP229BSF_SCL);
        nrf_delay_us(TTP_F_SCL);
        if (nrf_gpio_pin_read(TTP229BSF_SDO))
        {
            //data = data & 0xFE;
        }
        else
        {
            //data = data | 0x01;
            data = data | 0x8000;
        }
        //data = data << 1;
        nrf_gpio_pin_set(TTP229BSF_SCL);
        nrf_delay_us(TTP_F_SCL);
    }
    if (data != 0 && for_ttp229_is_card_present())
    {
        if (getTTP_state() == TETHYS_SET_STATE_0)
        {
            if (for_ttp229_PICC_ReadCardSerial())
            {
                uint32_t vOLH[3];
                vOLH[0] = tethys_get_cur_card_id();
                vOLH[1] = 3;
                vOLH[2] = SecondCountRTC;
                moto_open_lock(&vOLH, sizeof(vOLH));
                //ttp220_return_idle(4000);
                sd_nvic_critical_region_exit(is_nested_critical_region);
                nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
                gTTP229State = 0;
                return;
            }
            else
            {
                playLaBa(LABA_SHU_RU_YOU_WU);
                goto Exitme;
            }
        }
        else if (getTTP_state() == TETHYS_SET_STATE_1)
        {
            if (for_ttp229_PICC_ReadCardSerial_for_admin())
            {
                ttp229_resolve_RFID_state_1(0x00);
            }
            else
            {
                playLaBa(LABA_SHU_RU_YOU_WU);
            }
            goto Exitme;
        }
        else if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_2_1)
        {
            if (for_ttp229_PICC_Select())
            {
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)rc522_luru_OK);
                sd_nvic_critical_region_exit(is_nested_critical_region);

                //sd_nvic_critical_region_exit(0);
                //nrf_delay_ms(100);
                nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
                gTTP229State = 0;
                return;
            }
            else
            {
                uint8_t streams[16] = {LABA_LU_RU, 25,
                               LABA_KA_PIAN, 25,
                               LABA_SHI_BAI, 190};
                playLaBaMany(streams, 3);
            }
            goto Exitme;
        }
        goto Exitme;
    }
    switch (data)
    {
    case 0x0002: // 2
        gInput[gInputIdx++] = '1';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_1);
        #endif
        break;
    case 0x0004: // 3
        gInput[gInputIdx++] = '4';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_4);
        #endif
        break;
    case 0x0008: // 4
        gInput[gInputIdx++] = '2';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_2);
        #endif
        break;
    case 0x0010://5
        gInput[gInputIdx++] = '5';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_5);
        #endif
        break;
    case 0x0020://6
        gInput[gInputIdx++] = '8';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_8);
        #endif
        break;
    case 0x0040://7
        gInput[gInputIdx++] = '#';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_AN_JIAN_YIN_1);
        #endif
        break;
    case 0x0200://10
        gInput[gInputIdx++] = '9';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_9);
        #endif
        break;
    case 0x0400://11
        gInput[gInputIdx++] = '6';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_6);
        #endif
        break;
    case 0x0800://12
        gInput[gInputIdx++] = '3';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_3);
        #endif
        break;
    case 0x2000:
        gInput[gInputIdx++] = '0';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_0);
        #endif
        break;
    case 0x4000:
        gInput[gInputIdx++] = '7';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_1);
        #else
        playLaBa(LABA_7);
        #endif
        break;
    case 0x8000:
        gInput[gInputIdx++] = '*';
        #ifdef AN_JIAN_YIN
        playLaBa(LABA_AN_JIAN_YIN_2);
        #else
        playLaBa(LABA_AN_JIAN_YIN_2);
        #endif
        break;
    #if 0
////////////////////////////////////////////////////////////////
    case 0x1000:
        //gInput[gInputIdx++] = '*';
        playLaBa(LABA_MONTH);
        return;
        break;
    case 0x0080://8
        //gInput[gInputIdx++] = '8';
        playLaBa(LABA_YEAR);
        return;
        break;
    case 0x0100://9
        //gInput[gInputIdx++] = '9';
        playLaBa(LABA_DAY);
        return;
        break;
    case 0x0001: // 1
        //gInput[gInputIdx++] = '1';
        playLaBa(LABA_MINUTE);
        return;
        break;
    #endif
    default:
        //playLaBa(LABA_ZHI_WEN);
        //return;
        goto Exitme;
        break;
    }
    open_LEDs();
    if (gInputIdx >= (TTP_PASSWORD_LENGTH - 1))
    {
        gInput[TTP_PASSWORD_LENGTH - 1] = '#';
        //stopLaBaMany();
        ttp229_resolve_password();
    }
    else if (gInput[gInputIdx - 1] == '#')
    {
        //stopLaBaMany();
        ttp229_resolve_password();
    }
    else if (gInput[0] == '*' && (gSettingState != TETHYS_SET_STATE_0))
    {
        if (gSettingState == TETHYS_SET_STATE_SETTING_MAIN_1_1
          ||gSettingState == TETHYS_SET_STATE_SETTING_MAIN_2_1)
        {
            ttp229_timeout();
            return;
        }
        else
        {
            ttp229_return_last_menu();//返回上一级菜单
        }
    }
    else if (0 == ttp229_is_one_number_state())
    {
        gInput[gInputIdx] = '#';
        //stopLaBaMany();
        ttp229_resolve_password();
    }
    
Exitme:
    #if 0
    while (!nrf_gpio_pin_read(TTP229BSF_SDO))
    {
    }
    #endif
    sd_nvic_critical_region_exit(0);
    //nrf_delay_ms(100);
    nrf_drv_gpiote_in_event_enable(TTP229BSF_SDO, true);
    gTTP229State = 0;
}

static void ttp229_open_lock(void)
{
    uint32_t vOLH[3];
    vOLH[0] = tethys_get_cur_pass_id();
    vOLH[1] = 1;
    vOLH[2] = SecondCountRTC;
    moto_open_lock(&vOLH, sizeof(vOLH));
}

setting_state_t getTTP_state(void)
{
    return gSettingState;
}

void setTTP_state(setting_state_t state)
{
    gSettingState = state;
}

uint16_t getCurID(void)
{
    return gNumber;
}

static void ttp229_delete_callback(uint8_t flag)
{
    uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 90};
    playLaBaMany(streams, 1);
    led_DH_delete_stop();
    ttp220_return_idle(5000);
    gSettingState = TETHYS_SET_STATE_0;
}

static void ttp229_get_version_sound(uint8_t *pBuf, uint8_t *pLen)
{
    char ver[32] = {0};
    char verSound[32] = {0};
    strncpy(ver, SW_REV_STR, 11);
    int j = 0;
    for (int i = 0; i < 11; i++)
    {
        switch(ver[i])
        {
            case '0':
                verSound[j] = LABA_0;
                verSound[j + 1] = 70;
                break;
            case '1':
                verSound[j] = LABA_1;
                verSound[j + 1] = 70;
                break;
            case '2':
                verSound[j] = LABA_2;
                verSound[j + 1] = 70;
                break;
            case '3':
                verSound[j] = LABA_3;
                verSound[j + 1] = 70;
                break;
            case '4':
                verSound[j] = LABA_4;
                verSound[j + 1] = 70;
                break;
            case '5':
                verSound[j] = LABA_5;
                verSound[j + 1] = 70;
                break;
            case '6':
                verSound[j] = LABA_6;
                verSound[j + 1] = 70;
                break;
            case '7':
                verSound[j] = LABA_7;
                verSound[j + 1] = 70;
                break;
            case '8':
                verSound[j] = LABA_8;
                verSound[j + 1] = 70;
                break;
            case '9':
                verSound[j] = LABA_9;
                verSound[j + 1] = 70;
                break;
            case '.':
                verSound[j] = LABA_DIAN;
                verSound[j + 1] = 70;
                break;
            default:
                break;
        }
        j += 2;
    }
    *pLen = 11;
    memcpy(pBuf, verSound, 22);
}

extern uint8_t gNiuBi;
void ttp229_resolve_password_state_0(void)
{
    char debugString[32] = "*20161021#";
    char debugString1[32] = "**20161021#";
    char debugString2[32] = "***20161021#";
    if (strncmp(gInput, debugString, 10) == 0 ||
        strncmp(gInput, debugString1, 11) == 0 ||
        strncmp(gInput, debugString2, 12) == 0)
    {
        if (gNiuBi == 0)
        {
            gNiuBi = 1;
            led_donghua_open_NiuBi();
        }
        else
        {
            gNiuBi = 0;
            led_donghua_close_NiuBi();
        }
        return;
    }
    strcpy(debugString, "*8883#");
    strcpy(debugString1, "**8883#");
    strcpy(debugString2, "***8883#");
    if (strncmp(gInput, debugString, 6) == 0 ||
        strncmp(gInput, debugString1, 7) == 0 ||
        strncmp(gInput, debugString2, 8) == 0)
    {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_version_sound(streams, &len);
        playLaBaMany(streams, len);
        return;
    }
    strcpy(debugString, "*8882#");
    strcpy(debugString1, "**8882#");
    strcpy(debugString2, "***8882#");
    if (strncmp(gInput, debugString, 6) == 0 ||
        strncmp(gInput, debugString1, 7) == 0 ||
        strncmp(gInput, debugString2, 8) == 0)
    {
        tethys_factory_reset();
        return;
    }

    uint8_t len = strlen(gInput);

    if (strstr(gInput, "*#"))
    {
        gSettingState = TETHYS_SET_STATE_1;
        uint8_t streams[4] = {LABA_GUAN_LI_YUAN_SHE_ZHI, 160,
                              LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA, 255
                             };
        playLaBaMany(streams, 2);
        return;
    }
    if (len >= 2 && gInput[len - 2] == '*' && gInput[len - 1] == '#')
    {
        gSettingState = TETHYS_SET_STATE_1;
        uint8_t streams[4] = {LABA_GUAN_LI_YUAN_SHE_ZHI, 160,
                              LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA, 255
                             };
        playLaBaMany(streams, 2);
        return;
    }
    if ((gInput[0] == '*' && gInput[1] == '#') ||
         (gInput[0] == '*' && gInput[1] == '*' && gInput[2] == '#'))
    {
        gSettingState = TETHYS_SET_STATE_1;
        uint8_t streams[4] = {LABA_GUAN_LI_YUAN_SHE_ZHI, 160,
                              LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA, 255
                             };
        playLaBaMany(streams, 2);
        return;
    }
    if (gTethysUserCount == 0)
    {
        if (strstr(gInput, gDefaultPassword))
        {

            gSettingState = TETHYS_SET_STATE_0;
            ttp229_open_lock();
            return;
        }
    }
    else
    {
        if (0 == tethys_is_password((uint8_t *)gInput))
        {
            //LEDS_ON(BSP_LED_0_MASK);
            gSettingState = TETHYS_SET_STATE_0;
            ttp229_open_lock();
            return;
        }
    }

    playLaBa(LABA_ERROR);
}

static void ttp229_get_resolve_state_1_sound(uint8_t *pBuf, uint8_t *pLen)
{
    #if 0
    uint8_t streams[16] = {LABA_1, 80,
                           LABA_GUAN_LI_YUAN_SHE_ZHI, 150,//管理员设置
                           LABA_2, 80,
                           LABA_PU_TONG_YONG_HU_SHE_ZHI, 150,//普通用户设置
                           LABA_3, 80,
                           LABA_XI_TONG_SHE_ZHI, 150,//系统设置
                           LABA_4, 80,
                           LABA_SHU_JU_TONG_JI, 200//数据统计
                          };
    *pLen = 8;
    memcpy(pBuf, streams, 16);
    #else
    uint8_t streams[16] = {LABA_1, 80,
                           LABA_GUAN_LI_YUAN_SHE_ZHI, 150,//管理员设置
                           LABA_2, 80,
                           LABA_PU_TONG_YONG_HU_SHE_ZHI, 150,//普通用户设置
                          };
    *pLen = 4;
    memcpy(pBuf, streams, 8);
    #endif
}

void ttp229_resolve_RFID_state_1(uint8_t flag)
{
    if (flag == 0x00)
    {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_resolve_state_1_sound(streams, &len);
        playLaBaMany(streams, len);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}

void ttp229_resolve_fingerprint_state_1(uint8_t flag)
{
    if (flag == 0x00)
    {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_resolve_state_1_sound(streams, &len);
        playLaBaMany(streams, len);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}

// TETHYS_SET_STATE_1
void ttp229_resolve_password_state_1(void)
{
    if (gTethysUserCount == 0)
    {
        if (strstr(gInput, gDefaultPassword))
        {
            uint8_t streams[32] = {0};
            uint8_t len;
            ttp229_get_resolve_state_1_sound(streams, &len);
            playLaBaMany(streams, len);
            gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
        }
        else
        {
            playLaBa(LABA_ERROR);
        }
    }
    else if (0 == tethys_is_admin_password((uint8_t *)gInput))
    {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_resolve_state_1_sound(streams, &len);
        playLaBaMany(streams, len);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}

void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN(void)
{
    if (gInput[0] == '1')
    {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 90,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 90,
                               LABA_XIU_GAI_GUAN_LI_YUAN, 150,//修改管理员
                               LABA_3, 90,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170,//删除管理员
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 80,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170,//删除管理员
                              };
        playLaBaMany(streams, 4);
        #endif
        
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1;
    }
    else if (gInput[0] == '2')
    {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_XIUGAI_PUTONG_YONGHU, 150,//修改普通用户
                               LABA_3, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190
                              };//删除普通用户
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 170,//删除普通用户
                              };
        playLaBaMany(streams, 4);
        #endif
        
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2;
    }
    #if 0
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_SHIJIAN_SHEZHI, 150, //时间设置
                               LABA_2, 80,
                               LABA_YANZHENG_MOSHI_SHEZHI, 150,//验证模式设置
                               LABA_3, 80,
                               LABA_CHANGKAI_GONGNENG_SHEZHI, 190
                              };//常开功能设置
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_3;
    }
    else if (gInput[0] == '4')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_KAIMEN_JILU_CHAXUN, 150, //开门记录查询
                               LABA_2, 80,
                               LABA_CUNCHU_XINXI_TONGJI, 199,//存储信息统计
                              };
        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_4;
    }
    #endif
    else
    {
        playLaBa(LABA_ERROR);
    }

}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1;
    }
    #ifdef TETHYS_XIUGAI_GUANLIYUAN
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_XIUGAI_BIANHAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2;
    }
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 150, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU, 180,//删除全部
                              };
        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3;
    }
    #else
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 150, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU, 180,//删除全部
                              };
        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3;
    }
    #endif
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1(void)
{
    if ((gInput[0] == '*' && gInput[1] == '#') ||
         (gInput[0] == '*' && gInput[1] == '*' && gInput[2] == '#'))
    {
        led_DH_return_to_idle();
        ttp220_return_idle(100);
        return;
    }
    char theNumber[16] = {0};
    if (gInput[0] == '0' && gInput[1] == '0' && (gInput[2] >= '1' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[2];
    }
    else if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[1];
    }
    else if (gInput[0] >= '1' && gInput[0] <= '9' && gInput[1] == '#')
    {
        theNumber[0] = gInput[0];
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }

    gNumber = atoi(theNumber);
    if (0 == tethys_is_admin_number_used(gNumber)) //编号已被使用
    {
        //gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1;
        uint8_t streams[16] = {LABA_GAI_BIANHAO_YI_BEI_SHIYONG, 160,
                               LABA_QING_CHONGXIN_SHURU, 170};

        playLaBaMany(streams, 2);
        return;
    }

    memset(gFirstInputPassword, 0, sizeof(gFirstInputPassword));
    gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1_1;
    playLaBa(LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA);
    nrf_delay_ms(2600);
#if 0
    memcpy(m_beacon_info + BEACON_UUID_OFFSET, &gNumber, 2);
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    advertising_init_beacon();
#endif
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1_1(void)
{
    tethys_man_t aman;
    //Wait for password, fingerprint or card
    if (gInput[6] == '#')
    {
        if (strlen(gFirstInputPassword) == 0)
        {
            strncpy(gFirstInputPassword, gInput, 6);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        if (strncmp(gFirstInputPassword, gInput, 6) != 0)
        {
            playLaBa(LABA_ERROR);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        memset(&aman, 0, sizeof(aman));
        aman.admin = 0;
        aman.mode = 0;
        aman.id = gNumber;
        memcpy(aman.content, gInput, 6);
        
        //playLaBa(LABA_LURU_CHENGGONG);
        uint8_t streams[16] = {LABA_LURU_CHENGGONG, 100};
        playLaBaMany(streams, 1);
        tethys_storage_save(&aman);
        //ttp220_return_idle(5000);
        gSettingState = TETHYS_SET_STATE_NOTHING;
        ttp229_return_re_enter_luru(0, 3000);
        led_donghua_1(NULL);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}

void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2(void)
{
    char theNumber[16] = {0};
    if (gInput[0] == '0' && gInput[1] == '0' && (gInput[2] >= '1' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[2];
        gNumber = atoi(theNumber);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2_1;
    }
    else if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[1];
        gNumber = atoi(theNumber);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2_1;
    }
    else if (gInput[0] >= '1' && gInput[0] <= '9' && gInput[1] == '#')
    {
        theNumber[0] = gInput[0];
        gNumber = atoi(theNumber);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2_1;
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }
    if (0 == tethys_is_admin_number_used(gNumber))
    {
        memset(gFirstInputPassword, 0, sizeof(gFirstInputPassword));
        playLaBa(LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA);
        nrf_delay_ms(2600);
        return;
    }
    gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2;
    uint8_t streams[16] = {LABA_BIAN_HAO_CUO_WU, 160,
                           LABA_QING_CHONGXIN_SHURU, 190};

    playLaBaMany(streams, 2);
}

void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2_1(void)
{
    tethys_man_t aman;
    //Wait for password, fingerprint or card
    if (gInput[6] == '#')
    {
        if (strlen(gFirstInputPassword) == 0)
        {
            strncpy(gFirstInputPassword, gInput, 6);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        if (strncmp(gFirstInputPassword, gInput, 6) != 0)
        {
            playLaBa(LABA_ERROR);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        memset(&aman, 0, sizeof(aman));
        aman.admin = 0;
        aman.mode = 0;
        aman.id = gNumber;
        memcpy(aman.content, gInput, 6);
        
        //playLaBa(LABA_XIUGAI_CHENGGONG);
        uint8_t streams[16] = {LABA_XIUGAI_CHENGGONG, 189};
        playLaBaMany(streams, 1);
        tethys_storage_update(&aman);
        ttp220_return_idle(2000);
        gSettingState = TETHYS_SET_STATE_0;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_QING_SHURU_SHANCHU_BIANHAO, 170};

        playLaBaMany(streams, 1);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3_1;
    }
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 190,
            LABA_SHANCHU_QUANBU1, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3_2;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1(void)
{
    char theNumber[16] = {0};
    if (gInput[0] == '0' && gInput[1] == '0' && (gInput[2] >= '1' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[2];
    }
    else if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[1];
    }
    else if (gInput[0] >= '1' && gInput[0] <= '9' && gInput[1] == '#')
    {
        theNumber[0] = gInput[0];
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }
    gNumber = atoi(theNumber);
    if (0 == tethys_is_admin_number_used(gNumber))
    {
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1;
        playLaBa(LABA_AN_HUICHEJIAN_QUEREN);
        nrf_delay_ms(1200);
        return;
    }
    
    uint8_t streams[16] = {LABA_BIAN_HAO_CUO_WU, 160,
                           LABA_QING_CHONGXIN_SHURU, 190};

    playLaBaMany(streams, 2);
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 90};
        playLaBaMany(streams, 1);
        tethys_storage_delete(gNumber);
        ttp220_return_idle(5000);
        gSettingState = TETHYS_SET_STATE_0;
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_2(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        if (tethys_storage_delete_all_admin(ttp229_delete_callback) == 0)
        {
            uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
            playLaBaMany(streams, 1);
            ttp220_return_idle(5000);
            gSettingState = TETHYS_SET_STATE_0;
        }
        else//说明有指纹，需要一点儿一点儿地删
        {
            led_DH_delete_start();
        }
    }
}

void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_1;
    }
    #ifdef TETHYS_XIUGAI_GUANLIYUAN
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_QING_SHU_RU_XIUGAI_BIANHAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_2;
    }
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_AN_LEIXING_SHANCHU, 180,//按类型删除
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3;
    }
    #else
    else if (gInput[0] == '2')
    {
        #ifdef TETHYS_AN_BIANHAO_SHANCHU
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_AN_LEIXING_SHANCHU, 180,//按类型删除
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3;
    }
    #endif
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1(void)
{
    if ((gInput[0] == '*' && gInput[1] == '#') ||
         (gInput[0] == '*' && gInput[1] == '*' && gInput[2] == '#'))
    {
        led_DH_return_to_idle();
        ttp220_return_idle(100);
        return;
    }
    char theNumber[16] = {0};
    if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[1];
        theNumber[1] = gInput[2];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && 
             (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
        theNumber[2] = gInput[2];
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }
    
    gNumber = atoi(theNumber);
    if (gNumber > TETHYS_USERS_COUNT)
    {
        playLaBa(LABA_BIAN_HAO_CUO_WU);
        nrf_delay_ms(1100);
        return;
    }
    
    if (0 == tethys_is_common_user_number_used(gNumber))
    {
        uint8_t streams[16] = {LABA_GAI_BIANHAO_YI_BEI_SHIYONG, 160,
                               LABA_QING_CHONGXIN_SHURU, 170};

        playLaBaMany(streams, 2);
        return;
    }
    memset(gFirstInputPassword, 0, sizeof(gFirstInputPassword));
    gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_1_1;
    playLaBa(LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA);
    nrf_delay_ms(2600);
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1_1(void)
{
    tethys_man_t aman;
    //Wait for password, fingerprint or card
    if (gInput[6] == '#')
    {
        if (strlen(gFirstInputPassword) == 0)
        {
            strncpy(gFirstInputPassword, gInput, 6);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        if (strncmp(gFirstInputPassword, gInput, 6) != 0)
        {
            playLaBa(LABA_ERROR);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        memset(&aman, 0, sizeof(aman));
        aman.admin = 1;
        aman.mode = 0;
        aman.id = gNumber;
        memcpy(aman.content, gInput, 6);
        
        //playLaBa(LABA_LURU_CHENGGONG);
        uint8_t streams[16] = {LABA_LURU_CHENGGONG, 190};
        playLaBaMany(streams, 1);
        tethys_storage_save(&aman);
        //ttp220_return_idle(1999);
        //gSettingState = TETHYS_SET_STATE_0;
        ttp229_return_re_enter_luru(1, 3000);
        led_donghua_1(NULL);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2(void)
{
    char theNumber[16] = {0};
    if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[1];
        theNumber[1] = gInput[2];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && 
             (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
        theNumber[2] = gInput[2];
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }
    gNumber = atoi(theNumber);
    if (gNumber > TETHYS_USERS_COUNT)
    {
        playLaBa(LABA_BIAN_HAO_CUO_WU);
        nrf_delay_ms(1100);
        return;
    }
    
    if (0 == tethys_is_common_user_number_used(gNumber))
    {
        memset(gFirstInputPassword, 0, sizeof(gFirstInputPassword));
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_2_1;
        playLaBa(LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA);
        nrf_delay_ms(2600);
        return;
    }
    
    uint8_t streams[16] = {LABA_BIAN_HAO_CUO_WU, 160,
                           LABA_QING_CHONGXIN_SHURU, 190};
    playLaBaMany(streams, 2);
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2_1(void)
{
    tethys_man_t aman;
    //Wait for password, fingerprint or card
    if (gInput[6] == '#')
    {
        if (strlen(gFirstInputPassword) == 0)
        {
            strncpy(gFirstInputPassword, gInput, 6);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        if (strncmp(gFirstInputPassword, gInput, 6) != 0)
        {
            playLaBa(LABA_ERROR);
            uint8_t streams[16] = {LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN, 100,
                           LABA_MI_MA, 90};
            playLaBaMany(streams, 2);
            return;
        }
        memset(&aman, 0, sizeof(aman));
        aman.admin = 1;
        aman.mode = 0;
        aman.id = gNumber;
        memcpy(aman.content, gInput, 6);
        
        //playLaBa(LABA_XIUGAI_CHENGGONG);
        uint8_t streams[16] = {LABA_XIUGAI_CHENGGONG, 189};
        playLaBaMany(streams, 1);
        tethys_storage_update(&aman);
        ttp220_return_idle(2000);
        gSettingState = TETHYS_SET_STATE_0;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_QING_SHURU_SHANCHU_BIANHAO, 170};

        playLaBaMany(streams, 1);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_1;
    }
    #ifdef TETHYS_AN_BIANHAO_SHANCHU
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_SHANCHU_QUANBU_KAPIAN, 180,
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU_MIMA, 180,
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU_ZHIWEN, 180
                              };
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2;
    }
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 160,
                               LABA_SHANCHU_QUANBU1, 90};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_3;
    }
    #else
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 160,
                               LABA_SHANCHU_QUANBU1, 90};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_3;
    }
    #endif
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1(void)
{
    char theNumber[16] = {0};
    if (gInput[0] == '0' && (gInput[1] >= '1' && gInput[1] <= '9') && (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[1];
        theNumber[1] = gInput[2];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && gInput[2] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
    }
    else if ((gInput[0] >= '1' && gInput[0] <= '9') && (gInput[1] >= '0' && gInput[1] <= '9') && 
             (gInput[2] >= '0' && gInput[2] <= '9') && gInput[3] == '#')
    {
        theNumber[0] = gInput[0];
        theNumber[1] = gInput[1];
        theNumber[2] = gInput[2];
    }
    else
    {
        playLaBa(LABA_ERROR);
        return;
    }
    gNumber = atoi(theNumber);
    if (gNumber > TETHYS_USERS_COUNT)
    {
        playLaBa(LABA_BIAN_HAO_CUO_WU);
        nrf_delay_ms(1100);
        return;
    }
    
    if (0 == tethys_is_common_user_number_used(gNumber))
    {
        playLaBa(LABA_AN_HUICHEJIAN_QUEREN);
        nrf_delay_ms(2600);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1;
        return;
    }
    
    uint8_t streams[16] = {LABA_BIAN_HAO_CUO_WU, 160,
                           LABA_QING_CHONGXIN_SHURU, 190};
    playLaBaMany(streams, 2);
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
        playLaBaMany(streams, 1);
        tethys_storage_delete(gNumber);
        ttp220_return_idle(2000);
        gSettingState = TETHYS_SET_STATE_0;
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 150,
                               LABA_SHANCHU_QUANBU_KAPIAN, 110};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1;
    }
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 150,
                               LABA_SHANCHU_QUANBU_MIMA, 110};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2;
    }
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_AN_HUICHEJIAN_QUEREN, 150,
                               LABA_SHANCHU_QUANBU_ZHIWEN, 110};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
        playLaBaMany(streams, 1);
        tethys_storage_delete_all_common_users_card();
        ttp220_return_idle(2000);
        gSettingState = TETHYS_SET_STATE_0;
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
        playLaBaMany(streams, 1);
        tethys_storage_delete_all_common_users_password();
        ttp220_return_idle(2000);
        gSettingState = TETHYS_SET_STATE_0;
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        if (0 == tethys_storage_delete_all_common_users_fingerprint(ttp229_delete_callback))
        {
            uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
            playLaBaMany(streams, 1);
            ttp220_return_idle(5000);
            gSettingState = TETHYS_SET_STATE_0;
        }
        else//说明有指纹，需要一点儿一点儿地删
        {
            led_DH_delete_start();
        }
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_3(void)
{
    if (gInput[0] == '#')
    {
        gSettingState = TETHYS_SET_STATE_NOTHING;
        if (tethys_storage_delete_all_common_users(ttp229_delete_callback) == 0)
        {
            uint8_t streams[16] = {LABA_SHAN_CHU_CHENG_GONG, 89};
            playLaBaMany(streams, 1);
            ttp220_return_idle(5000);
            gSettingState = TETHYS_SET_STATE_0;
        }
        else//说明有指纹，需要一点儿一点儿地删
        {
            led_DH_delete_start();
        }
    }
}

#if 0
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3(void)
{
    if (gInput[0] == '1')
    {
        playLaBa(LABA_QING_SHURU_YEAR_MONTH_DAY_HOUR_MINUTE);
        nrf_delay_ms(4000);
        playLaBa(LABA_YIGONG_12_GE_NUMBER);
        nrf_delay_ms(3000);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_3_1;
    }
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_QIDONG_DANKAI_MOSHI, 185,
                               LABA_2, 80,
                               LABA_QIDONG_SHUANG_KAI_MOSHI, 185};

        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_3_2;
    }
    else if (gInput[0] == '3')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_OPEN_CHANGKAI_GONGNENG, 185,
                               LABA_2, 80,
                               LABA_CLOSE_CHANGKAI_GONGNENG, 185};

        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_3_3;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_1(void)
{
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2(void)
{
    if (gInput[0] == '1')
    {
        playLaBa(LABA_DANKAI_MOSHI_QIDONG_CHENGGONG);
        nrf_delay_ms(3600);
        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else if (gInput[0] == '2')
    {
        playLaBa(LABA_SHUANGKAI_MOSHI_QIDONG_CHENGGONG);
        nrf_delay_ms(3600);
        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2_1(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2_2(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3(void)
{
    if (gInput[0] == '1')
    {
        playLaBa(LABA_CHANGKAI_GONGNENG_YI_DAKAI);
        nrf_delay_ms(3600);
        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else if (gInput[0] == '2')
    {
        playLaBa(LABA_CHANGKAI_GONGNENG_YI_GUANBI);
        nrf_delay_ms(3600);
        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3_1(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3_2(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4(void)
{
    if (gInput[0] == '1')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_SHUNXU_CHAXUN, 185,
                               LABA_2, 80,
                               LABA_AN_SHIJIAN_CHAXUN, 185};

        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_4_1;
    }
    else if (gInput[0] == '2')
    {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_GUANLIYUAN_TONGJI, 185,
                               LABA_2, 80,
                               LABA_PUTONG_YONGHU_TONGJI, 185};

        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_4_2;
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1(void)
{
    if (gInput[0] == '1')
    {
        playLaBa(LABA_DING_DONG);
    }
    else if (gInput[0] == '2')
    {
        playLaBa(LABA_DING_DONG);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1_1(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1_2(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2(void)
{
    uint8_t addr[12] = {0};
    if (gInput[0] == '1')
    {
        int16_t cot = tethys_how_many_admin();
        uint8_t adrCnt = get_LaBa_address(cot, addr);
        playLaBa(LABA_YI_GONG_YOU);
        nrf_delay_ms(900);
        for (int i = 0; i < adrCnt; i++)
        {
            playLaBa(addr[i]);
            nrf_delay_ms(500);
        }
        playLaBa(LABA_MING_GUANLIYUAN);
        nrf_delay_ms(1600);

        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else if (gInput[0] == '2')
    {
        int16_t cot = tethys_how_many_user();
        uint8_t adrCnt = get_LaBa_address(cot, addr);
        playLaBa(LABA_YI_GONG_YOU);
        nrf_delay_ms(800);
        for (int i = 0; i < adrCnt; i++)
        {
            playLaBa(addr[i]);
            nrf_delay_ms(500);
        }
        playLaBa(LABA_GE_PUTONG_YONGHU);
        nrf_delay_ms(1600);
        gSettingState = TETHYS_SET_STATE_0;
        ttp220_return_idle(2000);
    }
    else
    {
        playLaBa(LABA_ERROR);
    }
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2_1(void)
{
    //No use now
}
void ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2_2(void)
{
    //No use now
}
#endif

void ttp229_resolve_password(void)
{
    //LEDS_ON(BSP_LED_1_MASK);
    switch(gSettingState)
    {
    case TETHYS_SET_STATE_0:
        ttp229_resolve_password_state_0();
        break;
    case TETHYS_SET_STATE_1:
        ttp229_resolve_password_state_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_2();
        break;

    case TETHYS_SET_STATE_SETTING_MAIN_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_3();
        break;
#if 0
    case TETHYS_SET_STATE_SETTING_MAIN_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_2_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_2_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_3:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_3_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_3_3_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_3_3_2();
        break;

    case TETHYS_SET_STATE_SETTING_MAIN_4:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_1_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_1_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_1_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_2_1:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2_1();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_4_2_2:
        ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_4_2_2();
        break;
#endif
    default:
        break;
    }
    memset(gInput, 0, sizeof(gInput));
    gInputIdx = 0;
}

void ttp229_return_last_menu(void)
{
    switch(gSettingState)
    {
    case TETHYS_SET_STATE_0:
        //ttp229_resolve_password_state_0();
        break;
    case TETHYS_SET_STATE_1:
        //ttp229_resolve_password_state_1();
        //gSettingState = TETHYS_SET_STATE_0;
        //led_DH_return_to_idle();
        //ttp220_return_idle(100);
        ttp229_timeout();
        break;
    case TETHYS_SET_STATE_SETTING_MAIN:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN();
        {
        gSettingState = TETHYS_SET_STATE_1;
        uint8_t streams[4] = {LABA_GUAN_LI_YUAN_SHE_ZHI, 160,
                              LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA, 255
                             };
        playLaBaMany(streams, 2);
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1();
        {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_resolve_state_1_sound(streams, &len);
        playLaBaMany(streams, len);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 80,
                               LABA_XIU_GAI_GUAN_LI_YUAN, 150,//修改管理员
                               LABA_3, 80,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 80,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 4);
        #endif

        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_1_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_1_1();
        {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 80,
                               LABA_XIU_GAI_GUAN_LI_YUAN, 150,//修改管理员
                               LABA_3, 80,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 80,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_2_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_2_1();
        {
            uint8_t streams[16] = {LABA_QING_SHU_RU_XIUGAI_BIANHAO, 160,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 90,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 90,
                               LABA_XIU_GAI_GUAN_LI_YUAN, 150,//修改管理员
                               LABA_3, 90,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 90,
                               LABA_LU_RU_GUAN_LI_YUAN, 150, //录入管理员
                               LABA_2, 90,
                               LABA_SHAN_CHU_GUAN_LI_YUAN, 170//删除管理员
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1();
        {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 150, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU, 180,//删除全部
                              };
        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1();
        {
        uint8_t streams[16] = {LABA_QING_SHURU_SHANCHU_BIANHAO, 170};

        playLaBaMany(streams, 1);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_1_3_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_1_3_2();
        {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 150, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU, 180,//删除全部
                              };
        playLaBaMany(streams, 4);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_1_3;
        }
        break;
//////////////////////////////////////////////////////////////////////////////////////////////////
    case TETHYS_SET_STATE_SETTING_MAIN_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2();
        {
        uint8_t streams[32] = {0};
        uint8_t len;
        ttp229_get_resolve_state_1_sound(streams, &len);
        playLaBaMany(streams, len);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_XIUGAI_PUTONG_YONGHU, 150,//修改普通用户
                               LABA_3, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190
                              };//删除普通用户
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190,//删除普通用户
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_1_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_1_1();
        {
        uint8_t streams[16] = {LABA_QING_SHU_RU_LU_RU_BIAN_HAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_XIUGAI_PUTONG_YONGHU, 150,//修改普通用户
                               LABA_3, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190
                              };//删除普通用户
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190,//删除普通用户
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_2_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_2_1();
        {
        uint8_t streams[16] = {LABA_QING_SHU_RU_XIUGAI_BIANHAO, 180,
            LABA_QUEREN_QINGAN_HUICHEJIAN, 191};

        playLaBaMany(streams, 2);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3();
        {
        #ifdef TETHYS_XIUGAI_GUANLIYUAN
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_XIUGAI_PUTONG_YONGHU, 150,//修改普通用户
                               LABA_3, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190
                              };//删除普通用户
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_LURU_PUTONG_YONGHU, 150, //录入普通用户
                               LABA_2, 80,
                               LABA_SHANCHU_PUTONG_YONGHU, 190,//删除普通用户
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1();
        {
        #ifdef TETHYS_AN_BIANHAO_SHANCHU
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_AN_LEIXING_SHANCHU, 180,//按类型删除
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1();
        {
        uint8_t streams[16] = {LABA_QING_SHURU_SHANCHU_BIANHAO, 170};
        playLaBaMany(streams, 1);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_1;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2();
        {
        #ifdef TETHYS_AN_BIANHAO_SHANCHU
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_AN_LEIXING_SHANCHU, 180,//按类型删除
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1();
        {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_SHANCHU_QUANBU_KAPIAN, 180,
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU_MIMA, 180,
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU_ZHIWEN, 180
                              };
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2();
        {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_SHANCHU_QUANBU_KAPIAN, 180,
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU_MIMA, 180,
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU_ZHIWEN, 180
                              };
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3();
        {
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_SHANCHU_QUANBU_KAPIAN, 180,
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU_MIMA, 180,
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU_ZHIWEN, 180
                              };
        playLaBaMany(streams, 6);
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3_2;
        }
        break;
    case TETHYS_SET_STATE_SETTING_MAIN_2_3_3:
        //ttp229_resolve_TETHYS_SET_STATE_SETTING_MAIN_2_3_3();
        {
        #ifdef TETHYS_AN_BIANHAO_SHANCHU
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_AN_LEIXING_SHANCHU, 180,
                               LABA_3, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 6);
        #else
        uint8_t streams[16] = {LABA_1, 80,
                               LABA_AN_BIANHAO_SHANCHU, 180, //按编号删除
                               LABA_2, 80,
                               LABA_SHANCHU_QUANBU1, 180//删除全部
                              };
        playLaBaMany(streams, 4);
        #endif
        gSettingState = TETHYS_SET_STATE_SETTING_MAIN_2_3;
        }
        break;
    default:
        break;
    }
}

#if 0
void ttp229_resolve_password(void)
{
#if 0
    uint16_t pagesize = NRF_FICR->CODEPAGESIZE;
    memcpy(m_beacon_info + BEACON_UUID_OFFSET, &pagesize, 2);
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    advertising_init_beacon();
#endif
    memset(gInput, 0, sizeof(gInput));
    gInputIdx = 0;
}
#endif

