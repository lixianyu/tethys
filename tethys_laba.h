#ifndef __TETHYS_LABA_H__
#define __TETHYS_LABA_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>

#define LABA_YI_KAI_SUO	0X01	//1	已开锁		
#define LABA_YI_GUAN_SUO 0X02	//2	已关锁		
#define LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN 0X03	//3	请重新输入指纹		
#define LABA_QING_SHU_RU_ZHI_WEN 0X04	//4	请输入指纹		
#define LABA_QING_BA_KA_PIAN_KAO_JIN 0X05	//5	请把卡片靠近		
#define LABA_QING_SHU_RU_KAI_SUO_XIN_XI  0X06	//6	请输入开锁信息		
#define LABA_SHAN_CHU_CHENG_GONG 0X07	//7	删除成功		
#define LABA_JIN_RU_CHU_SHI_HUA 0X08	//8	进入初始化		
#define LABA_DING_DONG 0X09	//9	发出“叮咚”的声音		
#define LABA_QING_XIA_YA_BA_SHOU_KAI_MEN 0X0A	//10	请下压把手开门		
#define LABA_DIAN_LIANG_BU_ZU 0X0B	//11	电量不足，请更换电池		
#define LABA_BIAN_HAO_CUO_WU 0X0C	//12	编号错误		
#define LABA_ZHI 0X0D	//13	至		
#define LABA_DAO 0X0E	//14	到		
#define LABA_SHU_RU_YOU_WU 0X0F	//15	输入有误		
#define LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN 0X10	//16	请再次输入指纹		
#define LABA_AN_JIAN_YIN_1 0X11	//17	830文件名		
#define LABA_AN_JIAN_YIN_2 0X12	//18	587文件名		
#define LABA_GUAN_LI_YUAN_SHE_ZHI 0X13	//19	管理员设置		
#define LABA_PU_TONG_YONG_HU_SHE_ZHI 0X14	//20	普通用户设置		
#define LABA_XI_TONG_SHE_ZHI 0X15	//21	系统设置		
#define LABA_SHU_JU_TONG_JI 0X16	//22	数据统计		
#define LABA_LU_RU_GUAN_LI_YUAN 0X17	//23	录入管理员		
#define LABA_XIU_GAI_GUAN_LI_YUAN 0X18	//24	修改管理员		
#define LABA_SHAN_CHU_GUAN_LI_YUAN 0X19	//25	删除管理员		
#define LABA_QING_SHU_RU_LU_RU_BIAN_HAO 0X1A	//26	请输入录入编号		
#define LABA_QING_SHU_RU_GUAN_LI_YUAN_BIAN_HAO 0X1B	//27	请输入管理员编号		
#define LABA_QING_SHU_RU_MIMA_ZHIWEN_OR_SHUA_KA 0X1C	//28	请输入密码、指纹或者刷卡		
#define LABA_QING_SHU_RU_XIUGAI_BIANHAO 0X1D	//29	请输入修改编号		
#define LABA_AN_BIANHAO_SHANCHU 0X1E	//30	按编号删除		
#define LABA_SHANCHU_QUANBU 0X1F	//31	删除全部		
#define LABA_QING_SHURU_SHANCHU_BIANHAO 0X20	//32	请输入删除编号		
#define LABA_LURU_PUTONG_YONGHU 0X21	//33	录入普通用户		
#define LABA_XIUGAI_PUTONG_YONGHU 0X22	//34	修改普通用户		
#define LABA_SHANCHU_PUTONG_YONGHU 0X23	//35	删除普通用户		
#define LABA_QING_SHURU_PUTONG_YONGHU_BIANHAO 0X24	//36	请输入普通用户编号		
#define LABA_AN_LEIXING_SHANCHU 0X25	//37	按类型删除		
#define LABA_SHANCHU_QUANBU1 0X26	//38	删除全部		
#define LABA_SHANCHU_QUANBU_KAPIAN 0X27	//39	删除全部卡片		
#define LABA_SHANCHU_QUANBU_MIMA 0X28	//40	删除全部密码		
#define LABA_SHANCHU_QUANBU_ZHIWEN 0X29	//41	删除全部指纹		
#define LABA_QUEREN_QINGAN_HUICHEJIAN 0X2A	//42	确认请按回车键		
#define LABA_SHIJIAN_SHEZHI 0X2B	//43	时间设置		
#define LABA_YANZHENG_MOSHI_SHEZHI 0X2C	//44	验证模式设置		
#define LABA_CHANGKAI_GONGNENG_SHEZHI 0X2D	//45	常开功能设置		
#define LABA_QING_SHURU_YEAR_MONTH_DAY_HOUR_MINUTE 0X2E	//46	请输入年、月、日、小时、分钟		
#define LABA_YIGONG_12_GE_NUMBER 0X2F	//47	一共十二个数字		
#define LABA_YEAR 0X30	//48	年		
#define LABA_MONTH 0X31	//49	月		
#define LABA_DAY 0X32	//50	日		
#define LABA_DIAN 0X33	//51	点		
#define LABA_MINUTE 0X34	//52	分		
#define LABA_QIDONG_DANKAI_MOSHI 0X35	//53	启动单开模式		
#define LABA_QIDONG_SHUANG_KAI_MOSHI 0X36	//54	启动双开模式		
#define LABA_OPEN_CHANGKAI_GONGNENG 0X37	//55	打开常开功能		
#define LABA_CLOSE_CHANGKAI_GONGNENG 0X38	//56	关闭常开功能		
#define LABA_KAIMEN_JILU_CHAXUN 0X39	//57	开门记录查询		
#define LABA_CUNCHU_XINXI_TONGJI 0X3A	//58	存储信息统计		
#define LABA_AN_SHUNXU_CHAXUN 0X3B	//59	按顺序查询		
#define LABA_AN_SHIJIAN_CHAXUN 0X3C	//60	按时间查询		
#define LABA_GUANLIYUAN_TONGJI 0X3D	//61	管理员统计		
#define LABA_PUTONG_YONGHU_TONGJI 0X3E	//62	普通用户统计		
#define LABA_1 0X3F	//63	1		
#define LABA_2 0X40	//64	2		
#define LABA_3 0X41	//65	3		
#define LABA_4 0X42	//66	4		
#define LABA_5 0X43	//67	5		
#define LABA_6 0X44	//68	6		
#define LABA_7 0X45	//69	7		
#define LABA_8 0X46	//70	8		
#define LABA_9 0X47	//71	9		
#define LABA_0 0X48	//72	0		
#define LABA_10 0X49	//73	十		
#define LABA_AN_HUICHEJIAN_QUEREN 0X4A	//74	请按回车键确认		
#define LABA_KAISUO_MOSHI_SHEZHI 0X4B	//75	开锁模式设置		
#define LABA_DANKAI_MOSHI_QIDONG_CHENGGONG 0X4C	//76	单开模式启动成功		
#define LABA_SHUANGKAI_MOSHI_QIDONG_CHENGGONG 0X4D	//77	双开模式启动成功		
#define LABA_CHANGKAI_GONGNENG_YI_DAKAI 0X4E	//78	常开功能已打开		
#define LABA_CHANGKAI_GONGNENG_YI_GUANBI 0X4F	//79	常开功能已关闭		
#define LABA_YI_GONG_YOU 0X50	//80	一共有		
#define LABA_MING_GUANLIYUAN 0X51	//81	名管理员		
#define LABA_MING_PUTONG_YONGHU 0X52	//82	名普通用户		
#define LABA_GE_GUANLIYUAN 0X53	//83	个管理员		
#define LABA_GE_PUTONG_YONGHU 0X54	//84	个普通用户		
#define LABA_CHENG_GONG 0X55	//85	成功		
#define LABA_XI_TONG_DANGQIAN_SHIJIAN_WEI 0X56	//86	系统当前时间为		
#define LABA_SHIJIAN_SHEZHI_CHENGGONG 0X57	//87	时间设置成功		
#define LABA_QUXIAO_QIANGAN_FANHUIJIAN 0X58	//88	取消请按返回键		
#define LABA_QINGAN_FANHUIJIAN_QUXIAO 0X59	//89	请按返回键取消		
#define LABA_XIUGAI_CHENGGONG 0X5A	//90	修改成功		
#define LABA_AN_HUICHEJIAN 0X5B	//91	按回车键		
#define LABA_JI_XU 0X5C	//92	继续		
#define LABA_AN_FANHUIJIAN 0X5D	//93	按返回键		
#define LABA_QU_XIAO 0X5E	//94	取消		
#define LABA_LURU_CHENGGONG 0X5F	//95	录入成功		
#define LABA_BIAN_HAO 0X60	//96	编号		
#define LABA_GAI_BIANHAO_YI_BEI_SHIYONG 0X61	//97	该编号已被使用		
#define LABA_QING_CHONGXIN_SHURU 0X62	//98	请重新输入		
#define LABA_ZHI_WEN 0X63	//99	指纹		
#define LABA_MI_MA 0X64	//100	密码		
#define LABA_KA_PIAN 0X65	//101	卡片		
#define LABA_LU_RU 0X66	//102	录入		
#define LABA_SHI_BAI 0X67	//103	失败		
#define LABA_GUANLIYUAN_BIANHAO_FANWEI_SHI 0X68	//104	管理员编号范围是		
#define LABA_PUTONG_YONGHU_BIANHAO_FANWEI_SHI 0X69	//105	普通用户编号范围是		

#define LABA_ERROR LABA_SHU_RU_YOU_WU
#define LABA_SHU_1 LABA_AN_JIAN_YIN_1
#define LABA_SHU_2 LABA_AN_JIAN_YIN_1
#define LABA_SHU_3 LABA_AN_JIAN_YIN_1
#define LABA_SHU_4 LABA_AN_JIAN_YIN_1
#define LABA_SHU_5 LABA_AN_JIAN_YIN_1
#define LABA_SHU_6 LABA_AN_JIAN_YIN_1
#define LABA_SHU_7 LABA_AN_JIAN_YIN_1
#define LABA_SHU_8 LABA_AN_JIAN_YIN_1
#define LABA_SHU_9 LABA_AN_JIAN_YIN_1
#define LABA_SHU_0 LABA_AN_JIAN_YIN_1
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_LaBa(void);
extern void testLaBa(void);
extern void playLaBa(uint8_t adrr);
extern void playLaBaMany(uint8_t *addrs, uint8_t len);
extern void stopLaBaMany(void);
extern void closeAmplifier(void);
extern int8_t isLabaPlaying(void);
#endif /* __MIMAS_RUN_IN_RAM_H__ */
