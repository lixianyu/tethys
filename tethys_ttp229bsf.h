#ifndef __TETHYS_TTP229BSF_H__
#define __TETHYS_TTP229BSF_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>

typedef enum
{
    TETHYS_SET_STATE_0 = 0,
    TETHYS_SET_STATE_1,
    TETHYS_SET_STATE_SETTING_MAIN,
    
    TETHYS_SET_STATE_SETTING_MAIN_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_2,
    TETHYS_SET_STATE_SETTING_MAIN_1_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_3,
    TETHYS_SET_STATE_SETTING_MAIN_1_3_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_3_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_1_3_2,
    
    TETHYS_SET_STATE_SETTING_MAIN_2,
    TETHYS_SET_STATE_SETTING_MAIN_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_2,
    TETHYS_SET_STATE_SETTING_MAIN_2_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_3,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_2,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_2_2,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_2_3,
    TETHYS_SET_STATE_SETTING_MAIN_2_3_3,
    
    TETHYS_SET_STATE_SETTING_MAIN_3,
    TETHYS_SET_STATE_SETTING_MAIN_3_1,
    TETHYS_SET_STATE_SETTING_MAIN_3_2,
    TETHYS_SET_STATE_SETTING_MAIN_3_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_3_2_2,
    TETHYS_SET_STATE_SETTING_MAIN_3_3,
    TETHYS_SET_STATE_SETTING_MAIN_3_3_1,
    TETHYS_SET_STATE_SETTING_MAIN_3_3_2,
    
    TETHYS_SET_STATE_SETTING_MAIN_4,
    TETHYS_SET_STATE_SETTING_MAIN_4_1,
    TETHYS_SET_STATE_SETTING_MAIN_4_1_1,
    TETHYS_SET_STATE_SETTING_MAIN_4_1_2,
    TETHYS_SET_STATE_SETTING_MAIN_4_2,
    TETHYS_SET_STATE_SETTING_MAIN_4_2_1,
    TETHYS_SET_STATE_SETTING_MAIN_4_2_2,

    TETHYS_SET_BLUETOOTH,
    TETHYS_SET_RF,
    TETHYS_SET_STATE_NOTHING,
}setting_state_t;
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_ttp229bsf(void);
extern void testttp229bsf(void);
extern void ttp229_enable(void);
extern void ttp229_disable(void);
extern setting_state_t getTTP_state(void);
extern void setTTP_state(setting_state_t state);
extern void ttp229_reset_timeout_timer(void);
extern uint16_t getCurID(void);
extern void ttp220_return_idle(uint32_t ms);
extern void ttp229_resolve_fingerprint_state_1(uint8_t flag);
extern void ttp229_resolve_RFID_state_1(uint8_t flag);
extern void ttp229_return_re_enter_luru(uint8_t where, uint32_t ms);
extern int8_t ttp229_is_correct_state_for_fingerprint(void);
extern int8_t ttp229_is_correct_state_for_RC522(void);
#endif
