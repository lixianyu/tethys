/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_battery.h"
#include "tethys_laba.h"
#include "tethys_ttp229bsf.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_adc.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"
#include "mimas_bsp.h"

extern bool gYiKaiSuo;
APP_TIMER_DEF(m_tethys_battery_timer_id);
static void tethys_battery_timeout_handler(void *p_context);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/

/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void init_battery(void)
{
    app_timer_create(&m_tethys_battery_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     tethys_battery_timeout_handler);
    app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(10000, 0), NULL);
}

__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}

void tethys_battery_timeout_handler(void *p_context)
{
    if (getTTP_state() != TETHYS_SET_STATE_0)
    {
        goto DoNotMeasureBatteryNOW;
    }
    if (0 != nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        goto DoNotMeasureBatteryNOW;
    }
    if (0 != nrf_gpio_pin_state(NRSTPD_PIN))
    {
        goto DoNotMeasureBatteryNOW;
    }
    if (!LED_IS_ON(LEDS_ROL_MASK))
    {
        goto DoNotMeasureBatteryNOW;
    }
    if (LED_IS_ON(LEDS_COL_MASK))
    {
        goto DoNotMeasureBatteryNOW;
    }
    nrf_adc_start();
    return;
DoNotMeasureBatteryNOW:
    app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(10000, 0), NULL);
}

void tethys_battery_calculate(void)
{//刚关闭锁时，电压低，所以得等一会儿
    app_timer_stop(m_tethys_battery_timer_id);
    app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(10000, 0), NULL);
}
