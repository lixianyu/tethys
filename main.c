/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_app_proximity_main main.c
 * @{
 * @ingroup ble_sdk_app_proximity_eval
 * @brief Proximity Application main file.
 *
 * This file contains is the source code for a sample proximity application using the
 * Immediate Alert, Link Loss and Tx Power services.
 *
 * This application would accept pairing requests from any peer device.
 *
 * It demonstrates the use of fast and slow advertising intervals.
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_soc.h"
#include "nrf_adc.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_radio_notification.h"
#include "ble_dis.h"
#include "ble_tps.h"
#ifdef BLE_DFU_APP_SUPPORT
#include "ble_dfu.h"
#include "dfu_app_handler.h"
#endif // BLE_DFU_APP_SUPPORT
#include "tw_ble_ias.h"
#include "ble_lls.h"
#include "ble_bas.h"
#include "mimas_ble_nus.h"
#include "tethys_ble_nus.h"
#include "ble_conn_params.h"
#include "mimas_ble_debug_assert_handler.h"
#include "sensorsim.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "ble_ias_c.h"
#include "app_util.h"
#include "pstorage.h"
#include "app_trace.h"
#include "mimas_bsp.h"
#include "mimas_bsp_btn_ble.h"
#include "app_uart.h"
#include "app_pwm.h"
#ifndef NRF51
#include "nrf_drv_saadc.h"
#endif //NRF51
#include "ADXL362.h"
#include "nrf_delay.h"
#include "bd_communicate_protocol.h"
#include "app_timer_appsh.h"
#include "app_scheduler.h"
#include "nrf_drv_wdt.h"
#include "bd_factory_test.h"
#include "bd_wall_clock_timer.h"
#include "tw_nrf_event_string.h"
#include "mimas_log.h"
#include "bd_private_bond.h"
#include "bd_sync_data.h"
#include "mimas_config.h"
#include "nrf_drv_gpiote.h"
#include "mimas_run_in_ram.h"
#include "tethys_laba.h"
#include "tethys_mfrc522.h"
#include "tethys_storage.h"
#include "tethys_ttp229bsf.h"
#include "tethys_fingerprint.h"
#include "tethys_moto.h"
#include "tethys_led.h"
#include "tethys_battery.h"

extern uint8_t gNiuBi;
extern void tethys_set_login_state(bool flag);
extern bool tethys_get_login_state(void);
extern void led_donghua_alert(void);
extern void tethys_reset_openLockTimes(void);
extern uint8_t tethys_get_OTA(void);

#define IS_SRVC_CHANGED_CHARACT_PRESENT     1                                            /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define SIGNAL_ALERT_BUTTON_ID              0                                            /**< Button used for send or cancel High Alert to the peer. */
#define STOP_ALERTING_BUTTON_ID             1                                            /**< Button used for clearing the Alert LED that may be blinking or turned ON because of alerts from the central. */

#define HW_REV_STR                      "0.1"
#define FW_REV_STR                      "110"
#define MANUFACTURER_NAME               "TOWEER"                      /**< Manufacturer. Will be passed to Device Information Service. */
#define MODEL_NUM                       "AA"                           /**< Model number. Will be passed to Device Information Service. */
#define MANUFACTURER_ID                 0x1122334455                               /**< Manufacturer ID, part of System ID. Will be passed to Device Information Service. */
#define ORG_UNIQUE_ID                   0x667788                                   /**< Organizational Unique ID, part of System ID. Will be passed to Device Information Service. */
#if 0
#define DEVICE_NAME                         "Tethys"  /**< Name of device. Will be included in the advertising data. */
#else
static uint8_t gDeviceName[21] = "Tethys";
#endif
#define APP_TIMER_PRESCALER                 0                                            /**< Value of the RTC1 PRESCALER register. */

#define ADXL362_INTERVAL          APP_TIMER_TICKS(999, APP_TIMER_PRESCALER)

#define ADV_LED_ON_TIME                     APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)    /**< Advertisement LED ON period when in blinking state. */
#define ADV_LED_OFF_TIME                    APP_TIMER_TICKS(900, APP_TIMER_PRESCALER)    /**< Advertisement LED OFF period when in blinking state. */

#define MILD_ALERT_LED_ON_TIME              APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)    /**< Alert LED ON period when in blinking state. */
#define MILD_ALERT_LED_OFF_TIME             APP_TIMER_TICKS(900, APP_TIMER_PRESCALER)    /**< Alert LED OFF period when in blinking state. */


#define SEC_PARAM_BOND                      1                                            /**< Perform bonding. */
#ifdef TETHYS_PAIR
#define SEC_PARAM_MITM                      1                                            /**< Man In The Middle protection required. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_DISPLAY_ONLY
#else
#define SEC_PARAM_MITM                      0                                            /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_NONE                         /**< No I/O capabilities. */

#endif
#define SEC_PARAM_OOB                       0                                            /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE              7                                            /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE              16                                           /**< Maximum encryption key size. */

#define INITIAL_LLS_ALERT_LEVEL             BLE_CHAR_ALERT_LEVEL_NO_ALERT                /**< Initial value for the Alert Level characteristic in the Link Loss service. */

#define ADC_REF_VOLTAGE_IN_MILLIVOLTS       600                                          /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION        6                                            /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS      270                                          /**< Typical forward voltage drop of the diode (Part no: SD103ATW-7-F) that is connected in series with the voltage supply. This is the voltage drop when the forward current is 1mA. Source: Data sheet of 'SURFACE MOUNT SCHOTTKY BARRIER DIODE ARRAY' available at www.diodes.com. */

#define DEAD_BEEF                           0xDEADBEEF                                   /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS   1200                                         /**< Value in millivolts for voltage used as reference in ADC conversion on NRF51. */
#define ADC_INPUT_PRESCALER                 3                                            /**< Input prescaler for ADC convestion on NRF51. */
#define ADC_RES_10BIT                       1024                                         /**< Maximum digital value for 10-bit ADC conversion. */

/**@brief Macro to convert the result of ADC conversion in millivolts.
 *
 * @param[in]  ADC_VALUE   ADC result.
 *
 * @retval     Result converted to millivolts.
 */
#ifdef TETHYS_ADC
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER / 2)
#else
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)
#endif // NRF51

#ifdef BLE_DFU_APP_SUPPORT
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */

STATIC_ASSERT(IS_SRVC_CHANGED_CHARACT_PRESENT);                                     /** When having DFU Service support in application the Service Changed Characteristic should always be present. */
#endif // BLE_DFU_APP_SUPPORT


/**@brief Advertisement states. */
typedef enum
{
    BLE_NO_ADV,                                                                      /**< No advertising running. */
    BLE_FAST_ADV_WHITELIST,                                                          /**< Advertising with whitelist. */
    BLE_FAST_ADV,                                                                    /**< Fast advertising running. */
    BLE_SLOW_ADV,                                                                    /**< Slow advertising running. */
    BLE_SLEEP,                                                                       /**< Go to system-off. */
    BLE_FOREVER_LED,
    BLE_FOREVER
} ble_advertising_mode_t;

#ifdef TETHYS_PAIR
static ble_opt_t   m_static_pin_option;
#endif
#if 0
typedef enum
{
    ADXL362_WORK_MODE_1,
    ADXL362_WORK_MODE_2,
    ADXL362_WORK_MODE_3,
} adxl362_work_mode_t;
adxl362_work_mode_t g_adxl362_work_mode = ADXL362_WORK_MODE_1;
#else
uint8_t g_adxl362_work_mode = 0;
#endif
static ble_tps_t                        m_tps;                                       /**< Structure used to identify the TX Power service. */
static ble_ias_t                        m_ias;                                       /**< Structure used to identify the Immediate Alert service. */
static ble_lls_t                        m_lls;                                       /**< Structure used to identify the Link Loss service. */

static ble_bas_t                        m_bas;                                       /**< Structure used to identify the battery service. */
static ble_ias_c_t                      m_ias_c;                                     /**< Structure used to identify the client to the Immediate Alert Service at peer. */

static uint8_t m_advertising_mode = BLE_FOREVER_LED; /**< Variable to keep track of when we are advertising. */
uint16_t                         global_connect_handle = BLE_CONN_HANDLE_INVALID;     /**< Handle of the current connection. */

static volatile bool                    m_is_high_alert_signalled;                   /**< Variable to indicate whether a high alert has been signalled to the peer. */
static volatile bool                    m_is_ias_present = false;                    /**< Variable to indicate whether the immediate alert service has been discovered at the connected peer. */
ble_nus_t m_nus;            /**< Structure to identify the Nordic UART Service. */
tethys_ble_nus_t m_tethys_nus;
APP_TIMER_DEF(m_alert_off_timer_id);
APP_TIMER_DEF(m_battery_timer_id);                                                   /**< Battery measurement timer. */
#ifdef ADXL362_OPEN
#ifdef ADXL362_AS_BUTTON_LONG_PUSH
static uint8_t gADXL362_adv = 0;
static uint8_t gADXL362_adv_count = 0;
APP_TIMER_DEF(m_adxl362_adv_timer_id);
#endif
APP_TIMER_DEF(m_adxl362_timer_id);                                                   /**< Test ADXL362 timer. */
#endif
APP_TIMER_DEF(m_test_task_timer_id); // To test something.
APP_TIMER_DEF(m_temperature_timer_id);
APP_TIMER_DEF(m_reset_timer_id);
APP_TIMER_DEF(m_tethys_login_timer_id);
#ifdef BUZZER_OPEN
static uint8_t gBeepCount = 4;
APP_TIMER_DEF(m_buzzer_timer_id);
static uint8_t gBuzzerState = 0;
static uint8_t gBuzzerCount = 0;
//static uint8_t gDutyCycle = 0;
//static uint8_t gDutyCycleI = 1;
#endif
static uint8_t g_timer_task;
static dm_application_instance_t        m_app_handle;                                /**< Application identifier allocated by device manager */

static bool                             m_memory_access_in_progress = false;         /**< Flag to keep track of ongoing operations on persistent memory. */
pstorage_handle_t       g_storage_handle;
int8_t g_device_tx_power = TX_POWER_LEVEL;
static uint8_t g_last_temp = 0;
static uint8_t g_last_batt = 0;
    
static void on_ias_evt(ble_ias_t *p_ias, ble_ias_evt_t *p_evt);
static void on_lls_evt(ble_lls_t *p_lls, ble_lls_evt_t *p_evt);
static void on_ias_c_evt(ble_ias_c_t *p_lls, ble_ias_c_evt_t *p_evt);
static void on_bas_evt(ble_bas_t *p_bas, ble_bas_evt_t *p_evt);
//static void advertising_init(uint8_t adv_flags);
static void show_battery_level(void);
static __INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts);
static void alert_signal(uint8_t alert_level);
#ifdef OBFUSCATING_ALGORITHMS
static void obfuscating_algorithms(void);
#endif
#ifdef BUZZER_OPEN
static void buzzer_stop(void);
#endif
#ifdef BLE_DFU_APP_SUPPORT
static ble_dfu_t                         m_dfus;                                    /**< Structure used to identify the DFU service. */
#endif // BLE_DFU_APP_SUPPORT   

#ifdef NRF52
static nrf_saadc_value_t adc_buf[2];
#endif //NRF52

//extern uint8_t should_update_prv_bond_info;
uint8_t g_pstorage_state = PSTORAGE_STATE_UPDATE_IDLE;
uint8_t g_pstorage_buffer[32];
uint16_t g_interval = 3800;//APP_ADV_INTERVAL_SLOW;// 4800 x 0.625 = 3s.
#ifdef TETHYS_PAIR
uint8_t gPasskey[6] = "123456";
#endif

#if 1
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name)
{
#ifndef DEBUG
    LOG("line_num = %u, filename=%s,error_code=0x%x", line_num, p_file_name, error_code);
    //LEDS_ON(LEDS_MASK);

    //for (;;)
    {
        LOG("line_num = %u, filename=%s,error_code=0x%x", line_num, p_file_name, error_code);
        nrf_delay_ms(100);

        //LEDS_INVERT(LEDS_MASK);
    }
#else
    //LOG("line_num = %u, filename=%s,error_code=0x%x", line_num,p_file_name,error_code);
    ble_debug_assert_handler(error_code, line_num, p_file_name);
#endif
}
#endif
/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name)
{
    LOG_ENTER;
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for handling Service errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in] nrf_error   Error code containing information about what went wrong.
 */
static void service_error_handler(uint32_t nrf_error)
{
    LOG_ENTER;
    APP_ERROR_HANDLER(nrf_error);
}

static int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

uint16_t g_battery_voltage_mv;
/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
#ifdef TETHYS_ADC
void ADC_IRQHandler(void)
{
    uint32_t err_code;
    uint16_t adc_value;
    uint8_t  percentage_batt_lvl;
    uint16_t batt_lvl_in_milli_volts;
    LOG_ENTER;
    nrf_adc_conversion_event_clean();
    nrf_adc_stop();

    adc_value = nrf_adc_result_get();


    batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);// 0 ~ 1800mv
    g_battery_voltage_mv = map(batt_lvl_in_milli_volts, 1, 1800, 1, 6660);//0 ~ 6660mv

    percentage_batt_lvl     = batteryLevelInPercent(g_battery_voltage_mv);

    err_code = ble_bas_battery_level_update(&m_bas, percentage_batt_lvl);
    if (
        (err_code != NRF_SUCCESS)
        &&
        (err_code != NRF_ERROR_INVALID_STATE)
        &&
        (err_code != BLE_ERROR_NO_TX_BUFFERS)
        &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    )
    {
        APP_ERROR_HANDLER(err_code);
    }
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)show_battery_level);
}
#else
void ADC_IRQHandler(void)
{
    uint32_t err_code;
    uint16_t adc_value;
    uint8_t  percentage_batt_lvl;
    uint16_t batt_lvl_in_milli_volts;
    LOG_ENTER;
    nrf_adc_conversion_event_clean();
    nrf_adc_stop();

    adc_value = nrf_adc_result_get();


    batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);
    g_battery_voltage_mv = batt_lvl_in_milli_volts;

    percentage_batt_lvl     = batteryLevelInPercent(batt_lvl_in_milli_volts);

    err_code = ble_bas_battery_level_update(&m_bas, percentage_batt_lvl);
    if (
        (err_code != NRF_SUCCESS)
        &&
        (err_code != NRF_ERROR_INVALID_STATE)
        &&
        (err_code != BLE_ERROR_NO_TX_BUFFERS)
        &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    )
    {
        APP_ERROR_HANDLER(err_code);
    }
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)show_battery_level);
}
#endif

/**@brief Function for configuring ADC to do battery level conversion.
 */
#ifdef TETHYS_ADC
static void adc_configure(void)
{
    //LOG_ENTER;
    nrf_adc_config_t adc_config;
    adc_config.resolution = NRF_ADC_CONFIG_RES_10BIT;
    adc_config.scaling = NRF_ADC_CONFIG_SCALING_INPUT_TWO_THIRDS;
    adc_config.reference = NRF_ADC_CONFIG_REF_VBG;
    nrf_adc_configure(&adc_config);
    nrf_adc_int_enable(ADC_INTENSET_END_Msk);
    NVIC_EnableIRQ(ADC_IRQn);
    NVIC_SetPriority(ADC_IRQn, 3);
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_2);
}
#else
static void adc_configure(void)
{
    //LOG_ENTER;
#ifdef NRF51
    nrf_adc_config_t adc_config = NRF_ADC_CONFIG_DEFAULT;
    adc_config.scaling = NRF_ADC_CONFIG_SCALING_SUPPLY_ONE_THIRD;
    nrf_adc_configure(&adc_config);
    nrf_adc_int_enable(ADC_INTENSET_END_Msk);
    NVIC_EnableIRQ(ADC_IRQn);
    NVIC_SetPriority(ADC_IRQn, 3);
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_DISABLED);
#else //  NRF52
    ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);
    err_code = nrf_drv_saadc_channel_init(0, &config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
    APP_ERROR_CHECK(err_code);
#endif //NRF51
}
#endif

/**@brief Function for starting advertising.
 */
static void advertising_start(uint8_t type)
{
    uint32_t             err_code;
    ble_gap_adv_params_t adv_params;
    ble_gap_whitelist_t  whitelist;
    uint32_t             count;

    //LOG_ENTER;
    // Verify if there is any flash access pending, if yes delay starting advertising until
    // it's complete.
    err_code = pstorage_access_status_get(&count);
    APP_ERROR_CHECK(err_code);

    if (count != 0)
    {
        m_memory_access_in_progress = true;
        return;
    }

    // Initialize advertising parameters with defaults values
    memset(&adv_params, 0, sizeof(adv_params));

    #ifdef BUTTON_SWITCH_ADV_STATE
    adv_params.type        = type;
    #else
    //adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    #endif
    adv_params.p_peer_addr = NULL;
    adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    adv_params.p_whitelist = NULL;

    // Configure advertisement according to current advertising state
    switch (m_advertising_mode)
    {
    case BLE_NO_ADV:
        m_advertising_mode = BLE_FAST_ADV_WHITELIST;
    // fall through.

    case BLE_FAST_ADV_WHITELIST:
    {
        ble_gap_addr_t        *p_whitelist_addr[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
        ble_gap_irk_t         *p_whitelist_irk[BLE_GAP_WHITELIST_IRK_MAX_COUNT];

        whitelist.addr_count = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
        whitelist.irk_count  = BLE_GAP_WHITELIST_IRK_MAX_COUNT;
        whitelist.pp_addrs   = p_whitelist_addr;
        whitelist.pp_irks    = p_whitelist_irk;

        err_code = dm_whitelist_create(&m_app_handle, &whitelist);
        APP_ERROR_CHECK(err_code);

        if ((whitelist.addr_count != 0) || (whitelist.irk_count != 0))
        {
            adv_params.fp          = BLE_GAP_ADV_FP_FILTER_CONNREQ;
            adv_params.p_whitelist = &whitelist;

            //advertising_init(BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED);
            advertising_init_beacon();
            m_advertising_mode = BLE_FAST_ADV;
        }
        else
        {
            m_advertising_mode = BLE_SLOW_ADV;
        }

        adv_params.interval = APP_ADV_INTERVAL_FAST;
        adv_params.timeout  = APP_FAST_ADV_TIMEOUT;

        err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_WHITELIST);
        APP_ERROR_CHECK(err_code);
        break;
    }

    case BLE_FAST_ADV:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE);
        advertising_init_beacon();

        adv_params.interval = APP_ADV_INTERVAL_FAST;
        adv_params.timeout  = APP_FAST_ADV_TIMEOUT;
        m_advertising_mode  = BLE_SLOW_ADV;
        err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING);
        APP_ERROR_CHECK(err_code);
        break;

    case BLE_SLOW_ADV:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE);
        advertising_init_beacon();

        adv_params.interval = g_interval;
        adv_params.timeout  = APP_SLOW_ADV_TIMEOUT;
        m_advertising_mode  = BLE_SLEEP;

        err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
        APP_ERROR_CHECK(err_code);
        break;

    case BLE_FOREVER_LED:
        advertising_init_beacon();
        adv_params.interval = g_interval;
        adv_params.timeout  = 0;
        m_advertising_mode  = BLE_FOREVER;
        //err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
        break;
    case BLE_FOREVER:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
        advertising_init_beacon();

        //adv_params.interval = APP_ADV_INTERVAL_SLOW;
        adv_params.interval = g_interval;
        adv_params.timeout  = 0;
        m_advertising_mode  = BLE_FOREVER;
        //err_code            = bsp_indication_set(BSP_INDICATE_IDLE);
        //APP_ERROR_CHECK(err_code);
        close_LEDS();
        break;
    default:
        // No implementation needed.
        break;
    }

    // Start advertising.
    err_code = sd_ble_gap_adv_start(&adv_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *          This function will start the ADC.
 *
 * @param[in] p_context   Pointer used for passing some arbitrary information (context) from the
 *                        app_start_timer() call to the timeout handler.
 */
static void battery_level_meas_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
#if 0
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//if in connected state.
    {
        LOG("temperature:Connectted state!return!");
        return;
    }
#endif
    LOG_ENTER;
#ifdef NRF51
    nrf_adc_start();
#else // NRF52
    uint32_t err_code;
    err_code = nrf_drv_saadc_sample();
    APP_ERROR_CHECK(err_code);
#endif // NRF51
}

//static ble_gap_adv_params_t m_adv_params;                                 /**< Parameters to be passed to the stack when starting advertising. */
uint8_t m_beacon_info[APP_BEACON_INFO_LENGTH] =                    /**< Information advertised by the Beacon. */
{
    APP_DEVICE_TYPE,     // Manufacturer specific information. Specifies the device type in this
    // implementation.
    APP_ADV_DATA_LENGTH, // Manufacturer specific information. Specifies the length of the
    // manufacturer specific data in this implementation.
    APP_BEACON_UUID,     // 128 bit UUID value.
    APP_MAJOR_VALUE,     // Major arbitrary value that can be used to distinguish between Beacons.
    APP_MINOR_VALUE,     // Minor arbitrary value that can be used to distinguish between Beacons.
    APP_MEASURED_RSSI,    // Used as battery level.(Manufacturer specific information. The Beacon's measured TX power in)
    // this implementation.
    0x64// Used as state.
};
#ifdef OBFUSCATING_ALGORITHMS
uint8_t m_beacon_info_major[2] = 
{
    APP_MAJOR_VALUE
};
#endif

#ifdef TETHYS_ADC
// mvolts should between 1297 and 1675
static __INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts)
{
    uint8_t temp;
    uint8_t battery_level;

    if (mvolts >= 6000)
    {
        battery_level = 100;
    }
    else if (mvolts > 5880)
    {
        temp  = (mvolts - 5880) / 12;
        battery_level = 90 + temp;
    }
    else if (mvolts > 5760)
    {
        temp  = (mvolts - 5760) / 12;
        battery_level = 80 + temp;
    }
    else if (mvolts > 5640)
    {
        temp  = (mvolts - 5640) / 12;
        battery_level = 70 + temp;
    }
    else if (mvolts > 5520)
    {
        temp  = (mvolts - 5520) / 12;
        battery_level = 60 + temp;
    }
    else if (mvolts > 5400)
    {
        temp  = (mvolts - 5400) / 12;
        battery_level = 50 + temp;
    }
    else if (mvolts > 5280)
    {
        temp  = (mvolts - 5280) / 12;
        battery_level = 40 + temp;
    }
    else if (mvolts > 5160)
    {
        temp  = (mvolts - 5160) / 12;
        battery_level = 30 + temp;
    }
    else if (mvolts > 5040)
    {
        temp  = (mvolts - 5040) / 12;
        battery_level = 20 + temp;
    }
    else if (mvolts >= 4920)
    {
        temp  = (mvolts - 4920) / 12;
        battery_level = 10 + temp;
    }
    else
    {
        if (mvolts > 4880)
        {
            battery_level = 8;
        }
        else if (mvolts > 4840)
        {
            battery_level = 5;
        }
        else if (mvolts > 4700)
        {
            battery_level = 1;
        }
        else
        {
            battery_level = 0;
        }
    }

    return battery_level;
}
#else
static __INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts)
{
    uint8_t temp;
    uint8_t battery_level;

    if (mvolts >= 3000)
    {
        battery_level = 100;
    }
    else if (mvolts > 2900)
    {
        temp  = (mvolts - 2900) / 10;
        battery_level = 90 + temp;
    }
    else if (mvolts > 2800)
    {
        temp  = (mvolts - 2800) / 10;
        battery_level = 80 + temp;
    }
    else if (mvolts > 2700)
    {
        temp  = (mvolts - 2700) / 10;
        battery_level = 70 + temp;
    }
    else if (mvolts > 2600)
    {
        temp  = (mvolts - 2600) / 10;
        battery_level = 60 + temp;
    }
    else if (mvolts > 2500)
    {
        temp  = (mvolts - 2500) / 10;
        battery_level = 50 + temp;
    }
    else if (mvolts > 2400)
    {
        temp  = (mvolts - 2400) / 10;
        battery_level = 40 + temp;
    }
    else if (mvolts > 2300)
    {
        temp  = (mvolts - 2300) / 10;
        battery_level = 30 + temp;
    }
    else if (mvolts > 2200)
    {
        temp  = (mvolts - 2200) / 10;
        battery_level = 20 + temp;
    }
    else if (mvolts >= 2100)
    {
        temp  = (mvolts - 2100) / 10;
        battery_level = 10 + temp;
    }
    else
    {
        if (mvolts > 2000)
        {
            battery_level = 8;
        }
        else if (mvolts > 1900)
        {
            battery_level = 5;
        }
        else if (mvolts > 1800)
        {
            battery_level = 1;
        }
        else
        {
            battery_level = 0;
        }
    }

    return battery_level;
}
#endif

static void show_battery_level(void)
{
    //uint8_t  percentage_batt_lvl = battery_level_in_percent(g_battery_voltage_mv);
    uint8_t  percentage_batt_lvl = batteryLevelInPercent(g_battery_voltage_mv);
    LOG("batt level:%d\r\n", percentage_batt_lvl);
    #if 1
    //m_beacon_info[BEACON_BATTERY_OFFSET] = percentage_batt_lvl;
    m_beacon_info[BEACON_BATTERY_OFFSET] = (m_beacon_info[BEACON_BATTERY_OFFSET] & ~BATTERY_Msk) |
                                            percentage_batt_lvl;
    if (g_last_batt != m_beacon_info[BEACON_BATTERY_OFFSET])
    {
        g_last_batt = m_beacon_info[BEACON_BATTERY_OFFSET];
        if (percentage_batt_lvl == 0)
        {
            playLaBa(LABA_DIAN_LIANG_BU_ZU);
        }
        advertising_init_beacon();
    }
    #else
    m_beacon_info[21] = g_battery_voltage_mv >> 8;
    m_beacon_info[22] = g_battery_voltage_mv & 0x00FF;
    m_beacon_info[23] = percentage_batt_lvl;
    advertising_init_beacon();
    #endif
}

/**< Address of the UICR register used by this example. 
 The major and minor versions to be encoded into the 
 advertising data will be picked up from this location. */
#define UICR_ADDRESS_MAJOR         0x10001080
#define UICR_ADDRESS_MAJOR_IS_SET  0x10001084

#define UICR_ADDRESS_MINOR         0x10001088
#define UICR_ADDRESS_MINOR_IS_SET  0x1000108C

#define UICR_ADDRESS_UUID          0x10001090
#define UICR_ADDRESS_UUID_IS_SET   0x100010A0
static void advertising_pre_init_beacon(void)
{
    LOG_ENTER;
    uint32_t      err_code;
    uint32_t major_is_set = 0;
    uint32_t minor_is_set = 0;
    uint32_t uuid_is_set = 0;

    // If USE_UICR_FOR_MAJ_MIN_VALUES is defined, the major and minor values will be read from the
    // UICR instead of using the default values. The major and minor values obtained from the UICR
    // are encoded into advertising data in big endian order (MSB First).
    // To set the UICR used by this example to a desired value, write to the address 0x10001080
    // using the nrfjprog tool. The command to be used is as follows.
    // nrfjprog --snr <Segger-chip-Serial-Number> --memwr 0x10001080 --val <your major/minor value>
    // For example, for a major value and minor value of 0xabcd and 0x0102 respectively, the
    // the following command should be used.
    // nrfjprog --snr <Segger-chip-Serial-Number> --memwr 0x10001080 --val 0xabcd0102
    major_is_set = *(uint32_t *)UICR_ADDRESS_MAJOR_IS_SET;
    minor_is_set = *(uint32_t *)UICR_ADDRESS_MINOR_IS_SET;
    uuid_is_set = *(uint32_t *)UICR_ADDRESS_UUID_IS_SET;
#if 0    
    uint16_t major_value = ((*(uint32_t *)UICR_ADDRESS_MAJOR) & 0xFFFF0000) >> 16;
    uint16_t minor_value = ((*(uint32_t *)UICR_ADDRESS_MINOR) & 0x0000FFFF);

    //uint8_t index = MAJ_VAL_OFFSET_IN_BEACON_INFO;
    uint8_t index = BEACON_MAJOR_OFFSET;

    m_beacon_info[index++] = MSB_16(major_value);
    m_beacon_info[index++] = LSB_16(major_value);

    m_beacon_info[index++] = MSB_16(minor_value);
    m_beacon_info[index++] = LSB_16(minor_value);
#endif
    //Get iBeacon info from pstorage
    pstorage_handle_t block_handle;
    uint8_t uuid[16];
    uint8_t ffs[16];
    memset(ffs, 0xFF, 16);
    //UUID
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_UUID, &block_handle);
    pstorage_load(uuid, &block_handle, 16, 0);
    if (memcmp(ffs, uuid, 16) != 0)
    {
        memcpy(m_beacon_info + BEACON_UUID_OFFSET, uuid, 16);
    }
    else if (uuid_is_set != 0xFFFFFFFF)
    {
        #if 0
        uint32_t *pUUID;
        pUUID = (uint32_t*)UICR_ADDRESS_UUID;
        uint32_t *pBeacon = (uint32_t *)(m_beacon_info + BEACON_UUID_OFFSET);
        for (int i = 0; i < 4; i++)
        {
            *pBeacon = *pUUID;
            pBeacon++;
            pUUID++;
        }
        #else
        uint32_t uuid_part1 = (*(uint32_t *)UICR_ADDRESS_UUID);
        uint32_t uuid_part2 = (*(uint32_t *)(UICR_ADDRESS_UUID+4));
        uint32_t uuid_part3 = (*(uint32_t *)(UICR_ADDRESS_UUID+8));
        uint32_t uuid_part4 = (*(uint32_t *)(UICR_ADDRESS_UUID+12));
        uint8_t ii = 0;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part1 & 0xFF000000) >> 24;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part1 & 0x00FF0000) >> 16;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part1 & 0x0000FF00) >> 8;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part1 & 0x000000FF);
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part2 & 0xFF000000) >> 24;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part2 & 0x00FF0000) >> 16;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part2 & 0x0000FF00) >> 8;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part2 & 0x000000FF);
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part3 & 0xFF000000) >> 24;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part3 & 0x00FF0000) >> 16;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part3 & 0x0000FF00) >> 8;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part3 & 0x000000FF);
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part4 & 0xFF000000) >> 24;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part4 & 0x00FF0000) >> 16;
        m_beacon_info[BEACON_UUID_OFFSET + ii++] = (uuid_part4 & 0x0000FF00) >> 8;
        m_beacon_info[BEACON_UUID_OFFSET + ii] = (uuid_part4 & 0x000000FF);
        #endif
    }
    //major
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_MAJOR, &block_handle);
    pstorage_load(uuid, &block_handle, 2, 0);
    if (memcmp(ffs, uuid, 2) != 0)
    {
    #ifdef OBFUSCATING_ALGORITHMS
        memcpy(m_beacon_info_major, uuid, 2);
        obfuscating_algorithms();
    #else
        memcpy(m_beacon_info + BEACON_MAJOR_OFFSET, uuid, 2);
    #endif
    }
    else if (major_is_set != 0xFFFFFFFF)
    {
        uint16_t major_value = (*(uint32_t *)UICR_ADDRESS_MAJOR);
    #ifdef OBFUSCATING_ALGORITHMS
        m_beacon_info_major[0] = MSB_16(major_value);
        m_beacon_info_major[1] = LSB_16(major_value);
        obfuscating_algorithms();
    #else
        m_beacon_info[BEACON_MAJOR_OFFSET] = MSB_16(major_value);
        m_beacon_info[BEACON_MAJOR_OFFSET + 1] = LSB_16(major_value);
    #endif
    }
    //minor
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_MINOR, &block_handle);
    pstorage_load(uuid, &block_handle, 2, 0);
    if (memcmp(ffs, uuid, 2) != 0)
    {
        //memcpy(m_beacon_info+20, uuid, 2);
        m_beacon_info[BEACON_MINOR_OFFSET] = uuid[0];
    }
    else if (minor_is_set != 0xFFFFFFFF)
    {
        uint16_t minor_value = (*(uint32_t *)UICR_ADDRESS_MINOR);
        m_beacon_info[BEACON_MINOR_OFFSET] = MSB_16(minor_value);
        m_beacon_info[BEACON_MINOR_OFFSET + 1] = LSB_16(minor_value);
    }
    // Calibrated tx power
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_Calibrated_Tx_Power, &block_handle);
    err_code = pstorage_load(uuid, &block_handle, 1, 0);
    if (memcmp(ffs, uuid, 1) != 0)
    {
        memcpy(m_beacon_info+22, uuid, 1);
    }
    APP_ERROR_CHECK(err_code);

    // adv interval. Unit is 100ms
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_ADV_Freq, &block_handle);
    err_code = pstorage_load(uuid, &block_handle, 1, 0);
    if (memcmp(ffs, uuid, 1) != 0)
    {
        uint16_t interval = uuid[0];
        if (interval >= 1 && interval <= 102)
        {
            g_interval = (uint16_t)((float)interval * (float)100 / (float)0.625);
            LOG("g_interval = %d", g_interval);
        }
    }

    // Device TX power.
    pstorage_block_identifier_get(&g_storage_handle, BLOCK_NUM_DEVICE_TX_POWER, &block_handle);
    err_code = pstorage_load(uuid, &block_handle, 1, 0);
    if (memcmp(ffs, uuid, 1) != 0)
    {
        switch (uuid[0])
        {
            case 0:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_0dBm;
                break;
            case 1:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Pos4dBm;
                break;
            case 2:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg4dBm;
                break;
            case 3:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg8dBm;
                break;
            case 4:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg12dBm;
                break;
            case 5:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg16dBm;
                break;
            case 6:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg20dBm;
                break;
            case 7:
                g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg30dBm;
                break;
        }
    }
}

#if 0
static void advertising_start_beacon(void)
{
    uint32_t err_code;
    LOG_ENTER;
    err_code = sd_ble_gap_adv_start(&m_adv_params);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    APP_ERROR_CHECK(err_code);
}
#endif

#ifdef ADXL362_OPEN
uint8_t bFirst = 0;
static void adxl362_timeout_handler(void *p_context)
{
    //static uint32_t bFirst = 0;
    UNUSED_PARAMETER(p_context);
    //LOG_ENTER;
    //nrf_gpio_cfg_output(14);
    //nrf_gpio_cfg_output(15);
    //nrf_gpio_cfg_output(16);
    //nrf_gpio_pin_set(14);
    //nrf_gpio_pin_set(15);
    //nrf_gpio_pin_set(16);
    //nrf_delay_ms(100);

#if 0
    m_beacon_info[18] = 0x76;
    m_beacon_info[19] = 0x77;
    m_beacon_info[20] = 0x78;
    m_beacon_info[21] = 0x79;

    if (bFirst == 0)
    {
        m_beacon_info[20] = ADXL362_Init();
    }
    bFirst++;
    unsigned char regValue = 0x99;
    ADXL362_GetRegisterValue(&regValue, ADXL362_REG_PARTID, 1);
    m_beacon_info[21] = regValue;
    advertising_init_beacon();
    //advertising_start_beacon();
#else
    //char ret;
    if (bFirst == 0)
    {
        #ifdef MIMAS_VER3
        nrf_gpio_pin_set(15);
        #else
        nrf_gpio_pin_set(14);
        nrf_gpio_pin_set(15);
        nrf_gpio_pin_set(16);
        #endif
        //nrf_delay_ms(20);
#if 0
        nrf_gpio_cfg_output(14);
        //nrf_gpio_cfg_output(15);
        //nrf_gpio_cfg_output(16);
        nrf_gpio_pin_set(14);
        //nrf_gpio_pin_set(15);
        //nrf_gpio_pin_set(16);
        nrf_delay_ms(2000);
#endif
        //ret = ADXL362_Init();
        ADXL326Init();
        bFirst++;
        //NRF_SPI0->ENABLE = 0;
        ADXL362_spi_master_uninit();
        //app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
        //nrf_gpio_cfg_input(SPIM0_SS_PIN, NRF_GPIO_PIN_PULLUP);
        nrf_gpio_cfg(
            SPIM0_SS_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        nrf_gpio_cfg(
            SPIM0_SCK_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        nrf_gpio_cfg(
            SPIM0_MOSI_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        nrf_gpio_cfg(
            SPIM0_MISO_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        return;
    }
    #if 0
    //uint32_t int1_pin_state = nrf_gpio_pin_sense_get(ADXL362_INT1_PIN);
    //uint32_t int2_pin_state = nrf_gpio_pin_sense_get(ADXL362_INT2_PIN);
    //LOG("INT1 = %d, INT2 = %d\r\n", int1_pin_state, int2_pin_state);
    bFirst++;
    int16_t xdata = 0;
    int16_t ydata = 0;
    int16_t zdata = 0;
    //LOG("ADXL362_Init ret is %d\r\n", ret);
    LOG("bFirst = %d\r\n", bFirst);
    //unsigned char regValue = 0x98;
    //unsigned char regValue1 = 0x99;
    //unsigned char regValue2 = 0x99;
    //short x,y,z;
    //ADXL362_GetRegisterValue(&regValue, ADXL362_REG_YDATA, 1);
    //float temp = ADXL362_ReadTemperature();
    //ADXL362_GetXyz(&x, &y, &z);
    uint8_t rawTempData[8] = {0};
    //int16_t         signedTemp     = 0;
    //float           tempCelsius    = 0;
    //regValue = ADXL362RegisterRead(ADXL362_REG_DEVID_AD);
    //regValue1 = ADXL362RegisterRead(ADXL362_REG_STATUS);
    //regValue2 = ADXL362RegisterRead(ADXL362_REG_ZDATA);
    ADXL362BurstRead(ADXL362_REG_XDATA_L, 6, rawTempData);
    xdata = (int16_t)(rawTempData[0] | (rawTempData[1]<<8));
    ydata = (int16_t)(rawTempData[2] | (rawTempData[3]<<8));
    zdata = (int16_t)(rawTempData[4] | (rawTempData[5]<<8));
    theBigX = abs(xdata) > theBigX ? abs(xdata) : theBigX;
    theBigY = abs(ydata) > theBigY ? abs(ydata) : theBigY;
    theBigZ = abs(zdata) > theBigZ ? abs(zdata) : theBigZ;
    //ADXL362_GetRegisterValue(rawTempData, ADXL362_REG_TEMP_L, 1);
    //signedTemp = (int16_t)(rawTempData[6] | rawTempData[7]<<8);
    //tempCelsius = (float)signedTemp * 0.065;
    //LOG("ADXL362_REG_PARTID = 0x%X\r\n", regValue);
    //LOG("ADXL362_REG_DEVID_AD = 0x%X\r\n", regValue);
    //LOG("ADXL362_REG_DEVID_MST = 0x%X\r\n", regValue);
    //LOG("ADXL362_REG_STATUS = 0x%X\r\n", regValue1);
    //LOG("ADXL362_REG_FIFO_SAMPLES = 0x%X\r\n", regValue);
    //LOG("x = %d, y = %d, z = %d\r\n", rawTempData[0], rawTempData[1], rawTempData[2]);
    //LOG("x = %d(0x%2X), raw0 = 0x%X, raw1 = 0x%X\r\n", xdata, xdata, rawTempData[0], rawTempData[1]);
    //printf("x = %d, y = %d, z = %d\r\n", xdata, ydata, zdata);
    if (bFirst % 10 == 0)
    {
        //printf("x = %d, y = %d, z = %d\r\n", theBigX, theBigY, theBigZ);
        m_beacon_info[BEACON_MAJOR_OFFSET-2] = MSB_16(theBigX);
        m_beacon_info[BEACON_MAJOR_OFFSET-1] = LSB_16(theBigX);
        m_beacon_info[BEACON_MAJOR_OFFSET] = MSB_16(theBigY);
        m_beacon_info[BEACON_MAJOR_OFFSET+1] = LSB_16(theBigY);
        m_beacon_info[BEACON_MINOR_OFFSET] = MSB_16(theBigZ);
        m_beacon_info[BEACON_MINOR_OFFSET+1] = LSB_16(theBigZ);
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    //LOG("temp_l = 0x%x, temp_h = 0x%x\r\n", rawTempData[6], rawTempData[7]);
    //LOG("signedTemp = %d, tempCelsius = %f\r\n", signedTemp+5, tempCelsius);

    //printf("\r\n");
    app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(200, APP_TIMER_PRESCALER), NULL);
    #else
    unsigned char regValue = 0xFE;
    if (bFirst == 1)
    {
        regValue = ADXL362RegisterRead(ADXL362_REG_ACT_INACT_CTL);
        m_beacon_info[BEACON_UUID_OFFSET+bFirst] = regValue;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    }
    else if (bFirst == 2)
    {
        regValue = ADXL362RegisterRead(ADXL362_REG_INTMAP1);
        m_beacon_info[BEACON_UUID_OFFSET+bFirst] = regValue;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    }
    else if (bFirst == 3)
    {
        regValue = ADXL362RegisterRead(ADXL362_REG_INTMAP2);
        m_beacon_info[BEACON_UUID_OFFSET+bFirst] = regValue;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    }
    else if (bFirst == 4)
    {
        regValue = ADXL362RegisterRead(ADXL362_REG_FILTER_CTL);
        m_beacon_info[BEACON_UUID_OFFSET+bFirst] = regValue;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    }
    else if (bFirst == 5)
    {
        regValue = ADXL362RegisterRead(ADXL362_REG_POWER_CTL);
        m_beacon_info[BEACON_UUID_OFFSET+bFirst] = regValue;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
        app_timer_start(m_adxl362_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    }
    bFirst++;
    #endif
#endif
}

#ifdef ADXL362_AS_BUTTON_LONG_PUSH
static void adxl362_adv_timeout_handler(void *p_context)
{
    gADXL362_adv_count = 0;
    gADXL362_adv = 0;
}
#endif
#endif

static void adv_start_ind(void)
{
    advertising_start(BLE_GAP_ADV_TYPE_ADV_IND);
}

static void adv_start_nonconn_ind(void)
{
    advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
}

extern void get_nrf51822_temperature(void *p_event_data, uint16_t event_size);
static void test_task_timeout_handler(void *p_context)
{
    LOG("test_task_timeout_handler: p_context = 0x%x\r\n", p_context);
#if 0
    static char data_array[BLE_NUS_MAX_DATA_LEN];
    strcpy(data_array, "To be finished...");
    ble_nus_string_send(&m_nus, (uint8_t *)data_array, strlen(data_array));
#elif 0
    get_nrf51822_temperature(NULL, 0);
#elif 0
    bond_store_user_id();
#elif 0
    //uint32_t hTest = 0x78DA908B;
    //uint32_t  *mp_flash_bond_info;
    USER_TIMER_COMMAND_t *command = (USER_TIMER_COMMAND_t *)p_context;
    LOG("command=%d\r\n", *command);
    if(*command == DO_BOND)
    {
        bond_store_user_id();
    }
    else if (*command == DO_UN_BOND)
    {
        bond_clear_user_id();
    }
    //ble_flash_page_addr(FLASH_PAGE_USER_ID, &mp_flash_bond_info);
    //ble_flash_block_write(mp_flash_bond_info, &hTest, 1);
    //ble_flash_word_write(mp_flash_bond_info, hTest);
#elif 0
    pstorage_handle_t block_handle;
    static uint8_t    dest_data[16];
    static uint32_t destData;
    uint32_t          retval;
    // Request to get identifier for 3rd block.
    retval = pstorage_block_identifier_get(&m_storage_handle, 0, &block_handle);
    LOG("block_handle1:0x%x\r\n", &block_handle);
    if (retval == NRF_SUCCESS)
    {
        LOG("Get Block Identifier successful.\r\n");
    }
    else
    {
        LOG("Failed to get block id, take corrective action.\r\n");
    }
    // Request to read 4 bytes from block at an offset of 12 bytes.
    retval = pstorage_load((uint8_t *)&destData, &block_handle, 4, 12);
    if (retval == NRF_SUCCESS)
    {
        // Load successful. Consume data.
    }
    else
    {
        // Failed to load, take corrective action.
    }
#else
    if (g_timer_task == 0)
    {
        //get_nrf51822_temperature(NULL, 0);
        bsp_indication_set(BSP_INDICATE_IDLE);
    }
    else if (g_timer_task == 1)
    {
        bsp_indication_set(BSP_INDICATE_IDLE);
        g_timer_task = 2;
        app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(60000, APP_TIMER_PRESCALER), NULL);
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)adv_start_ind);
    }
    else if (g_timer_task == 2)
    {
        bsp_indication_set(BSP_INDICATE_USER_STATE_1);
        sd_ble_gap_adv_stop();
        g_timer_task = 3;
        app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(3000, APP_TIMER_PRESCALER), NULL);
    }
    else if (g_timer_task == 3)
    {
        bsp_indication_set(BSP_INDICATE_IDLE);
        g_timer_task = 0;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)adv_start_nonconn_ind);
    }
    else if (g_timer_task == 4)
    {
        bsp_indication_set(BSP_INDICATE_IDLE);
        g_timer_task = 0;
    }
    else if (g_timer_task == 5) // To test LaBa
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)testLaBa);
        g_timer_task = 5;
        app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(4900, APP_TIMER_PRESCALER), NULL);
    }
    else if (g_timer_task == 6) // To test RC522
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)init_MFRC522);
    }
    else if (g_timer_task == 7) // To test RC522
    {
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)RFID_init);
    }
    else if (g_timer_task == 8) // To init fingerprint.
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)init_Fingerprint);
    }
    else if (g_timer_task == 9) // 5分钟到了，可以蓝牙开锁了
    {
        tethys_reset_openLockTimes();
    }
#endif
}

void start_tethys_open_lock_timeout_timer(void)
{
    g_timer_task = 9;
    app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(300000, APP_TIMER_PRESCALER), NULL);
    //app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER), NULL);
}

static void reset_timeout_handler(void *p_context)
{
    //sd_nvic_SystemReset();
}

static void login_timeout_handler(void *p_context)
{
    if (tethys_get_login_state() == false)
    {
        if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//If in connected state.
        {
            led_blink(6);
            sd_ble_gap_disconnect(global_connect_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        }
    }
}

void stop_login_timer(void)
{
    app_timer_stop(m_tethys_login_timer_id);
}

static void temperature_timeout_handler(void *p_context)
{
    int32_t temp;
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//If in connected state.
    {
        LOG("temperature:Connectted state!return!");
        return;
    }
    sd_temp_get(&temp);
    LOG("temp = %d\r\n", temp);
   
    float cTemp = (float)temp / 4;
    cTemp -= 3.0;

    m_beacon_info[BEACON_TEMP_OFFSET] = 20 + cTemp;
    if (g_last_temp != m_beacon_info[BEACON_TEMP_OFFSET])
    {
        g_last_temp = m_beacon_info[BEACON_TEMP_OFFSET];
    #ifdef OBFUSCATING_ALGORITHMS
        obfuscating_algorithms();
    #endif
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    else
    {
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    LOG("temperature:%.2f;\r\n", cTemp);
}

static void alert_off_timeout_handler(void *p_context)
{
    alert_signal(BLE_CHAR_ALERT_LEVEL_NO_ALERT);
#ifdef BUZZER_OPEN
    buzzer_stop();
#endif
}

#ifdef BUZZER_OPEN
#define DUTY_CYCLE 61
// Create the instance "PWM1" using TIMER1.
APP_PWM_INSTANCE(PWM1, 1);   
static void buzzer_timeout_handler(void *p_context)
{
    #if 1
    if (gBuzzerState == 0)
    {
        gBuzzerCount++;
        if (gBuzzerCount > gBeepCount)
        {
            gBuzzerCount = 0;
            app_pwm_disable(&PWM1);
            nrf_gpio_pin_clear(BUZZER_OUT);
            return;
        }
        #if 0
        gDutyCycle = (gDutyCycleI < 20) ? (gDutyCycleI * 5) : (95 - (gDutyCycleI - 20) * 5);
        gDutyCycleI++;
        if (gDutyCycleI >= 40)
        {
            gDutyCycleI = 1;
        }
        #endif
    }
    switch (gBuzzerState)
    {
        case 0:
            gBuzzerState = 1;
            app_pwm_enable(&PWM1);
            app_pwm_channel_duty_set(&PWM1, 0, DUTY_CYCLE);
            app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(30, APP_TIMER_PRESCALER), NULL);
            break;
        case 1:
            gBuzzerState = 2;
            app_pwm_disable(&PWM1);
            nrf_gpio_pin_clear(BUZZER_OUT);
            app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(70, APP_TIMER_PRESCALER), NULL);
            break;
        case 2:
            gBuzzerState = 3;
            app_pwm_enable(&PWM1);
            app_pwm_channel_duty_set(&PWM1, 0, DUTY_CYCLE);
            app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(30, APP_TIMER_PRESCALER), NULL);
            break;
        case 3:
            gBuzzerState = 0;
            app_pwm_disable(&PWM1);
            nrf_gpio_pin_clear(BUZZER_OUT);
            app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(1100, APP_TIMER_PRESCALER), NULL);
            break;
        case 4:
            break;
    }
    
    #else
    if (gBuzzerState == 0)
    {
        gBuzzerState = 1;
        app_pwm_enable(&PWM1);
        app_pwm_channel_duty_set(&PWM1, 0, 80);
        app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER), NULL);
    }
    else
    {
        app_pwm_disable(&PWM1);
    }
    #endif
}
                
static void buzzer_start(void)
{
    app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(3000, APP_TIMER_PRESCALER), NULL);
}

static void buzzer_start_now(void)
{
    gBuzzerState = 0;
    gBuzzerCount = 0;
    gBeepCount = 20;
    app_timer_start(m_buzzer_timer_id, APP_TIMER_TICKS(998, APP_TIMER_PRESCALER), NULL);
}

static void buzzer_stop(void)
{
    gBuzzerCount = gBeepCount + 1;
}

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    //ready_flag = true;
}

static void buzzer_init(void)
{
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(366L, BUZZER_OUT);
    
    /* Switch the polarity of the second channel. */
    //pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_LOW;
    
    /* Initialize and enable PWM. */
    uint32_t err_code = app_pwm_init(&PWM1, &pwm1_cfg, pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
}
#endif

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    //LOG_ENTER;
    uint32_t err_code;

    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create battery timer.
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_level_meas_timeout_handler);
    APP_ERROR_CHECK(err_code);
#ifdef ADXL362_OPEN
    err_code = app_timer_create(&m_adxl362_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                adxl362_timeout_handler);
    APP_ERROR_CHECK(err_code);
#ifdef ADXL362_AS_BUTTON_LONG_PUSH    
    err_code = app_timer_create(&m_adxl362_adv_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                adxl362_adv_timeout_handler);
    APP_ERROR_CHECK(err_code);
#endif
#endif
    err_code = app_timer_create(&m_test_task_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                test_task_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_temperature_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                temperature_timeout_handler);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_reset_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                reset_timeout_handler);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_tethys_login_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                login_timeout_handler);
    APP_ERROR_CHECK(err_code);
#ifdef MIMAS_WALKER
    init_factory_test_timer();
#endif
#ifdef BUZZER_OPEN
    err_code = app_timer_create(&m_buzzer_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                buzzer_timeout_handler);
    APP_ERROR_CHECK(err_code);
#endif
    err_code = app_timer_create(&m_alert_off_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                alert_off_timeout_handler);
    APP_ERROR_CHECK(err_code);
}

static bool gMacAuthorized = false;
// length of 'serialNumber' must >= 12
static void getMacASCII(uint8_t *serialNumber)
{
    ble_gap_addr_t mac_addr_t;
    sd_ble_gap_address_get(&mac_addr_t);
    //uint8_t serialNumber[14] = "\0";
    uint8_t aNumber;
    uint8_t j = 0;
    for (int i = BLE_GAP_ADDR_LEN - 1; i >= 0; i--)
    {
        aNumber = mac_addr_t.addr[i];
        sprintf((char*)serialNumber + j * 2, "%02X", aNumber);
        j++;
    }
    //uint8_t addr[BLE_GAP_ADDR_LEN] = {0xDB, 0x96, 0x42, 0x79, 0xCC, 0x78};
    //char addr[14] = "DB964279CC78";
    //char addr[14] = "D8E507551E04";
    char addr[14] = "EDE9C872E210";
    if (memcmp(addr, serialNumber, 12) == 0)
    {
        gMacAuthorized = true;
    }
}

bool isMacAuthorized(void)
{
    return gMacAuthorized;
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    //LOG_ENTER;
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    //BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&sec_mode);
    //BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&sec_mode);

    uint8_t buf[14] = {0};
    getMacASCII(buf);
    memcpy(gDeviceName+6, buf, 12);
#if 0
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
#else
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          gDeviceName,
                                          18);
#endif
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_KEYRING);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_tx_power_set(g_device_tx_power);
    APP_ERROR_CHECK(err_code);
#ifdef TETHYS_PAIR
    m_static_pin_option.gap_opt.passkey.p_passkey = gPasskey;
    err_code = sd_ble_opt_set(BLE_GAP_OPT_PASSKEY, &m_static_pin_option);
    APP_ERROR_CHECK(err_code);
#endif
}

/**@brief Function for initializing the Event Scheduler.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

#if 0
/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 *
 * @param[in] adv_flags  Indicates which type of advertisement to use, see @ref BLE_GAP_DISC_MODES.
 *
 */
static void advertising_init(uint8_t adv_flags)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    int8_t        tx_power_level = g_device_tx_power;
    //LOG_ENTER;
    ble_uuid_t adv_uuids[] =
    {
        {BLE_UUID_TX_POWER_SERVICE,        BLE_UUID_TYPE_BLE},
        {BLE_UUID_IMMEDIATE_ALERT_SERVICE, BLE_UUID_TYPE_BLE},
        {BLE_UUID_LINK_LOSS_SERVICE,       BLE_UUID_TYPE_BLE}
    };

    m_advertising_mode = BLE_FOREVER;

    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = true;
    advdata.flags                   = adv_flags;
    advdata.p_tx_power_level        = &tx_power_level;
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;

    err_code = ble_advdata_set(&advdata, NULL);
    APP_ERROR_CHECK(err_code);
}
#endif

/**@brief Function for initializing the TX Power Service.
 */
static void tps_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_tps_init_t tps_init_obj;

    memset(&tps_init_obj, 0, sizeof(tps_init_obj));
    tps_init_obj.initial_tx_power_level = g_device_tx_power;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&tps_init_obj.tps_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&tps_init_obj.tps_attr_md.write_perm);

    err_code = ble_tps_init(&m_tps, &tps_init_obj);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Device Information Service.
 */
static void Device_Information_init(void)
{
    //LOG_ENTER;
    uint32_t         err_code;
    ble_dis_init_t   dis_init;
    ble_dis_sys_id_t sys_id;
    // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&dis_init.model_num_str,     MODEL_NUM);
    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str,     HW_REV_STR);
    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str,     FW_REV_STR);
    ble_srv_ascii_to_utf8(&dis_init.sw_rev_str,     SW_REV_STR);

    sys_id.manufacturer_id            = MANUFACTURER_ID;
    sys_id.organizationally_unique_id = ORG_UNIQUE_ID;
    dis_init.p_sys_id                 = &sys_id;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Immediate Alert Service.
 */
static void ias_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_ias_init_t ias_init_obj;

    memset(&ias_init_obj, 0, sizeof(ias_init_obj));
    ias_init_obj.evt_handler = on_ias_evt;

    err_code = tw_ble_ias_init(&m_ias, &ias_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Link Loss Service.
 */
static void lls_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_lls_init_t lls_init_obj;

    // Initialize Link Loss Service
    memset(&lls_init_obj, 0, sizeof(lls_init_obj));

    lls_init_obj.evt_handler         = on_lls_evt;
    lls_init_obj.error_handler       = service_error_handler;
    lls_init_obj.initial_alert_level = INITIAL_LLS_ALERT_LEVEL;
#if 0
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&lls_init_obj.lls_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&lls_init_obj.lls_attr_md.write_perm);
#else
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lls_init_obj.lls_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lls_init_obj.lls_attr_md.write_perm);
#endif
    err_code = ble_lls_init(&m_lls, &lls_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Battery Service.
 */
static void bas_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = on_bas_evt;
    bas_init_obj.support_notification = true;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init_obj.battery_level_char_attr_md.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_report_read_perm);

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the immediate alert service client.
 *
 * @details This will initialize the client side functionality of the Find Me profile.
 */
static void ias_client_init(void)
{
    LOG_ENTER;
    uint32_t         err_code;
    ble_ias_c_init_t ias_c_init_obj;

    memset(&ias_c_init_obj, 0, sizeof(ias_c_init_obj));

    m_is_high_alert_signalled = false;

    ias_c_init_obj.evt_handler   = on_ias_c_evt;
    ias_c_init_obj.error_handler = service_error_handler;

    err_code = ble_ias_c_init(&m_ias_c, &ias_c_init_obj);
    APP_ERROR_CHECK(err_code);
}

#ifdef BLE_DFU_APP_SUPPORT
/**@brief Function for stopping advertising.
 */
static void advertising_stop(void)
{
    LOG_ENTER;
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for loading application-specific context after establishing a secure connection.
 *
 * @details This function will load the application context and check if the ATT table is marked as
 *          changed. If the ATT table is marked as changed, a Service Changed Indication
 *          is sent to the peer if the Service Changed CCCD is set to indicate.
 *
 * @param[in] p_handle The Device Manager handle that identifies the connection for which the context
 *                     should be loaded.
 */
static void app_context_load(dm_handle_t const *p_handle)
{
    LOG_ENTER;
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;

    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(global_connect_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                    (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                    (err_code != NRF_ERROR_INVALID_STATE) &&
                    (err_code != BLE_ERROR_NO_TX_BUFFERS) &&
                    (err_code != NRF_ERROR_BUSY) &&
                    (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/** @snippet [DFU BLE Reset prepare] */
/**@brief Function for preparing for system reset.
 *
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by
 *          @ref dfu_app_handler.c before entering the bootloader/DFU.
 *          This allows the current running application to shut down gracefully.
 */
static void reset_prepare(void)
{
    LOG_ENTER;
    uint32_t err_code;

    if (global_connect_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(global_connect_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        err_code = bsp_indication_set(BSP_INDICATE_IDLE);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }

    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(500);
}

static void tethys_dfu_app_on_dfu_evt(ble_dfu_t * p_dfu, ble_dfu_evt_t * p_evt)
{
    if (gNiuBi != 0)
    {
        dfu_app_on_dfu_evt(p_dfu, p_evt);
    }
    else if (1 == tethys_get_OTA())
    {
        dfu_app_on_dfu_evt(p_dfu, p_evt);
    }
}

static void dfu_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    /** @snippet [DFU BLE Service initialization] */
    ble_dfu_init_t   dfus_init;

    // Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = tethys_dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.revision      = DFU_REVISION;

    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);

    dfu_app_reset_prepare_set(reset_prepare);
    dfu_app_dm_appl_instance_set(m_app_handle);
    /** @snippet [DFU BLE Service initialization] */
}
/** @snippet [DFU BLE Reset prepare] */
#endif // BLE_DFU_APP_SUPPORT

#if 0
/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_t *p_nus, uint8_t *p_data, uint16_t length)
{
    static char data_array[BLE_NUS_MAX_DATA_LEN];
    strcpy(data_array, "To be finished...");
#if 0
    for (uint32_t i = 0; i < length; i++)
    {
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
    while(app_uart_put('\n') != NRF_SUCCESS);
#endif
    ble_nus_string_send(&m_nus, (uint8_t *)data_array, strlen(data_array));
}
#endif

static void tethys_nus_init(void)
{
    tethys_ble_nus_init_t nus_init;
    memset(&nus_init, 0, sizeof(nus_init));
    nus_init.data_handler = L1_receive_data_tethys;
    tethys_ble_nus_init(&m_tethys_nus, &nus_init);
}

/**@snippet [Handling the data received over BLE] */

/**@brief Function for initializing the services that will be used by the application.
 */
static void services_init(void)
{
    //LOG_ENTER;
    Device_Information_init();

    ble_nus_init_t nus_init;
    memset(&nus_init, 0, sizeof(nus_init));
    //nus_init.data_handler = L1_receive_data;//nus_data_handler;
    nus_init.data_handler = L1_receive_data_iBeacon;
    //nus_init.data_handler = nus_data_handler;
    uint32_t err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
    
    ias_init();
    tps_init();
    lls_init();
    bas_init();
    ias_client_init();
#ifdef BLE_DFU_APP_SUPPORT
    dfu_init();
#endif
    tethys_nus_init();
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    LOG_ENTER;
    APP_ERROR_HANDLER(nrf_error);
}

static void on_conn_params_evt(ble_conn_params_evt_t *p_evt)
{
    //uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        LOG("BLE_CONN_PARAMS_EVT_FAILED\r\n");
    }
    else
    {
        LOG("BLE_CONN_PARAMS_EVT_SUCCEEDED\r\n");
    }
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    //LOG_ENTER;
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    ble_gap_conn_params_t   gap_conn_params;

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = &gap_conn_params;//NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Signals alert event from Immediate Alert or Link Loss services.
 *
 * @param[in] alert_level  Requested alert level.
 */
static void alert_signal(uint8_t alert_level)
{
    LOG_ENTER;
    uint32_t err_code;
    switch (alert_level)
    {
    case BLE_CHAR_ALERT_LEVEL_NO_ALERT:
        #if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_OFF);
        APP_ERROR_CHECK(err_code);
        #else
        #endif
        break;

    case BLE_CHAR_ALERT_LEVEL_MILD_ALERT:
        #if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_0);
        APP_ERROR_CHECK(err_code);
        app_timer_start(m_alert_off_timer_id, APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER), NULL);
    #ifdef BUZZER_OPEN
        buzzer_start_now();
    #endif
        #else
        led_donghua_alert();
        #endif
        break;

    case BLE_CHAR_ALERT_LEVEL_HIGH_ALERT:
        #if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_2);
        APP_ERROR_CHECK(err_code);
        app_timer_start(m_alert_off_timer_id, APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER), NULL);
    #ifdef BUZZER_OPEN
        buzzer_start_now();
    #endif
        #else
        led_donghua_alert();
        #endif
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
void mimas_sleep_mode_enter(void)
{
    LOG_ENTER;
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Immediate Alert events.
 *
 * @details This function will be called for all Immediate Alert events which are passed to the
 *          application.
 *
 * @param[in] p_ias  Immediate Alert structure.
 * @param[in] p_evt  Event received from the Immediate Alert service.
 */
static void on_ias_evt(ble_ias_t *p_ias, ble_ias_evt_t *p_evt)
{
    LOG_ENTER;
    switch (p_evt->evt_type)
    {
    case BLE_IAS_EVT_ALERT_LEVEL_UPDATED:
        alert_signal(p_evt->params.alert_level);
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for handling Link Loss events.
 *
 * @details This function will be called for all Link Loss events which are passed to the
 *          application.
 *
 * @param[in] p_lls  Link Loss structure.
 * @param[in] p_evt  Event received from the Link Loss service.
 */
static void on_lls_evt(ble_lls_t *p_lls, ble_lls_evt_t *p_evt)
{
    LOG_ENTER;
    switch (p_evt->evt_type)
    {
    case BLE_LLS_EVT_LINK_LOSS_ALERT:
        alert_signal(p_evt->params.alert_level);
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for handling IAS Client events.
 *
 * @details This function will be called for all IAS Client events which are passed to the
 *          application.
 *
 * @param[in] p_ias_c  IAS Client structure.
 * @param[in] p_evt    Event received.
 */
static void on_ias_c_evt(ble_ias_c_t *p_ias_c, ble_ias_c_evt_t *p_evt)
{
    LOG_ENTER;
    switch (p_evt->evt_type)
    {
    case BLE_IAS_C_EVT_SRV_DISCOVERED:
        // IAS is found on peer. The Find Me Locator functionality of this app will work.
        m_is_ias_present = true;
        break;

    case BLE_IAS_C_EVT_SRV_NOT_FOUND:
        // IAS is not found on peer. The Find Me Locator functionality of this app will NOT work.
        break;

    case BLE_IAS_C_EVT_DISCONN_COMPLETE:
        // Disable alert buttons
        m_is_ias_present = false;
        break;

    default:
        break;
    }
}


/**@brief Function for handling the Battery Service events.
 *
 * @details This function will be called for all Battery Service events which are passed to the
 |          application.
 *
 * @param[in] p_bas  Battery Service structure.
 * @param[in] p_evt  Event received from the Battery Service.
 */
static void on_bas_evt(ble_bas_t *p_bas, ble_bas_evt_t *p_evt)
{
    LOG_ENTER;
    uint32_t err_code;

    switch (p_evt->evt_type)
    {
    case BLE_BAS_EVT_NOTIFICATION_ENABLED:
        // Start battery timer
        err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
        APP_ERROR_CHECK(err_code);
        break;

    case BLE_BAS_EVT_NOTIFICATION_DISABLED:
        err_code = app_timer_stop(m_battery_timer_id);
        APP_ERROR_CHECK(err_code);
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t *p_ble_evt)
{
    //LOG_ENTER;
    uint32_t        err_code      = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
    case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        led_blink(1);        
        break;
    case BLE_GAP_EVT_CONNECTED:
        app_timer_stop(m_test_task_timer_id);
        //err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
        //APP_ERROR_CHECK(err_code);

        m_advertising_mode = BLE_NO_ADV;
        global_connect_handle      = p_ble_evt->evt.gap_evt.conn_handle;
        if (tethys_storage_is_bangding() == 0)//设备未绑定时不需要这个机制
        {
            app_timer_start(m_tethys_login_timer_id, APP_TIMER_TICKS(60000, APP_TIMER_PRESCALER), NULL);
        }
        break;

    case BLE_GAP_EVT_DISCONNECTED:
        //led_blink(3);
        tethys_set_login_state(false);
        m_advertising_mode = BLE_FOREVER;
        global_connect_handle = BLE_CONN_HANDLE_INVALID;        
        advertising_start(BLE_GAP_ADV_TYPE_ADV_IND);
        if (tethys_storage_is_bangding() == 0)//设备未绑定时不需要这个机制
        {
            app_timer_stop(m_tethys_login_timer_id);
        }
        break;

    case BLE_GAP_EVT_TIMEOUT:
        led_blink(4);
        if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING)
        {
            if (m_advertising_mode == BLE_SLEEP)
            {
                mimas_sleep_mode_enter();
            }
            else
            {
                advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
            }
        }
        break;

    case BLE_GATTC_EVT_TIMEOUT:
    case BLE_GATTS_EVT_TIMEOUT:
        led_blink(5);
        // Disconnect on GATT Server and Client timeout events.
        err_code = sd_ble_gap_disconnect(global_connect_handle,
                                         BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        break;
#if 0
    case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
         err_code = sd_ble_gap_sec_params_reply(global_connect_handle,
                                    BLE_GAP_SEC_STATUS_SUCCESS,&m_sec_params,NULL);
         APP_ERROR_CHECK(err_code);
         break;
#endif
    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for handling the Application's system events.
 *
 * @param[in] sys_evt  system event.
 */
static void on_sys_evt(uint32_t sys_evt)
{
    //LOG_ENTER;
    switch(sys_evt)
    {
    case NRF_EVT_FLASH_OPERATION_SUCCESS:
    case NRF_EVT_FLASH_OPERATION_ERROR:

        if (m_memory_access_in_progress)
        {
            m_memory_access_in_progress = false;
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t *p_ble_evt)
{
    //LOG_ENTER;
    //nrf_events_strings_log(p_ble_evt->header.evt_id);
    //LOG("evt_len = %d\r\n", p_ble_evt->header.evt_len);
    
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
    tethys_ble_nus_on_ble_evt(&m_tethys_nus, p_ble_evt);
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    tw_ble_ias_on_ble_evt(&m_ias, p_ble_evt);
    ble_lls_on_ble_evt(&m_lls, p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt);
    ble_ias_c_on_ble_evt(&m_ias_c, p_ble_evt);
    ble_tps_on_ble_evt(&m_tps, p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
#ifdef BLE_DFU_APP_SUPPORT
    /** @snippet [Propagating BLE Stack events to DFU Service] */
    ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
    /** @snippet [Propagating BLE Stack events to DFU Service] */
#endif // BLE_DFU_APP_SUPPORT
    on_ble_evt(p_ble_evt);
}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    //LOG_ENTER;
    //nrf_sys_events_strings_log(sys_evt);
    pstorage_sys_event_handler(sys_evt);
    on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    //LOG_ENTER;
    uint32_t err_code;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL);

    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const *p_handle,
        dm_event_t const   *p_event,
        ret_code_t        event_result)
{
    LOG_ENTER;
    APP_ERROR_CHECK(event_result);
#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT
    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    LOG("Enter device_manager_init(), erase_bonds = %d\r\n", erase_bonds);
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

static void alert_phone(void)
{
    strcpy((char*)g_pstorage_buffer, "alert");
    ble_nus_string_send(&m_nus, g_pstorage_buffer, 5);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated when button is pressed.
 */
static void bsp_event_handler(bsp_event_t event)
{
    //LOG_ENTER;
    static uint8_t flag = 1;
    uint32_t err_code;
    bsp_indication_t ind;
    LOG(LEVEL_INFO, "event = %d", event);
    switch (event)
    {
    case BSP_EVENT_SLEEP:
        //sleep_mode_enter();
        break;

    case BSP_EVENT_DISCONNECT:
        break;

    case BSP_EVENT_KEY_0:        
        break;

    case BSP_EVENT_KEY_1:
        break;

    case BSP_EVENT_INTO_CONNECTABLE:
        LOG("BSP_EVENT_INTO_CONNECTABLE\r\n");
        if (flag)
        {
            flag = 0;
            open_LEDs();
        }
        else
        {
            flag = 1;
            close_LEDS();
        }
        tethys_factory_reset();
        break;

    case BSP_EVENT_INTO_NON_CONNECTABLE:
        LOG("BSP_EVENT_INTO_NON_CONNECTABLE\r\n");
        break;

    case BSP_EVENT_ALERT:
        LOG("BSP_EVENT_ALERT\r\n");
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)alert_phone);
        break;
    default:
        break;
    }
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool *p_erase_bonds)
{
    //LOG_ENTER;
    //bsp_event_t startup_event;
#if 1
    uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                                 bsp_event_handler);
#else
    uint32_t err_code = bsp_init(BSP_INIT_LED,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                                 bsp_event_handler);
#endif
    APP_ERROR_CHECK(err_code);
#if 0 // Mimas just have one button.Its function is just wake up and bond.
    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
#elif 1
    err_code = bsp_btn_ble_init(NULL, NULL);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = false;
#else
    *p_erase_bonds = false;
#endif
}

/**@brief Function for configuring the Low Frequency Clock on the nRF51 Series.
 *        It will return when the clock has started.
 *
 */
static void lfclk_config(void)
{
    LOG_ENTER;
    NRF_CLOCK->LFCLKSRC            = (CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos);
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_LFCLKSTART    = 1;

    //  while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0)
    //  {
    //  }

    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
}

void pre_init()
{
    LOG_ENTER;
    //NRF_GPIO->DIRSET = 0;
    lfclk_config();
    //wakeup_rtc_config();
    __WFI();
}

#if 1
#ifdef OPEN_UART
#define UART_TX_BUF_SIZE                1024                                         /**< UART TX buffer size. */
#else
#define UART_TX_BUF_SIZE                16
#endif
#define UART_RX_BUF_SIZE                16                                         /**< UART RX buffer size. */

void uart_error_handle(app_uart_evt_t *p_event)
{
    #if 0
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
    #endif
}

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
void mimas_uart_init(void)
{
    uint32_t                     err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT( &comm_params,
                        UART_RX_BUF_SIZE,
                        UART_TX_BUF_SIZE,
                        uart_error_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
    APP_ERROR_CHECK(err_code);
#ifndef OPEN_UART
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}
#endif

nrf_drv_wdt_channel_id m_channel_id;
/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    //LEDS_OFF(LEDS_MASK);
    LOG_ENTER;
    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}

void wdt_config(void)
{
    uint32_t err_code = NRF_SUCCESS;
    //Configure WDT.
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
}

void get_nrf51822_temperature(void *p_event_data, uint16_t event_size)
{
    int32_t temp;
    sd_temp_get(&temp);
    LOG("temp = %d\r\n", temp);
    #if 0
    float cTemp = (temp - 32) * 5;
    cTemp /= 9.0;
    cTemp -= 7.0;
    #else
    float cTemp = (float)temp / 4;
    cTemp -= 3.0;
    #endif
    LOG("temperature:%.2f;\r\n", cTemp);
}

#if 0
static void ble_radio_notification_evt_handle(bool radio_active)
{
#if 0
    static uint32_t ti = 0;
    LOG("ra%u:%d\r\n", ti++, radio_active);
#endif
}

static void radio_notification_init(void)
{
    uint32_t err_code;

    err_code = ble_radio_notification_init(NRF_APP_PRIORITY_HIGH,
                                           NRF_RADIO_NOTIFICATION_DISTANCE_4560US,
                                           ble_radio_notification_evt_handle);
    APP_ERROR_CHECK(err_code);
}
#endif

static void handle_pstorage_update(uint32_t result)
{
    if (result == NRF_SUCCESS)
    {
        LOG("Update operation successful.\r\n");
    }
    else
    {
        LOG("Update operation failed.\r\n");
    }
    switch (g_pstorage_state)
    {
        case PSTORAGE_STATE_UPDATE_UUID:
            if (result == NRF_SUCCESS)
            {
                memcpy(&m_beacon_info[BEACON_UUID_OFFSET], g_pstorage_buffer, 16);
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
                
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again0");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
        case PSTORAGE_STATE_UPDATE_MAJOR:
            if (result == NRF_SUCCESS)
            {
            #ifdef OBFUSCATING_ALGORITHMS
                memcpy(&m_beacon_info_major[0], g_pstorage_buffer, 2);
                obfuscating_algorithms();
            #else
                memcpy(&m_beacon_info[BEACON_MAJOR_OFFSET], g_pstorage_buffer, 2);
            #endif
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
                
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again1");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
        case PSTORAGE_STATE_UPDATE_MINOR:
            if (result == NRF_SUCCESS)
            {
                memcpy(&m_beacon_info[BEACON_MINOR_OFFSET], g_pstorage_buffer, 1);
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
                
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again2");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
        case PSTORAGE_STATE_UPDATE_CALIBRATED_TX_POWER:
            if (result == NRF_SUCCESS)
            {
                memcpy(&m_beacon_info[22], g_pstorage_buffer, 1);
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
                
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again3");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
        case PSTORAGE_STATE_UPDATE_ADV_INTERVAL:
            if (result == NRF_SUCCESS)
            {
                g_interval = (uint16_t)((float)g_pstorage_buffer[0] * (float)100 / (float)0.625);
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again4");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
        case PSTORAGE_STATE_UPDATE_TX_POWER:
            if (result == NRF_SUCCESS)
            {
                switch (g_pstorage_buffer[0])
                {
                    case 0:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_0dBm;
                        break;
                    case 1:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Pos4dBm;
                        break;
                    case 2:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg4dBm;
                        break;
                    case 3:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg8dBm;
                        break;
                    case 4:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg12dBm;
                        break;
                    case 5:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg16dBm;
                        break;
                    case 6:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg20dBm;
                        break;
                    case 7:
                        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg30dBm;
                        break;
                }
                strcpy((char*)g_pstorage_buffer, "OK");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 2);
                uint32_t err_code = sd_ble_gap_tx_power_set(g_device_tx_power);
                LOG("err_code = 0x%x\r\n", err_code);
                ble_tps_tx_power_level_set(&m_tps, g_device_tx_power);
            }
            else
            {
                strcpy((char*)g_pstorage_buffer, "FAIL:Try again5");
                ble_nus_string_send(&m_nus, g_pstorage_buffer, strlen((char*)g_pstorage_buffer));
            }
            break;
    }
}

static void pstorage_callback_handler(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        handle_pstorage_update(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void pstorage_mimas_init(void)
{
    pstorage_module_param_t param;

    param.block_size  = BLOCK_SIZE;//4
    param.block_count = BLOCK_COUNT;
    param.cb          = pstorage_callback_handler;

    uint32_t err_code = pstorage_register(&param, &g_storage_handle);
    APP_ERROR_CHECK(err_code);
}

#if 0
static void pstorage_update_do(void *p_event_data, uint16_t event_size)
{
    pstorage_handle_t block_handle;
    static uint8_t source_data[4] = {0x15, 0x76, 0x9A, 0xF2};
    pstorage_block_identifier_get(&g_storage_handle, 0, &block_handle);
    pstorage_update(&block_handle, source_data, 4, 12);
    LOG("block_handle:0x%x\r\n", &block_handle);
}
#endif

static void gpio_init(void)
{
    uint32_t i = 0;
    for(i = 0; i < 32 ; ++i )
    {
        NRF_GPIO->PIN_CNF[i] = (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
                               | (GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)
                               | (GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)
                               | (GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)
                               | (GPIO_PIN_CNF_DIR_Input << GPIO_PIN_CNF_DIR_Pos);
    }
}

#ifdef OBFUSCATING_ALGORITHMS
static void obfuscating_algorithms(void)
{
    m_beacon_info[BEACON_MAJOR_OFFSET] = m_beacon_info_major[0] ^ m_beacon_info[BEACON_TEMP_OFFSET];
    m_beacon_info[BEACON_MAJOR_OFFSET+1] = m_beacon_info_major[1] ^ m_beacon_info[BEACON_BATTERY_OFFSET];
}

static void obfuscating_algorithms_init(void)
{
    m_beacon_info[BEACON_STATE_OFFSET] = (OBFUSCATING_ALGORITHMS_1 << OBFUSCATING_ALGORITHMS_Pos) |
                                         (ACTIVE_INACTIVE_1 << ACTIVE_INACTIVE_Pos);
    m_beacon_info[BEACON_MAJOR_OFFSET] = m_beacon_info_major[0] ^ m_beacon_info[BEACON_TEMP_OFFSET];
    m_beacon_info[BEACON_MAJOR_OFFSET+1] = m_beacon_info_major[1] ^ m_beacon_info[BEACON_BATTERY_OFFSET];
}
#endif

void tethys_adv_algorithms_init(void)
{
    if (gTethys == 0)//We still not get the license.
    {
        m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] & ~ACTIVE_BIND_Msk;
    }
    else
    {
        m_beacon_info[BEACON_STATE_OFFSET] = m_beacon_info[BEACON_STATE_OFFSET] | 
                                         (ACTIVE_BIND_1 << ACTIVE_BIND_Pos);
    }
}

#ifdef ADXL362_OPEN
static void ADXL362_interrupt_init(void)
{
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    uint32_t mode = *(uint32_t *)0x100010A4;
    g_adxl362_work_mode = mode;
    if (g_adxl362_work_mode == 0)
    {
        in_config.is_watcher = false;
        in_config.hi_accuracy = false;
        in_config.pull = NRF_GPIO_PIN_PULLDOWN;
        in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
        err_code = nrf_drv_gpiote_in_init(ADXL362_INT1_PIN, &in_config, ADXL362_int1_pin_handler);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT1_PIN, true);

        err_code = nrf_drv_gpiote_in_init(ADXL362_INT2_PIN, &in_config, ADXL362_int2_pin_handler);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT2_PIN, true);
    }
    else if (g_adxl362_work_mode == 1)
    {
        in_config.is_watcher = false;
        in_config.hi_accuracy = false;
        in_config.pull = NRF_GPIO_PIN_NOPULL;
        in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
        err_code = nrf_drv_gpiote_in_init(ADXL362_INT1_PIN, &in_config, ADXL362_int2_pin_handler_toggle);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT1_PIN, true);
    }
    else if (g_adxl362_work_mode == 2)
    {
        in_config.is_watcher = false;
        in_config.hi_accuracy = false;
        in_config.pull = NRF_GPIO_PIN_NOPULL;
        in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
        err_code = nrf_drv_gpiote_in_init(ADXL362_INT2_PIN, &in_config, ADXL362_int2_pin_handler_toggle);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT2_PIN, true);
    }
    else
    {
        in_config.is_watcher = false;
        in_config.hi_accuracy = false;
        in_config.pull = NRF_GPIO_PIN_PULLDOWN;
        in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
        err_code = nrf_drv_gpiote_in_init(ADXL362_INT1_PIN, &in_config, ADXL362_int1_pin_handler);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT1_PIN, true);

        err_code = nrf_drv_gpiote_in_init(ADXL362_INT2_PIN, &in_config, ADXL362_int2_pin_handler);
        APP_ERROR_CHECK(err_code);
        nrf_drv_gpiote_in_event_enable(ADXL362_INT2_PIN, true);
    }
    //nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    //nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(false);
    //nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
    //in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    //printf("ADXL362_interrupt_init\r\n");
}
#endif

/**@snippet [UART Initialization] */
/**@brief Function for application main entry.
 */
int main(void)
{
    bool erase_bonds;

    gpio_init();
#ifdef OBFUSCATING_ALGORITHMS
    obfuscating_algorithms_init();
#endif
    //pre_init();
    // Initialize.
    //nrf_mem_init(); 
    //app_trace_init();
    //mimas_uart_init();
    LOG("DCDCEN:%x; \r\n", NRF_POWER->DCDCEN);
    timers_init();
    tethys_communicate_init();
    scheduler_init();
    buttons_leds_init(&erase_bonds);
    ble_stack_init();//协议栈初始化
    adc_configure();
    device_manager_init(erase_bonds);
    pstorage_mimas_init();// Must after device_manager_init and scheduler_init function.
    tethys_storage_init();
#if 1
    system_clock_init();
#endif
    tethys_adv_algorithms_init();
    advertising_pre_init_beacon();
    gap_params_init();//Generic Access Profile
    //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
    services_init();//需要开启的服务初始化
    conn_params_init();//连接参数设置
    //radio_notification_init();
#ifdef BUZZER_OPEN
    nrf_gpio_cfg_output(BUZZER_OUT);
    nrf_gpio_pin_clear(BUZZER_OUT);
    buzzer_init();
    buzzer_start();
    nrf_gpio_pin_clear(BUZZER_OUT);
#endif
#ifdef ADXL362_OPEN
/*
    nrf_gpio_cfg(
            16,
            NRF_GPIO_PIN_DIR_OUTPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_NOPULL,
            NRF_GPIO_PIN_H0H1,
            NRF_GPIO_PIN_NOSENSE);
    */
    #ifdef MIMAS_VER3
    nrf_gpio_cfg_output(15);
    nrf_gpio_pin_clear(15);
    #else
    nrf_gpio_cfg_output(14);
    nrf_gpio_cfg_output(15);
    nrf_gpio_cfg_output(16);
    nrf_gpio_pin_clear(14);
    nrf_gpio_pin_clear(15);
    nrf_gpio_pin_clear(16);
    #endif
#endif
    sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
    // Start execution.
    advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);

    //LOG("Before enter loop...\r\n");
    LOG("PSTORAGE_FLASH_PAGE_SIZE : %d\r\n", PSTORAGE_FLASH_PAGE_SIZE);
    LOG("CODESIZE : %u\r\n", NRF_FICR->CODESIZE);
    LOG("CODEPAGESIZE : %u\r\n", NRF_FICR->CODEPAGESIZE);
    LOG("BOOTLOADER_ADDRESS : 0x%x\r\n", BOOTLOADER_ADDRESS);

#if 0
    uint32_t err_code;
    err_code = app_timer_start(m_temperature_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
    temperature_timeout_handler(NULL);
#endif
#if 0
    nrf_adc_start();
    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
#endif
    init_battery();
#if 1
    init_LED();
#endif
#if 1 // Init MFRC522
    //g_timer_task = 6;
    //app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(999, APP_TIMER_PRESCALER), NULL);
    init_MFRC522();
#endif
#if 1 // Init MOTO
    init_Moto();
#endif
#if 1 // Init speaker
    init_LaBa();
    //g_timer_task = 5;
    //app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(8000, APP_TIMER_PRESCALER), NULL);
#endif
#if 1 // Init fingerprint
    nrf_gpio_cfg_output(FINGERPRINT_EN_PIN);
#ifdef FP_POWER_ALWAYS_ON
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
#else
    nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);
#endif
    nrf_gpio_cfg_output(FINGERPRINT_PW_PIN);
    nrf_gpio_pin_set(FINGERPRINT_PW_PIN);
    //nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    g_timer_task = 8;
    app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER), NULL);
    //init_Fingerprint();
#endif
#if 1 // Init TTP229
    init_ttp229bsf();
#endif
#ifdef TETHYS_PWM
    led_pwm_init_1();
#endif
    //err_code = app_timer_start(m_reset_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    //APP_ERROR_CHECK(err_code);

    
    // Enter main loop.

    //wdt_config();
    //LOG("SCHED_MAX_EVENT_DATA_SIZE = %d\r\n", SCHED_MAX_EVENT_DATA_SIZE);
    //LOG("RESETREAS:%x; POWER_RESETREAS_RESETPIN_Msk:%x\r\n", NRF_POWER->RESETREAS, POWER_RESETREAS_RESETPIN_Msk);
    //printf("DCDCEN:%x; \r\n", NRF_POWER->DCDCEN);
    //printf("RESETREAS:%x; \r\n", NRF_POWER->RESETREAS);
    
    //LOG("uint64_t:%d", sizeof(uint64_t));
    //NRF_POWER->RESETREAS = 0;
    //NRF_POWER->RESETREAS &= POWER_RESETREAS_SREQ_Msk;
    //LOG("RESETREAS:%x; POWER_RESETREAS_RESETPIN_Msk:%x\r\n",NRF_POWER->RESETREAS,POWER_RESETREAS_RESETPIN_Msk);
    //app_sched_event_put(NULL, 0, get_nrf51822_temperature);
#ifdef ADXL362_OPEN
    ADXL362_interrupt_init();
    app_timer_start(m_adxl362_timer_id, ADXL362_INTERVAL, NULL);
#endif
    mimas_loop();
}

/**
 * @}
 */
