#ifndef MIMAS_LOG_H
#define MIMAS_LOG_H

#include <stdbool.h>
#include <stdint.h>
#include "app_trace.h"
//#define LOG app_trace_log

#define LEVEL_VERBOSE       1
#define LEVEL_INFO          2
#define LEVEL_DEBUG         3
#define LEVEL_WARNING       4
#define LEVEL_ERROR         5
#define LEVEL_NONE          6

#define __LEVEL__   LEVEL_VERBOSE
#if 1
void __log(uint8_t level, const char * func, uint32_t line, const char * restrict format, ...);
#if defined(DEBUG_LOG) || defined(DEBUG_ACC) || defined(DEBUG_PHILL)
#define LOG1(level, format, ...) __log(level, __func__, __LINE__, format, ##__VA_ARGS__);
#define LOG app_trace_log
#define LOG_ENTER LOG(__LEVEL__, "Enter %s\r\n", __func__);
#else
#define LOG1(level, format, ...)
#define LOG(...)
#define LOG_ENTER 
#endif
#else
#define LOG(level, format, ...)                   \
        if( level >= __LEVEL__ ) {                                          \
        char    str[64];                                \
        snprintf(str, sizeof(str), "[%d|%s|%d]"format,      \
        level,__func__,__LINE__,##__VA_ARGS__);\
        simple_uart_putstring((const uint8_t *)str);                    \
        }
#endif

/*lint --flb "Leave library region" */
#endif

