#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#ifdef AKII
// LEDs definitions for CUSTOM_BOARD AK-II
#define LEDS_NUMBER    5

#define LED_START      18
#define LED_1          18
#define LED_2          19
#define LED_3          20
#define LED_4          21
#define LED_5          22
#define LED_STOP       22

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4
#define BSP_LED_4      LED_5

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)
#define BSP_LED_2_MASK (1<<BSP_LED_2)
#define BSP_LED_3_MASK (1<<BSP_LED_3)
#define BSP_LED_4_MASK (1<<BSP_LED_4)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK | BSP_LED_2_MASK | BSP_LED_3_MASK | BSP_LED_4_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~(LEDS_MASK))
////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 2

#define BUTTON_START   16
#define BUTTON_1       16
#define BUTTON_2       17
#define BUTTON_STOP    17
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)

#define BUTTONS_MASK   0x00030000
#if 0
#define RX_PIN_NUMBER  14
#define TX_PIN_NUMBER  12
#define CTS_PIN_NUMBER 10
#define RTS_PIN_NUMBER 8
#define HWFC           false
#else
#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  9
#define CTS_PIN_NUMBER 10
#define RTS_PIN_NUMBER 11
#define HWFC           false
#endif
#else // Not defined AKII
///////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 1
#define BUTTON_START   0
#define BUTTON_1       0
#define BUTTON_STOP    0
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   0x00000001

#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  4
#define CTS_PIN_NUMBER 8
#define RTS_PIN_NUMBER 9
#define HWFC           false

#endif //#ifdef AKII
////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 On AKII board,pin 26,27 used for 32768; pin 16~17 for button and 18~22 used for led.
 pin 1,2,3 used for MPU6050.
 */
/* Speaker */
 //PA2
#define LABA_BUSY 21
 //PA1
#define LABA_SDA  22

/* Fingerprint */
#define FINGERPRINT_RX_PIN     29 
#define FINGERPRINT_TX_PIN     28
#if 0
#define FINGERPRINT_RTS_PIN    24
#define FINGERPRINT_CTS_PIN    30
#endif
#define FINGERPRINT_DETECT_PIN 25
#define FINGERPRINT_EN_PIN     23
#define FINGERPRINT_PLUG_PIN   24 //NOT used now
#define FINGERPRINT_PW_PIN     30 //NOT used

/* TTP229BSF */
#define TTP229BSF_SDO    6
#define TTP229BSF_SCL    7

/* RFID(RC522) */
#define SPIM0_SCK_PIN       19     /**< SPI clock GPIO pin number. */
#define SPIM0_MOSI_PIN      18     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM0_MISO_PIN      17     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM0_SS_PIN        20     /**< SPI Slave Select GPIO pin number. */
#define NRSTPD_PIN          15
#define RF_DET_PIN          16
//#define RC522_IRQ_PIN       4

/* LEDs */
#define LED_CO_1 11
#define LED_CO_2 10
#define LED_CO_3 9
#define LED_CO_4 8
#define LED_RO_1 14
#define LED_RO_2 13
#define LED_RO_3 12

#define ADC_VBAT_PIN 1 //Should be 'NRF_ADC_CONFIG_INPUT_2'

#define MOTO_B 2
#define MOTO_A 3

// For test leds
#define LEDS_NUMBER    7

#define LED_START      8

#define LED_1          8 //col 4
#define LED_2          9 //col 3
#define LED_3          10//col 2
#define LED_4          11//col 1

#define LED_5          12//rol 3
#define LED_6          13//rol 2
#define LED_7          14//rol 1

#define LED_STOP       14

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5, LED_6, LED_7 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4
#define BSP_LED_4      LED_5
#define BSP_LED_5      LED_6
#define BSP_LED_6      LED_7

#define BSP_LED_COL_4_MASK (1<<BSP_LED_0)
#define BSP_LED_COL_3_MASK (1<<BSP_LED_1)
#define BSP_LED_COL_2_MASK (1<<BSP_LED_2)
#define BSP_LED_COL_1_MASK (1<<BSP_LED_3)

#define BSP_LED_ROL_3_MASK (1<<BSP_LED_4)
#define BSP_LED_ROL_2_MASK (1<<BSP_LED_5)
#define BSP_LED_ROL_1_MASK (1<<BSP_LED_6)

#define LEDS_MASK (BSP_LED_COL_4_MASK|BSP_LED_COL_3_MASK|BSP_LED_COL_2_MASK|BSP_LED_COL_1_MASK|BSP_LED_ROL_3_MASK |BSP_LED_ROL_2_MASK|BSP_LED_ROL_1_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK (~(LEDS_MASK))

#define LEDS_COL_MASK (BSP_LED_COL_4_MASK | BSP_LED_COL_3_MASK | BSP_LED_COL_2_MASK | BSP_LED_COL_1_MASK)
#define LEDS_ROL_MASK (BSP_LED_ROL_3_MASK |BSP_LED_ROL_2_MASK|BSP_LED_ROL_1_MASK)

#define TETHYS_LED_ROL_1 LED_7
#define TETHYS_LED_ROL_2 LED_6
#define TETHYS_LED_ROL_3 LED_5
#define TETHYS_LED_COL_1 LED_4
#define TETHYS_LED_COL_2 LED_3
#define TETHYS_LED_COL_3 LED_2
#define TETHYS_LED_COL_4 LED_1


#define BSP_LED_0_MASK BSP_LED_COL_4_MASK
#define BSP_LED_1_MASK BSP_LED_COL_4_MASK
#define BSP_LED_2_MASK BSP_LED_COL_4_MASK
#define BSP_LED_3_MASK BSP_LED_COL_4_MASK
#define BSP_LED_4_MASK BSP_LED_ROL_3_MASK
#define BSP_LED_5_MASK BSP_LED_ROL_2_MASK
#define BSP_LED_6_MASK BSP_LED_ROL_1_MASK
#endif // CUSTOM_BOARD_H
