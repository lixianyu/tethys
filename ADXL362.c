/***************************************************************************//**
 *   @file   ADXL362.c
 *   @brief  Implementation of ADXL362 Driver.
 *   @author DNechita(Dan.Nechita@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: $WCREV$
*******************************************************************************/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <string.h>
#include "ADXL362.h"
#include "mimas_bsp.h"
#include "nrf_drv_config.h"
#if (SPI0_ENABLED == 1)
#include <nrf_drv_spi.h>
#endif
#include "app_util_platform.h"
#include "nrf_delay.h"

//#include "Communication.h"

/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
extern uint8_t g_adxl362_work_mode;
char selectedRange = 0;
static volatile bool m_transfer_completed = true;
#define TX_RX_MSG_LENGTH         32

static uint8_t m_tx_data_spi[TX_RX_MSG_LENGTH]; ///< SPI master TX buffer.
static uint8_t m_rx_data_spi[TX_RX_MSG_LENGTH]; ///< SPI master RX buffer.
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/

#if (SPI0_ENABLED == 1)
static const nrf_drv_spi_t m_spi_master_0 = NRF_DRV_SPI_INSTANCE(0);
#endif
#if (SPI1_ENABLED == 1)
static const nrf_drv_spi_t m_spi_master_1 = NRF_DRV_SPI_INSTANCE(1);
#endif
#if (SPI2_ENABLED == 1)
static const nrf_drv_spi_t m_spi_master_2 = NRF_DRV_SPI_INSTANCE(2);
#endif

#if (SPI0_ENABLED == 1)
/**@brief Handler for SPI0 master events.
 *
 * @param[in] event SPI master event.
 */
void spi_master_0_event_handler(nrf_drv_spi_event_t event)
{
    uint32_t err_code = NRF_SUCCESS;
    //bool result = false;

    switch (event)
    {
        case NRF_DRV_SPI_EVENT_DONE:
            // Check if received data is correct.
            //result = check_buf_equal(m_tx_data_spi, m_rx_data_spi, TX_RX_MSG_LENGTH);
            //APP_ERROR_CHECK_BOOL(result);

            nrf_drv_spi_uninit(&m_spi_master_0);

            err_code = bsp_indication_set(BSP_INDICATE_RCV_OK);
            APP_ERROR_CHECK(err_code);

            m_transfer_completed = true;
            break;

        default:
            // No implementation needed.
            break;
    }
}
#endif // (SPI0_ENABLED == 1)

#if (SPI0_ENABLED == 1)
/**@brief Function for initializing a SPI master driver.
 *
 * @param[in] p_instance    Pointer to SPI master driver instance.
 * @param[in] lsb           Bits order LSB if true, MSB if false.
 */
static void spi_master_init_nrf(nrf_drv_spi_t const * p_instance, bool lsb)
{
    uint32_t err_code = NRF_SUCCESS;

    nrf_drv_spi_config_t config =
    {
        .ss_pin       = SPIM0_SS_PIN,
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_4M,
        .mode         = NRF_DRV_SPI_MODE_0,
        .bit_order    = (lsb ?
            NRF_DRV_SPI_BIT_ORDER_LSB_FIRST : NRF_DRV_SPI_BIT_ORDER_MSB_FIRST),
    };

    #if (SPI0_ENABLED == 1)
    if (p_instance == &m_spi_master_0)
    {
        config.sck_pin  = SPIM0_SCK_PIN;
        config.mosi_pin = SPIM0_MOSI_PIN;
        config.miso_pin = SPIM0_MISO_PIN;
        //err_code = nrf_drv_spi_init(p_instance, &config, spi_master_0_event_handler);
        err_code = nrf_drv_spi_init(p_instance, &config, NULL);
    }
    else
    #endif // (SPI0_ENABLED == 1)

    #if (SPI1_ENABLED == 1)
    if (p_instance == &m_spi_master_1)
    {
        config.sck_pin  = SPIM1_SCK_PIN;
        config.mosi_pin = SPIM1_MOSI_PIN;
        config.miso_pin = SPIM1_MISO_PIN;
        err_code = nrf_drv_spi_init(p_instance, &config,
            spi_master_1_event_handler);
    }
    else
    #endif // (SPI1_ENABLED == 1)

    #if (SPI2_ENABLED == 1)
    if (p_instance == &m_spi_master_2)
    {
        config.sck_pin  = SPIM2_SCK_PIN;
        config.mosi_pin = SPIM2_MOSI_PIN;
        config.miso_pin = SPIM2_MISO_PIN;
        err_code = nrf_drv_spi_init(p_instance, &config,
            spi_master_2_event_handler);
    }
    else
    #endif // (SPI2_ENABLED == 1)

    {}

    APP_ERROR_CHECK(err_code);
}
#endif

void ADXL362_spi_master_uninit(void)
{
    nrf_drv_spi_uninit(&m_spi_master_0);
}

/***************************************************************************//**
 * @brief Initializes communication with the device and checks if the part is
 *        present by reading the device id.
 *
 * @return  0 - the initialization was successful and the device is present;
 *         -1 - an error occurred.
*******************************************************************************/
char ADXL362_Init(void)
{
    unsigned char regValue = 0;
    int8_t          status   = 0;

    //status = SPI_Init(0, 4000000, 0, 1);
    #if (SPI0_ENABLED == 1)
    spi_master_init_nrf(&m_spi_master_0, false);
    #endif
    /* Initialise SPI */
    /*!< Sample data at rising edge of clock and shift serial data at falling edge */
    /*!< MSb first */
    /*uint32_t *spi_base_address = spi_master_init(SPI0, SPI_MODE0, false);
    if (spi_base_address == 0) {
        return 2;
    }
    spi_master_enable(SPI0);
    */
    #if 0
    ADXL362_GetRegisterValue(&regValue, ADXL362_REG_PARTID, 1);
    if((regValue != ADXL362_PART_ID))
    {
        status = 1;
    }
    #endif
    selectedRange = 2; // Measurement Range: +/- 2g (reset default).
    //spi_master_disable(SPI0);
    return status;
}

#if 0
void ADXL326Init(void)
{
    #if (SPI0_ENABLED == 1)
    spi_master_init_nrf(&m_spi_master_0, false);
    #endif
    ADXL362RegisterWrite(ADXL362_REG_SOFT_RESET,0x52);   // software reset  
    nrf_delay_ms(20);
#if 1
    ADXL362RegisterWrite(0x20,0x1F);   // 0x20 THRESH_ACT_L  
    ADXL362RegisterWrite(0x21,0x00);   // 0x21 THRESH_ACT_H  
    ADXL362RegisterWrite(0x22,50);     // 0x22 TIME_ACT  
    ADXL362RegisterWrite(0x23,0x09);   // 0x23 THRESH_INACT_L  
    ADXL362RegisterWrite(0x24,0x00);   // 0x24 THRESH_INACT_H  
    ADXL362RegisterWrite(0x25,0x01);   // 0x25 TIME_INACT_L  
    ADXL362RegisterWrite(0x26,0x00);   // 0x26 TIME_INACT_H  
    ADXL362RegisterWrite(0x27,0x03);   // 0x27 ACT_INACT_CTL   ????  
    //ADXL362_Write_Reg(0x28,0x00);   // 0x28 FIFO_CONTROL  
    //ADXL362_Write_Reg(0x29,0x80);   // 0x29 FIFO_SAMPLES  
    ADXL362RegisterWrite(0x28,0x01);   // 0x28 FIFO_CONTROL  Stream mode  0x09  
    ADXL362RegisterWrite(0x29,0x33);   // 0x29 FIFO_SAMPLES  0xfe--> 510 byte  
    ADXL362RegisterWrite(ADXL362_REG_INTMAP1,0x84);   // 0x2a INTMAP1  Data_ready ??? Int1,???  
    ADXL362RegisterWrite(ADXL362_REG_INTMAP2,0x90);   // 0x2b INTMAP2  avtivity ???Int2,???  
    ADXL362RegisterWrite(0x2C,0x02);   // 0x2c FILTER_CTL  0x82 50hz 0x84 200hz 

    ADXL362RegisterWrite(0x2D,0x02);   // 0x2d POWER_CTL  Measurement mode.  
#else
    uint8_t sendbuf[14] = {0x1F, 0x00, 50, 0x09, 0, 1, 0, 3, 1, 0x33, 0x84, 0x90, 0x82, 0x02};
    ADXL362BurstWrite(0x20, 14, sendbuf);
#endif
    nrf_delay_ms(200);  
    //ADXL362RegisterRead(ADXL362_REG_DEVID_AD);  
    //ADXL362_Burst_Read_Reg(0x0e,6,databuf);  
}
#else //Autonomous Motion Switch
#define DELAY_TIME 1
void ADXL326Init(void)
{
    #if (SPI0_ENABLED == 1)
    spi_master_init_nrf(&m_spi_master_0, false);
    #endif
    ADXL362RegisterWrite(ADXL362_REG_SOFT_RESET,0x52);   // software reset  
    nrf_delay_ms(20);
#if 0
    ADXL362RegisterWrite(ADXL362_REG_THRESH_ACT_L, 99);//  sets activity threshold to 250 mg
    nrf_delay_ms(DELAY_TIME);
    ADXL362RegisterWrite(ADXL362_REG_THRESH_ACT_H, 0);
    nrf_delay_ms(DELAY_TIME);

    ADXL362RegisterWrite(ADXL362_REG_THRESH_INACT_L, 150); // sets inactivity threshold to 150 mg
    nrf_delay_ms(DELAY_TIME);
    ADXL362RegisterWrite(ADXL362_REG_THRESH_INACT_H, 0);
    nrf_delay_ms(DELAY_TIME);

    ADXL362RegisterWrite(ADXL362_REG_TIME_INACT_L, 30);// sets inactivity timer to 30 samples or about 5 seconds
    nrf_delay_ms(DELAY_TIME);
    ADXL362RegisterWrite(ADXL362_REG_TIME_INACT_H, 0);
    nrf_delay_ms(DELAY_TIME);

    ADXL362RegisterWrite(ADXL362_REG_ACT_INACT_CTL, 0x3F); //configures motion detection in loop mode and enables referenced activity and inactivity detection
    nrf_delay_ms(DELAY_TIME);

    ADXL362RegisterWrite(ADXL362_REG_INTMAP1, 0x10);// map the INACT bit to INT1.
    nrf_delay_ms(DELAY_TIME);
    ADXL362RegisterWrite(ADXL362_REG_INTMAP2, 0x20);// map the AWAKE & ACT bit to INT2.
    nrf_delay_ms(DELAY_TIME);

    ADXL362RegisterWrite(ADXL362_REG_FILTER_CTL, 0x00);// 2g, 12.5Hz
    nrf_delay_ms(DELAY_TIME);
    ADXL362RegisterWrite(ADXL362_REG_POWER_CTL, 0x0E);//  begins the measurement in wake-up mode
#else // 0x20 ~ 0x2D
    uint8_t sendbuf[14] = {
        99, // 0x20, ADXL362_REG_THRESH_ACT_L
        0,  // 0x21, ADXL362_REG_THRESH_ACT_H , 99mg
        0,  // 0x22, ADXL362_REG_TIME_ACT

        150,// 0x23, ADXL362_REG_THRESH_INACT_L
        0,  // 0x24, ADXL362_REG_THRESH_INACT_H , 150mg
        30, // 0x25, ADXL362_REG_TIME_INACT_L
        0x00,  // 0x26, ADXL362_REG_TIME_INACT_H

        0x3F,// 0x27, ADXL362_REG_ACT_INACT_CTL : Loop Mode
        0x00, // 0x28, ADXL362_REG_FIFO_CTL
        0x80, // 0x29, ADXL362_REG_FIFO_SAMPLES

        0x10, // 0x2A, ADXL362_REG_INTMAP1
        0x20, // 0x2B, ADXL362_REG_INTMAP2

        0x00, // 0x2C, ADXL362_REG_FILTER_CTL , Measurement Range is 2g
        0x0E, // 0x2D, ADXL362_REG_POWER_CTL , 
    };
    if (g_adxl362_work_mode == 0)
    {
        sendbuf[10] = 0x10;
        sendbuf[11] = 0x20;
    }
    else if (g_adxl362_work_mode == 1)
    {
        sendbuf[10] = 0x50;
        sendbuf[11] = 0x00;
    }
    else if (g_adxl362_work_mode == 2)
    {
        sendbuf[10] = 0x00;
        sendbuf[11] = 0x50;
    }
    else
    {
        sendbuf[10] = 0x10;
        sendbuf[11] = 0x20;
    }
    ADXL362BurstWrite(ADXL362_REG_THRESH_ACT_L, 14, sendbuf);
#endif
    nrf_delay_ms(200);  
    //ADXL362RegisterRead(ADXL362_REG_DEVID_AD);  
    //ADXL362_Burst_Read_Reg(0x0e,6,databuf);  
}
#endif
/***************************************************************************//**
 * @brief Writes data into a register.
 *
 * @param registerValue   - Data value to write.
 * @param registerAddress - Address of the register.
 * @param bytesNumber     - Number of bytes. Accepted values: 0 - 1.
 *
 * @return None.
*******************************************************************************/
void ADXL362_SetRegisterValue(unsigned short registerValue,
                              unsigned char  registerAddress,
                              unsigned char  bytesNumber)
{
    unsigned char buffer[4] = {0, 0, 0, 0};

    buffer[0] = ADXL362_WRITE_REG;
    buffer[1] = registerAddress;
    buffer[2] = (registerValue & 0x00FF);
    buffer[3] = (registerValue >> 8);
    //SPI_Write(ADXL362_SLAVE_ID, buffer, bytesNumber + 2);
#if (SPI0_ENABLED == 1)
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, buffer, 4, NULL, 0);
    APP_ERROR_CHECK(err_code);
#endif
}
void ADXL362RegisterWrite(unsigned char Address, unsigned char SendValue)
{    
    unsigned char SendTemp[3];
    unsigned char ReceiveTemp[3];
    
    //DioClr(pADI_GP1,0x80);              //CS down
    m_tx_data_spi[0] = 0x0A;                 //0x0A: write register
    m_tx_data_spi[1] = Address;              //address byte
    m_tx_data_spi[2] = SendValue;
    
    //SpiFunction(SendTemp, ReceiveTemp, 3);
    //DioSet(pADI_GP1,0x80);              //CS up
    #if (SPI0_ENABLED == 1)
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 3, m_rx_data_spi, 3);
    APP_ERROR_CHECK(err_code);
    #endif
}
void ADXL362BurstWrite(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
{ 
    unsigned char RegisterIndex;
  
    //DioClr(pADI_GP1,0x80);              //CS down
    m_tx_data_spi[0] = 0x0A;                 //0x0A: write register
    m_tx_data_spi[1] = Address;              //address byte  
    for (RegisterIndex = 0; RegisterIndex < NumberofRegisters; RegisterIndex++)
    {    
        m_tx_data_spi[2 + RegisterIndex] = *(RegisterData + RegisterIndex);
        //SpiFunction(SendTemp, ReceiveTemp, 1);       
    }
    
    //SpiFunction(SendTemp, ReceiveTemp, 2);
    nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 2 + NumberofRegisters, m_rx_data_spi, NumberofRegisters + 2);

       
    //DioSet(pADI_GP1,0x80);              //CS up
}
unsigned char ADXL362RegisterRead(unsigned char Address)
{   
    //unsigned char SendTemp[3];
    //unsigned char ReceiveTemp[3];
    unsigned char ReceiveValue;
 
    //DioClr(pADI_GP1,0x80);              //CS down  
    m_tx_data_spi[0] = 0x0B;                 //0x0B: read register command
    m_tx_data_spi[1] = Address;              //address byte
    m_tx_data_spi[2] = 0;  
    //SpiFunction(SendTemp, ReceiveTemp, 3);
#if (SPI0_ENABLED == 1)
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 3, m_rx_data_spi, 3);
    APP_ERROR_CHECK(err_code);
#endif

    ReceiveValue = m_rx_data_spi[2];   
    //DioSet(pADI_GP1,0x80);
    return(ReceiveValue);               //CS up
}
void ADXL362BurstRead(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
{    
    //unsigned char SendTemp[2];
    //unsigned char ReceiveTemp[2];
    unsigned char RegisterIndex;
  
    //DioClr(pADI_GP1,0x80);         //CS down
    m_tx_data_spi[0] = 0x0B;            //0x0B: read register  
    m_tx_data_spi[1] = Address;         //address byte  
    //SpiFunction(SendTemp, ReceiveTemp, 2);
    //SpiFifoFlush(pADI_SPI0,SPICON_TFLUSH_EN,SPICON_RFLUSH_EN);
    nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 2+NumberofRegisters, m_rx_data_spi, 2+NumberofRegisters);
  
//    SendTemp[0] = 0x00;
    for (RegisterIndex=0; RegisterIndex<NumberofRegisters; RegisterIndex++)
    {    
        //SpiFifoFlush(pADI_SPI0,SPICON_TFLUSH_EN,SPICON_RFLUSH_EN);
        //SpiFunction(SendTemp, ReceiveTemp, 1);
        *(RegisterData + RegisterIndex) = m_rx_data_spi[2+RegisterIndex];        
    }
    //DioSet(pADI_GP1,0x80);         //CS up
}
/***************************************************************************//**
 * @brief Performs a burst read of a specified number of registers.
 *
 * @param pReadData       - The read values are stored in this buffer.
 * @param registerAddress - The start address of the burst read.
 * @param bytesNumber     - Number of bytes to read.
 *
 * @return None.
*******************************************************************************/
#if 1
void ADXL362_GetRegisterValue(unsigned char* pReadData,
                              unsigned char  registerAddress,
                              unsigned char  bytesNumber)
{
    //unsigned char buffer[32];// = {0x24};
    unsigned char index = 0;
    memset(m_rx_data_spi, 0x99, sizeof(m_rx_data_spi));
    m_tx_data_spi[0] = ADXL362_READ_REG;
    m_tx_data_spi[1] = registerAddress;
    #if 0
    for(index = 0; index < bytesNumber; index++)
    {
        m_tx_data_spi[index + 2] = pReadData[index];
    }
    #endif
    //SPI_Read(ADXL362_SLAVE_ID, buffer, bytesNumber + 2);
#if (SPI0_ENABLED == 1)
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 2, m_rx_data_spi, bytesNumber + 2);
    APP_ERROR_CHECK(err_code);
#endif
    for(index = 0; index < bytesNumber; index++)
    {
        pReadData[index] = m_rx_data_spi[index + 2];
    }
}
#else
void ADXL362_GetRegisterValue(unsigned char* pReadData,
                              unsigned char  registerAddress,
                              unsigned char  bytesNumber)
{
    unsigned char index = 0;
    
    m_tx_data_spi[0] = ADXL362_READ_REG;
    m_tx_data_spi[1] = registerAddress;
    #if 0
    for(index = 0; index < bytesNumber; index++)
    {
        m_tx_data_spi[index + 2] = pReadData[index];
    }
    #endif
    //SPI_Read(ADXL362_SLAVE_ID, buffer, bytesNumber + 2);
    //uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, m_tx_data_spi, 2, m_rx_data_spi, 1);
    //APP_ERROR_CHECK(err_code);
    spi_master_tx_rx((uint32_t *)NRF_SPI0, 3, m_tx_data_spi, m_rx_data_spi)
    for(index = 0; index < bytesNumber; index++)
    {
        pReadData[index] = m_rx_data_spi[index + 2];
    }
}

#endif
/***************************************************************************//**
 * @brief Reads multiple bytes from the device's FIFO buffer.
 *
 * @param pBuffer     - Stores the read bytes.
 * @param bytesNumber - Number of bytes to read.
 *
 * @return None.
*******************************************************************************/
void ADXL362_GetFifoValue(unsigned char* pBuffer, unsigned short bytesNumber)
{
    unsigned char  buffer[512];
    unsigned short index = 0;

    buffer[0] = ADXL362_WRITE_FIFO;
    for(index = 0; index < bytesNumber; index++)
    {
        buffer[index + 1] = pBuffer[index];
    }
    //SPI_Read(ADXL362_SLAVE_ID, buffer, bytesNumber + 1);
#if (SPI0_ENABLED == 1)
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_0, buffer, 1, buffer + 1, bytesNumber + 1);
    APP_ERROR_CHECK(err_code);
#endif
    for(index = 0; index < bytesNumber; index++)
    {
        pBuffer[index] = buffer[index + 1];
    }
}

/***************************************************************************//**
 * @brief Resets the device via SPI communication bus.
 *
 * @return None.
*******************************************************************************/
void ADXL362_SoftwareReset(void)
{
    ADXL362_SetRegisterValue(ADXL362_RESET_KEY, ADXL362_REG_SOFT_RESET, 1);
}

/***************************************************************************//**
 * @brief Places the device into standby/measure mode.
 *
 * @param pwrMode - Power mode.
 *                  Example: 0 - standby mode.
 *                  1 - measure mode.
 *
 * @return None.
*******************************************************************************/
void ADXL362_SetPowerMode(unsigned char pwrMode)
{
    unsigned char oldPowerCtl = 0;
    unsigned char newPowerCtl = 0;

    ADXL362_GetRegisterValue(&oldPowerCtl, ADXL362_REG_POWER_CTL, 1);
    newPowerCtl = oldPowerCtl & ~ADXL362_POWER_CTL_MEASURE(0x3);
    newPowerCtl = newPowerCtl |
                  (pwrMode * ADXL362_POWER_CTL_MEASURE(ADXL362_MEASURE_ON));
    ADXL362_SetRegisterValue(newPowerCtl, ADXL362_REG_POWER_CTL, 1);
}

/***************************************************************************//**
 * @brief Selects the measurement range.
 *
 * @param gRange - Range option.
 *                  Example: ADXL362_RANGE_2G  -  +-2 g
 *                           ADXL362_RANGE_4G  -  +-4 g
 *                           ADXL362_RANGE_8G  -  +-8 g
 *                           
 * @return None.
*******************************************************************************/
void ADXL362_SetRange(unsigned char gRange)
{
    unsigned char oldFilterCtl = 0;
    unsigned char newFilterCtl = 0;

    ADXL362_GetRegisterValue(&oldFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    newFilterCtl = oldFilterCtl & ~ADXL362_FILTER_CTL_RANGE(0x3);
    newFilterCtl = newFilterCtl | ADXL362_FILTER_CTL_RANGE(gRange);
    ADXL362_SetRegisterValue(newFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    selectedRange = (1 << gRange) * 2;
}

/***************************************************************************//**
 * @brief Selects the Output Data Rate of the device.
 *
 * @param outRate - Output Data Rate option.
 *                  Example: ADXL362_ODR_12_5_HZ  -  12.5Hz
 *                           ADXL362_ODR_25_HZ    -  25Hz
 *                           ADXL362_ODR_50_HZ    -  50Hz
 *                           ADXL362_ODR_100_HZ   -  100Hz
 *                           ADXL362_ODR_200_HZ   -  200Hz
 *                           ADXL362_ODR_400_HZ   -  400Hz
 *
 * @return None.
*******************************************************************************/
void ADXL362_SetOutputRate(unsigned char outRate)
{
    unsigned char oldFilterCtl = 0;
    unsigned char newFilterCtl = 0;

    ADXL362_GetRegisterValue(&oldFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    newFilterCtl = oldFilterCtl & ~ADXL362_FILTER_CTL_ODR(0x7);
    newFilterCtl = newFilterCtl | ADXL362_FILTER_CTL_ODR(outRate);
    ADXL362_SetRegisterValue(newFilterCtl, ADXL362_REG_FILTER_CTL, 1);
}

/***************************************************************************//**
 * @brief Reads the 3-axis raw data from the accelerometer.
 *
 * @param x - Stores the X-axis data(as two's complement).
 * @param y - Stores the Y-axis data(as two's complement).
 * @param z - Stores the Z-axis data(as two's complement).
 *
 * @return None.
*******************************************************************************/
void ADXL362_GetXyz(short* x, short* y, short* z)
{
    unsigned char xyzValues[6] = {0, 0, 0, 0, 0, 0};

    ADXL362_GetRegisterValue(xyzValues, ADXL362_REG_XDATA_L, 6);
    *x = ((short)xyzValues[1] << 8) + xyzValues[0];
    *y = ((short)xyzValues[3] << 8) + xyzValues[2];
    *z = ((short)xyzValues[5] << 8) + xyzValues[4];
}

/***************************************************************************//**
 * @brief Reads the 3-axis raw data from the accelerometer and converts it to g.
 *
 * @param x - Stores the X-axis data.
 * @param y - Stores the Y-axis data.
 * @param z - Stores the Z-axis data.
 *
 * @return None.
*******************************************************************************/
void ADXL362_GetGxyz(float* x, float* y, float* z)
{
    unsigned char xyzValues[6] = {0, 0, 0, 0, 0, 0};

    ADXL362_GetRegisterValue(xyzValues, ADXL362_REG_XDATA_L, 6);
    *x = ((short)xyzValues[1] << 8) + xyzValues[0];
    *x /= (1000 / (selectedRange / 2));
    *y = ((short)xyzValues[3] << 8) + xyzValues[2];
    *y /= (1000 / (selectedRange / 2));
    *z = ((short)xyzValues[5] << 8) + xyzValues[4];
    *z /= (1000 / (selectedRange / 2));
}

/***************************************************************************//**
 * @brief Reads the temperature of the device.
 *
 * @return tempCelsius - The value of the temperature(degrees Celsius).
*******************************************************************************/
float ADXL362_ReadTemperature(void)
{
    unsigned char rawTempData[2] = {0, 0};
    short         signedTemp     = 0;
    float         tempCelsius    = 0;

    ADXL362_GetRegisterValue(rawTempData, ADXL362_REG_TEMP_L, 2);
    signedTemp = (short)(rawTempData[1] << 8) + rawTempData[0];
    tempCelsius = (float)signedTemp * 0.065;
    
    return tempCelsius;
}

/***************************************************************************//**
 * @brief Configures the FIFO feature.
 *
 * @param mode         - Mode selection.
 *                       Example: ADXL362_FIFO_DISABLE      -  FIFO is disabled.
 *                                ADXL362_FIFO_OLDEST_SAVED -  Oldest saved mode.
 *                                ADXL362_FIFO_STREAM       -  Stream mode.
 *                                ADXL362_FIFO_TRIGGERED    -  Triggered mode.
 * @param waterMarkLvl - Specifies the number of samples to store in the FIFO.
 * @param enTempRead   - Store Temperature Data to FIFO.
 *                       Example: 1 - temperature data is stored in the FIFO
 *                                    together with x-, y- and x-axis data.
 *                                0 - temperature data is skipped.
 *
 * @return None.
*******************************************************************************/
void ADXL362_FifoSetup(unsigned char  mode,
                       unsigned short waterMarkLvl,
                       unsigned char  enTempRead)
{
    unsigned char writeVal = 0;

    writeVal = ADXL362_FIFO_CTL_FIFO_MODE(mode) |
               (enTempRead * ADXL362_FIFO_CTL_FIFO_TEMP) |
               ADXL362_FIFO_CTL_AH;
    ADXL362_SetRegisterValue(writeVal, ADXL362_REG_FIFO_CTL, 1);
    ADXL362_SetRegisterValue(waterMarkLvl, ADXL362_REG_FIFO_SAMPLES, 2);
}

/***************************************************************************//**
 * @brief Configures activity detection.
 *
 * @param refOrAbs  - Referenced/Absolute Activity Select.
 *                    Example: 0 - absolute mode.
 *                             1 - referenced mode.
 * @param threshold - 11-bit unsigned value that the adxl362 samples are
 *                    compared to.
 * @param time      - 8-bit value written to the activity timer register. The 
 *                    amount of time (in seconds) is: time / ODR, where ODR - is 
 *                    the output data rate.
 *
 * @return None.
*******************************************************************************/
void ADXL362_SetupActivityDetection(unsigned char  refOrAbs,
                                    unsigned short threshold,
                                    unsigned char  time)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;

    /* Configure motion threshold and activity timer. */
    ADXL362_SetRegisterValue((threshold & 0x7FF), ADXL362_REG_THRESH_ACT_L, 2);
    ADXL362_SetRegisterValue(time, ADXL362_REG_TIME_ACT, 1);
    /* Enable activity interrupt and select a referenced or absolute
       configuration. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_ACT_REF;
    newActInactReg |= ADXL362_ACT_INACT_CTL_ACT_EN |
                     (refOrAbs * ADXL362_ACT_INACT_CTL_ACT_REF);
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}

/***************************************************************************//**
 * @brief Configures inactivity detection.
 *
 * @param refOrAbs  - Referenced/Absolute Inactivity Select.
 *                    Example: 0 - absolute mode.
 *                             1 - referenced mode.
 * @param threshold - 11-bit unsigned value that the adxl362 samples are
 *                    compared to.
 * @param time      - 16-bit value written to the inactivity timer register. The 
 *                    amount of time (in seconds) is: time / ODR, where ODR - is  
 *                    the output data rate.
 *
 * @return None.
*******************************************************************************/
void ADXL362_SetupInactivityDetection(unsigned char  refOrAbs,
                                      unsigned short threshold,
                                      unsigned short time)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;
    
    /* Configure motion threshold and inactivity timer. */
    ADXL362_SetRegisterValue((threshold & 0x7FF),
                              ADXL362_REG_THRESH_INACT_L,
                              2);
    ADXL362_SetRegisterValue(time, ADXL362_REG_TIME_INACT_L, 2);
    /* Enable inactivity interrupt and select a referenced or absolute
       configuration. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_INACT_REF;
    newActInactReg |= ADXL362_ACT_INACT_CTL_INACT_EN |
                     (refOrAbs * ADXL362_ACT_INACT_CTL_INACT_REF);
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}
