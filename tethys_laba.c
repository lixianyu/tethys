/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "nrf_soc.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "config.h"
#include "tethys_laba.h"
#include "tethys_led.h"

static uint8_t gLabaStream[32];
static uint8_t gLabaIdx = 0;
static uint8_t gLabaIsPlaying = 0;
//static uint8_t gLabaLen = 0;
APP_TIMER_DEF(m_laba_timer_id);

extern void SC50X0_SDA(uint8_t data);
extern void SC50X0_SDA1(uint8_t data);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/


/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void testLaBa(void)
{
    static uint8_t sound = 0xE0;
    static uint8_t cmd = 0x01;
    //LEDS_ON(BSP_LED_1_MASK);
    SC50X0_SDA(0xFC); // Open amplifier
    //SC50X0_SDA(sound++);
    SC50X0_SDA(cmd++);
    //SC50X0_SDA(0xFD); // Close amplifier
    //LEDS_OFF(BSP_LED_1_MASK);
    if (cmd > LABA_PUTONG_YONGHU_BIANHAO_FANWEI_SHI)
    {
        cmd = 0x01;
    }
    if (sound > 0xEF)
    {
        sound = 0xE0;
    }
}
#if 0
void playLaBa(uint8_t adrr)
{
    //if (gLabaIsPlaying == 1) return;
    gLabaIsPlaying = 1;
    close_LEDS();
    ttp229_disable();
    SC50X0_SDA(0xFC); // Open amplifier
    SC50X0_SDA(adrr);
    ttp229_enable();
    open_LEDs();
    gLabaIsPlaying = 0;
}
void playLaBaMany(uint8_t *addrs, uint8_t len)
{
    //if (gLabaIsPlaying == 1) return;
    gLabaIsPlaying = 1;
    close_LEDS();
    ttp229_disable();
    gLabaIdx = 0;
    memset(gLabaStream, 0, sizeof(gLabaStream));
    memcpy(gLabaStream, addrs, len * 2);

    app_timer_start(m_laba_timer_id, APP_TIMER_TICKS(10, APP_TIMER_PRESCALER), NULL);
}

void playLaBaManyContinue(void)
{
    close_LEDS();
    if (gLabaStream[gLabaIdx] == 0 || gLabaIdx >= 32)
    {
        gLabaIsPlaying = 0;
        open_LEDs();
        ttp229_enable();
        gLabaIdx = 0;
        return;
    }
    //ttp229_disable();
    uint16_t delayMS = gLabaStream[gLabaIdx + 1] * 10; // ms
    SC50X0_SDA(gLabaStream[gLabaIdx]);
    gLabaIdx += 2;
    app_timer_start(m_laba_timer_id, APP_TIMER_TICKS(delayMS, APP_TIMER_PRESCALER), NULL);
}
#elif 1
void playLaBa(uint8_t adrr)
{
    //sd_mutex_new
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    //SC50X0_SDA(0xFC); // Open amplifier
    SC50X0_SDA(adrr);
    if (adrr == LABA_ERROR)
    {
        nrf_delay_ms(1000);
    }
    sd_nvic_critical_region_exit(is_nested_critical_region);
}

void playLaBaMany(uint8_t *addrs, uint8_t len)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    close_LEDS();
    gLabaIdx = 0;
    memset(gLabaStream, 0, sizeof(gLabaStream));
    memcpy(gLabaStream, addrs, len * 2);

    app_timer_start(m_laba_timer_id, APP_TIMER_TICKS(10, APP_TIMER_PRESCALER), NULL);
    sd_nvic_critical_region_exit(is_nested_critical_region);
}

void playLaBaManyContinue(void)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    close_LEDS();
    if (gLabaStream[gLabaIdx] == 0 || gLabaIdx >= 32)
    {
        gLabaIsPlaying = 0;
        open_LEDs();
        gLabaIdx = 0;
        sd_nvic_critical_region_exit(is_nested_critical_region);
        return;
    }
    uint16_t delayMS = gLabaStream[gLabaIdx + 1] * 10; // ms
    SC50X0_SDA(gLabaStream[gLabaIdx]);
    gLabaIdx += 2;
    app_timer_start(m_laba_timer_id, APP_TIMER_TICKS(delayMS, APP_TIMER_PRESCALER), NULL);
    sd_nvic_critical_region_exit(is_nested_critical_region);
}

void stopLaBaMany(void)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    app_timer_stop(m_laba_timer_id);
    sd_nvic_critical_region_exit(is_nested_critical_region);
}
#else
void playLaBa(uint8_t adrr)
{
    //sd_mutex_new
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    //close_LEDS();
    //SC50X0_SDA(0xFC); // Open amplifier
    SC50X0_SDA(adrr);
    if (adrr == LABA_ERROR)
    {
        nrf_delay_ms(1000);
    }
    //open_LEDs();
    sd_nvic_critical_region_exit(is_nested_critical_region);
}

void playLaBaMany(uint8_t *addrs, uint8_t len)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    close_LEDS();
    gLabaIdx = 0;
    memset(gLabaStream, 0, sizeof(gLabaStream));
    memcpy(gLabaStream, addrs, len * 2);

    while (1)
    {
        if (gLabaStream[gLabaIdx] == 0 || gLabaIdx >= 32)
        {
            gLabaIsPlaying = 0;
            open_LEDs();
            gLabaIdx = 0;
            break;
        }
        uint16_t delayMS = gLabaStream[gLabaIdx + 1] * 10; // ms
        SC50X0_SDA(gLabaStream[gLabaIdx]);
        gLabaIdx += 2;
        nrf_delay_ms(delayMS);
    }
    sd_nvic_critical_region_exit(is_nested_critical_region);
}

void playLaBaManyContinue(void)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    //close_LEDS();
    if (gLabaStream[gLabaIdx] == 0 || gLabaIdx >= 32)
    {
        gLabaIsPlaying = 0;
        //open_LEDs();
        gLabaIdx = 0;
        sd_nvic_critical_region_exit(is_nested_critical_region);
        return;
    }
    uint16_t delayMS = gLabaStream[gLabaIdx + 1] * 10; // ms
    SC50X0_SDA(gLabaStream[gLabaIdx]);
    gLabaIdx += 2;
    app_timer_start(m_laba_timer_id, APP_TIMER_TICKS(delayMS, APP_TIMER_PRESCALER), NULL);
    sd_nvic_critical_region_exit(is_nested_critical_region);
}
#endif


//return 1, is playing .
int8_t isLabaPlaying(void)
{
    return gLabaIsPlaying;
}

void closeAmplifier(void)
{
    SC50X0_SDA(0xFD); // Close amplifier
}

void Laba_busy_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //LEDS_ON(BSP_LED_4_MASK);
}

void Laba_busy_pin_handler_toggle(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //LEDS_ON(BSP_LED_4_MASK);
    if (nrf_drv_gpiote_in_is_set(pin))
    {
        //LEDS_OFF(BSP_LED_4_MASK);
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)closeAmplifier);
    }
    else
    {
        //LEDS_ON(BSP_LED_4_MASK);
    }
}

static void Laba_timeout_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)playLaBaManyContinue);
}

void init_LaBa(void)
{
    nrf_gpio_cfg_output(LABA_SDA);
    nrf_gpio_pin_set(LABA_SDA);
#if 0
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    //in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    err_code = nrf_drv_gpiote_in_init(LABA_BUSY, &in_config, Laba_busy_pin_handler_toggle);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(LABA_BUSY, true);
#endif
    app_timer_create(&m_laba_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     Laba_timeout_handler);
}

#define LABA_LOW_TIME  499
#define LABA_HIGH_TIME 1497
/*
 * SDA发送2ms-8ms低电平再发送高低电平3：1或1：3信号且数据低位先送。
 * 时序范围为200us：600us到500us：1500us。
 */
void SC50X0_SDA(uint8_t data)
{
    //LEDS_ON(BSP_LED_2_MASK);
    uint8_t i;
    //LABA_SDA = 0;
    nrf_gpio_pin_clear(LABA_SDA);
    nrf_delay_ms(3);
    for(i = 0; i < 8; i++)
    {
        //LABA_SDA = 1;
        nrf_gpio_pin_set(LABA_SDA);
        if(data & 0x01)
        {
            nrf_delay_us(LABA_HIGH_TIME);
            //LABA_SDA = 0;
            nrf_gpio_pin_clear(LABA_SDA);
            nrf_delay_us(LABA_LOW_TIME);
        }
        else
        {
            nrf_delay_us(LABA_LOW_TIME);
            //LABA_SDA = 0;
            nrf_gpio_pin_clear(LABA_SDA);
            nrf_delay_us(LABA_HIGH_TIME);
        }
        data >>= 1;
    }
    //LABA_SDA = 1;
    nrf_gpio_pin_set(LABA_SDA);
//    nrf_delay_us(200);
    //LEDS_OFF(BSP_LED_2_MASK);
}

#if 0
void SC50X0_SDA1(uint8_t data)
{
    //LEDS_ON(BSP_LED_2_MASK);
    uint8_t i;
    //LABA_SDA = 0;
    nrf_gpio_pin_clear(LABA_SDA);
    nrf_delay_ms(7);
    for(i = 0; i < 8; i++)
    {
        //LABA_SDA = 1;
        nrf_gpio_pin_set(LABA_SDA);
        if(data & 0x01)
        {
            nrf_delay_us(600);
            //LABA_SDA = 0;
            nrf_gpio_pin_clear(LABA_SDA);
            nrf_delay_us(200);
        }
        else
        {
            nrf_delay_us(200);
            //LABA_SDA = 0;
            nrf_gpio_pin_clear(LABA_SDA);
            nrf_delay_us(600);
        }
        data >>= 1;
    }
    //LABA_SDA = 1;
    nrf_gpio_pin_set(LABA_SDA);
    nrf_delay_us(200);
    //LEDS_OFF(BSP_LED_2_MASK);
}
#endif
