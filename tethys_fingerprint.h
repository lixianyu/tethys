#ifndef __TETHYS_FINGERPRINT_H__
#define __TETHYS_FINGERPRINT_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>
#include "tethys_storage.h"

#define FINGERPRINT_INT_DELAY    500
#define FP_CAN_SLEEP_TIME        3500
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_Fingerprint(void);
extern void testFingerprint(void);
extern void start_fingerprint(void);
extern void sleep_fingerprint(void);
extern void _FG_REQ_ERASE_ONE(uint16_t id, delete_one_fingerprint_cb callback);
extern void _FG_REQ_ERASE_ALL(void);
#endif
