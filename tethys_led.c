/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <math.h>
#include "tethys_led.h"
#include "tethys_laba.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "app_pwm.h"
#include "led_softblink.h"
#include "mimas_run_in_ram.h"
#include "mimas_bsp.h"

typedef enum
{
    LED_DH_NOTHING,
    LED_DH_0,
    LED_DH_1,
    LED_DH_delete,
    LED_DH_Open_Niubi,
    LED_DH_Close_Niubi,
    LED_DH_alert,
    LED_DH_factory_reset,
    LED_DH_return_to_idle,
    LED_DH_blink,
    LED_DH_blink_hang,
    LED_DH_blink_lie,
    LED_DH_blink_TWO_lie,
    LED_DH_BREATH,
} LED_DH_t;

APP_TIMER_DEF(m_led_timer_id);
extern uint8_t m_beacon_info[];
static void led_timeout_handler(void *p_context);
static void resolve_donghua_1(void);
static void resolve_delete_DH(void);
static void resolve_DH_open_NiuBi(void);
static void resolve_DH_close_NiuBi(void);
static void resolve_DH_alert(void);
static void resolve_blink(void);
static void resolve_blink_hang(void);
static void resolve_blink_lie(void);
static void resolve_blink_two_lie(void);
static void resolve_factory_reset_DH(void);
static void resolve_return_to_idle_DH(void);
#ifdef TETHYS_PWM
static void resolve_pwm_1(void);
#endif
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
static LED_DH_t mTethysLEDstate = LED_DH_NOTHING;
static uint8_t mTethysDH = 0;
static uint8_t mTethysLEDcount = 0;
static uint8_t mTethysLEDcount2 = 0;
static uint8_t mTethysLEDHadOpened = 0;
static uint8_t gLedNum;
static uint8_t gLedNum2;
static bool mStop = false;
static LED_DH_callback gLedCallback = NULL;
#ifdef TETHYS_PWM
static bool gPWMstop = false;
static uint32_t millis = 5;
#endif
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void init_LED(void)
{
    mTethysLEDstate = LED_DH_0;
    mTethysLEDcount = 0;
    app_timer_create(&m_led_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     led_timeout_handler);
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(99, 0), NULL);
}

//At the same time , we close the other leds.
void open_LED(uint8_t led)
{
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);
    switch (led)
    {
        case 1:
            LEDS_ON(BSP_LED_COL_1_MASK);
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 2:
            LEDS_ON(BSP_LED_COL_1_MASK);
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 3:
            LEDS_ON(BSP_LED_COL_1_MASK);
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;
        case 4:
            LEDS_ON(BSP_LED_COL_2_MASK);
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 5:
            LEDS_ON(BSP_LED_COL_2_MASK);
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 6:
            LEDS_ON(BSP_LED_COL_2_MASK);
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;
        case 7:
            LEDS_ON(BSP_LED_COL_3_MASK);
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 8:
            LEDS_ON(BSP_LED_COL_3_MASK);
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 9:
            LEDS_ON(BSP_LED_COL_3_MASK);
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;

        case 0x2A:
            LEDS_ON(BSP_LED_COL_4_MASK);
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 0:
            LEDS_ON(BSP_LED_COL_4_MASK);
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 0x23:
            LEDS_ON(BSP_LED_COL_4_MASK);
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;
        default:
            //shit here...
            break;
    }
}

void open_LED_ROL(uint8_t rol)
{
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);
    switch (rol)
    {
        case 1:
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 2:
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 3:
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;
    }
    LEDS_ON(LEDS_COL_MASK);
}

void open_LED_COL(uint8_t col)
{
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);

    LEDS_OFF(LEDS_ROL_MASK);
    switch (col)
    {
        case 1:
            LEDS_ON(BSP_LED_COL_1_MASK);
            break;
        case 2:
            LEDS_ON(BSP_LED_COL_2_MASK);
            break;
        case 3:
            LEDS_ON(BSP_LED_COL_3_MASK);
            break;
        case 4:
            LEDS_ON(BSP_LED_COL_4_MASK);
            break;
    }
}

void open_LEDs(void)
{
    #if 1
    if (mTethysLEDHadOpened == 0)
    {
        LEDS_ON(LEDS_COL_MASK);
        LEDS_OFF(LEDS_ROL_MASK);
        mTethysLEDHadOpened = 1;
    }
    #else
    nrf_gpio_cfg_output(LED_RO_1);
    nrf_gpio_cfg_output(LED_RO_2);
    nrf_gpio_cfg_output(LED_RO_3);
    nrf_gpio_cfg_output(LED_CO_1);
    nrf_gpio_cfg_output(LED_CO_2);
    nrf_gpio_cfg_output(LED_CO_3);
    nrf_gpio_cfg_output(LED_CO_4);
    
    nrf_gpio_pin_clear(LED_RO_1);
    nrf_gpio_pin_clear(LED_RO_2);
    nrf_gpio_pin_clear(LED_RO_3);

    nrf_gpio_pin_set(LED_CO_1);
    nrf_gpio_pin_set(LED_CO_2);
    nrf_gpio_pin_set(LED_CO_3);
    nrf_gpio_pin_set(LED_CO_4);
    #endif
}

void close_LEDS(void)
{
    mTethysLEDHadOpened = 0;
    LEDS_OFF(LEDS_COL_MASK);
    LEDS_ON(LEDS_ROL_MASK);
}


#define TETHYS_LED_0_ON_INTERVAL       10
#define TETHYS_LED_0_OFF_INTERVAL      1100
static void tethys_led_indication(LED_DH_t state)
{
    uint32_t next_delay = 0;
    switch (state)
    {
        case LED_DH_0:
            LEDS_OFF(LEDS_ROL_MASK);

            if (LED_IS_ON(LEDS_COL_MASK))
            {
                LEDS_OFF(LEDS_COL_MASK);
                next_delay = TETHYS_LED_0_OFF_INTERVAL;
            }
            else
            {
                mTethysLEDcount++;
                LEDS_ON(LEDS_COL_MASK);
                next_delay = TETHYS_LED_0_ON_INTERVAL;
            }
            mTethysLEDstate = state;
            if (mTethysLEDcount > 2)
            {
                mTethysLEDcount = 0;
                //bsp_indication_set(BSP_INDICATE_IDLE);
                close_LEDS();
                //open_LEDs();
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(next_delay, 0), NULL);
            break;
            
        case LED_DH_1:
            resolve_donghua_1();
            break;
        case LED_DH_delete:
            resolve_delete_DH();
            break;
        case LED_DH_Open_Niubi:
            resolve_DH_open_NiuBi();
            break;
        case LED_DH_Close_Niubi:
            resolve_DH_close_NiuBi();
            break;
        case LED_DH_alert:
            resolve_DH_alert();
            break;
        case LED_DH_factory_reset:
            resolve_factory_reset_DH();
            break;
        case LED_DH_return_to_idle:
            resolve_return_to_idle_DH();
            break;
        case LED_DH_blink:
            resolve_blink();
            break;
        case LED_DH_blink_hang:
            resolve_blink_hang();
            break;
        case LED_DH_blink_lie:
            resolve_blink_lie();
            break;
        case LED_DH_blink_TWO_lie:
            resolve_blink_two_lie();
            break;
#ifdef TETHYS_PWM
        case LED_DH_BREATH:
            if (!gPWMstop)
            {
                resolve_pwm_1();
            }
            break;
#endif
        default:
            break;
    }
}

static void led_timeout_handler(void *p_context)
{
    tethys_led_indication(mTethysLEDstate);
}

static void resolve_donghua_1(void)
{
#define DELAY_TIME 50
    switch (mTethysDH)
    {
        case 0:
            LEDS_OFF(LEDS_ROL_MASK);
            nrf_gpio_pin_set(TETHYS_LED_COL_1);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_2);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_3);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            nrf_gpio_pin_clear(TETHYS_LED_COL_1);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_2);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_3);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
            mTethysDH = 2;
            break;
        case 2:
            nrf_gpio_pin_set(TETHYS_LED_COL_1);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_2);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_3);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_set(TETHYS_LED_COL_4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
            mTethysDH = 3;
            break;
        case 3:
            nrf_gpio_pin_clear(TETHYS_LED_COL_1);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_2);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_3);
            nrf_delay_ms(DELAY_TIME);
            nrf_gpio_pin_clear(TETHYS_LED_COL_4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
            mTethysDH = 4;
            break;
        case 4:
            mTethysLEDcount++;
            if (mTethysLEDcount > 1)
            {
                LEDS_ON(LEDS_ROL_MASK);
                LEDS_OFF(LEDS_COL_MASK);
                if (gLedCallback != NULL)
                {
                    gLedCallback();
                }
            }
            else
            {
                mTethysDH = 0;
                app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
            }
            break;
        default:
            break;
    }
#undef DELAY_TIME
}

void led_donghua_1(LED_DH_callback callback)
{
    gLedCallback = callback;
    mTethysLEDstate = LED_DH_1;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
}
/////////////////////////////////////////////////////////////////////////////////////
void led_donghua_open_NiuBi(void)
{
    close_LEDS();
    mTethysLEDstate = LED_DH_Open_Niubi;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
}

static void resolve_DH_open_NiuBi(void)
{
    switch (mTethysDH)
    {
        case 0:
            LEDS_OFF(LEDS_ROL_MASK);
            nrf_gpio_pin_set(TETHYS_LED_COL_1);
            nrf_gpio_pin_set(TETHYS_LED_COL_2);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(300, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            nrf_gpio_pin_clear(TETHYS_LED_COL_1);
            nrf_gpio_pin_clear(TETHYS_LED_COL_2);
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 4)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(300, 0), NULL);
            break;
        default:
            break;
    }
}

void led_donghua_close_NiuBi(void)
{
    close_LEDS();
    mTethysLEDstate = LED_DH_Close_Niubi;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
}

static void resolve_DH_close_NiuBi(void)
{
    switch (mTethysDH)
    {
        case 0:
            LEDS_OFF(LEDS_ROL_MASK);
            nrf_gpio_pin_set(TETHYS_LED_COL_3);
            nrf_gpio_pin_set(TETHYS_LED_COL_4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(300, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            nrf_gpio_pin_clear(TETHYS_LED_COL_4);
            nrf_gpio_pin_clear(TETHYS_LED_COL_3);
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 4)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(300, 0), NULL);
            break;
        default:
            break;
    }
}
/////////////////////////////////////////////////////////////////////////////////////
void led_donghua_alert(void)
{
    close_LEDS();
    mTethysLEDstate = LED_DH_alert;
    mTethysLEDcount = 0;
    mTethysLEDcount2 = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
}

static void resolve_DH_alert(void)
{
    #define DELAY_TIME 5
    #define STOPTIME 10000
    #define DELAY_TIME_OFF 500
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            close_LEDS();
            nrf_gpio_pin_set(TETHYS_LED_COL_1);
            nrf_gpio_pin_clear(TETHYS_LED_ROL_2);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LEDS();
            nrf_gpio_pin_set(TETHYS_LED_COL_2);
            LEDS_OFF(LEDS_ROL_MASK);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME, 0), NULL);
            mTethysDH = 2;
            break;
        case 2:
        {
            mTethysDH = 0;
            close_LEDS();
            nrf_gpio_pin_set(TETHYS_LED_COL_3);
            nrf_gpio_pin_clear(TETHYS_LED_ROL_2);
            mTethysLEDcount++;
            uint16_t onTimeCnt = DELAY_TIME_ON / (DELAY_TIME*3);
            if (mTethysLEDcount > onTimeCnt)
            {
                close_LEDS();
                mTethysLEDcount2++;
                uint16_t stopCnt = STOPTIME / DELAY_TIME_OFF;//��׼��......
                if (mTethysLEDcount2 > stopCnt)
                {
                    break;
                }
                mTethysLEDcount = 0;
                app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME, 0), NULL);
        }
        break;
        default:
            break;
    }
    #undef DELAY_TIME
    #undef STOPTIME
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one LED.
void led_blink(uint8_t lednumber)
{
    close_LEDS();
    gLedNum = lednumber;
    mTethysLEDstate = LED_DH_blink;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink(void)
{
    #define DELAY_TIME_OFF 500
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            open_LED(gLedNum);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LEDS();
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 15)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one hang LED.There 4 hang
void led_blink_hang(uint8_t hang)
{
    close_LEDS();
    gLedNum = hang;
    mTethysLEDstate = LED_DH_blink_hang;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink_hang(void)
{
    #define DELAY_TIME_OFF 500
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            open_LED_COL(gLedNum);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LEDS();
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 15)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one lie LED.There 3 lie
void led_blink_lie(uint8_t lie)
{
    close_LEDS();
    gLedNum = lie;
    mTethysLEDstate = LED_DH_blink_lie;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink_lie(void)
{
    #define DELAY_TIME_OFF 500
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            open_LED_ROL(gLedNum);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LEDS();
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 15)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one lie LED.There 3 lie
void led_blink_two_lie(uint8_t lie1, uint8_t lie2)
{
    close_LEDS();
    gLedNum = lie1;
    gLedNum2 = lie2;
    mTethysLEDstate = LED_DH_blink_TWO_lie;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void open_a_lie(uint8_t rol)
{
    switch (rol)
    {
        case 1:
            LEDS_OFF(BSP_LED_ROL_1_MASK);
            break;
        case 2:
            LEDS_OFF(BSP_LED_ROL_2_MASK);
            break;
        case 3:
            LEDS_OFF(BSP_LED_ROL_3_MASK);
            break;
    }
}

static void resolve_blink_two_lie(void)
{
    #define DELAY_TIME_OFF 700
    #define DELAY_TIME_ON 100
    switch (mTethysDH)
    {
        case 0:
            LEDS_ON(LEDS_COL_MASK);
            open_a_lie(gLedNum);
            open_a_lie(gLedNum2);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LEDS();
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 15)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
static void resolve_delete_DH(void)
{
    open_LED_COL(mTethysDH++);
    if (mTethysDH > 4)
    {
        mTethysDH = 1;
    }
    if (mStop == false)
    {
        app_timer_start(m_led_timer_id, APP_TIMER_TICKS(500, 0), NULL);
    }
}

void led_DH_delete_start(void)
{
    mTethysLEDstate = LED_DH_delete;
    mTethysDH = 1;
    mStop = false;
    LEDS_OFF(LEDS_ROL_MASK);
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
}

void led_DH_delete_stop(void)
{
    mStop = true;
}
/////////////////////////////////////////////////////////////////////////////////////
static void resolve_factory_reset_DH(void)
{
    open_LED_ROL(mTethysDH++);
    if (mTethysDH > 3)
    {
        mTethysDH = 1;
    }
    mTethysLEDcount++;
    if (mTethysLEDcount > 10)
    {
        sleep_fingerprint();
        close_MFRC522();
        close_LEDS();
        return;
    }
    if (mStop == false)
    {
        app_timer_start(m_led_timer_id, APP_TIMER_TICKS(450, 0), NULL);
    }
}

void led_DH_factory_reset_start(void)
{
    mTethysLEDstate = LED_DH_factory_reset;
    mTethysLEDcount = 0;
    mTethysDH = 1;
    mStop = false;
    close_LEDS();
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(98, 0), NULL);
}
/////////////////////////////////////////////////////////////////////////////////////
static void resolve_return_to_idle_DH(void)
{
    open_LED_COL(mTethysDH++);
    if (mTethysDH > 4)
    {
        mTethysLEDcount++;
        if (mTethysLEDcount > 1)
        {
            close_LEDS();
            return;
        }
        mTethysDH = 1;
    }
    if (mStop == false)
    {
        app_timer_start(m_led_timer_id, APP_TIMER_TICKS(110, 0), NULL);
    }
}

void led_DH_return_to_idle(void)
{
    mTethysLEDstate = LED_DH_return_to_idle;
    mTethysLEDcount = 0;
    mTethysDH = 1;
    mStop = false;
    close_LEDS();
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(90, 0), NULL);
}
//////////////////////////////////////////////////////////////////////////////////////
#ifdef TETHYS_PWM
void led_pwm_init(void)
{
    ret_code_t           err_code;
    
    led_sb_init_params_t led_sb_init_params = LED_SB_INIT_DEFAULT_PARAMS(BSP_LED_ROL_2_MASK);
    led_sb_init_params.active_high = false;
    led_sb_init_params.duty_cycle_min = 0;
    led_sb_init_params.duty_cycle_max = 250;
    led_sb_init_params.duty_cycle_step = 2;
    
    err_code = led_softblink_init(&led_sb_init_params);

    APP_ERROR_CHECK(err_code);
}

void led_pwm_start(void)
{
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);

    LEDS_ON(LEDS_COL_MASK);
    led_softblink_start(BSP_LED_ROL_2_MASK);
}

void led_pwm_stop(void)
{
    led_softblink_stop();
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);
}

//////////////////////////////////////////////////////////////////////////////////////
APP_PWM_INSTANCE(PWM1, 1);
static void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    //ready_flag = true;
}

static int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void led_pwm_init_1(void)
{
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(256L, LED_6);
    
    /* Switch the polarity of the second channel. */
    //pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_LOW;
    
    /* Initialize PWM. */
    uint32_t err_code = app_pwm_init(&PWM1, &pwm1_cfg, pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    //app_pwm_disable(&PWM1);
}

void led_pwm_start_1(void)
{
    mTethysLEDstate = LED_DH_BREATH;
    millis = 100;
    gPWMstop = false;
    
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);

    LEDS_ON(LEDS_COL_MASK);
    app_pwm_enable(&PWM1);
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
    
    //app_pwm_channel_duty_set(&PWM1, 0, 1);
    //while (app_pwm_channel_duty_set(&PWM1, 0, 1) == NRF_ERROR_BUSY);
}

void led_pwm_stop_1(void)
{
    app_pwm_disable(&PWM1);
    gPWMstop = true;
    mTethysLEDstate = LED_DH_NOTHING;
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);
}

static void resolve_pwm_1(void)
{
    double val = (exp(sin((double)millis/3500.0*2.5)) - 0.36787944)*100;
    //app_pwm_duty_t duty = map(val, 0, 255, 0, 2040);
    app_pwm_duty_t duty = val;
//    app_pwm_channel_duty_set(&PWM1, 0, duty);
    while (app_pwm_channel_duty_set(&PWM1, 0, duty) == NRF_ERROR_BUSY);
    millis += 4;
    //millis++;
    if (millis > 300000)
    {
        led_pwm_stop_1();
        return;
    }
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(1, 0), NULL);
}
#endif