#ifndef MIMAS_CONFIG_H__
#define MIMAS_CONFIG_H__

#define BLOCK_SIZE 32
#define BLOCK_COUNT 31

#define BLOCK_NUM_PROFILE 0
#define BLOCK_NUM_STEP_TARGET 1
#define BLOCK_NUM_OS 2
#define BLOCK_NUM_SN 3
#define BLOCK_NUM_TEST_FLAG 4
#define BLOCK_NUM_USER_ID 5

#define BLOCK_NUM_MAJOR 6
#define BLOCK_NUM_MINOR 7
#define BLOCK_NUM_Calibrated_Tx_Power 8
#define BLOCK_NUM_DEVICE_TX_POWER 9
#define BLOCK_NUM_ADV_Freq 10 //Unit: 100ms, 1~102.
#define BLOCK_NUM_UUID 11

///////////////////////////////////////
#define PSTORAGE_STATE_UPDATE_IDLE 0
#define PSTORAGE_STATE_UPDATE_UUID 1
#define PSTORAGE_STATE_UPDATE_MAJOR 2
#define PSTORAGE_STATE_UPDATE_MINOR 3 
#define PSTORAGE_STATE_UPDATE_CALIBRATED_TX_POWER 4 
#define PSTORAGE_STATE_UPDATE_ADV_INTERVAL 5
#define PSTORAGE_STATE_UPDATE_TX_POWER 6
///////////////////////////////////////


/* Bit 0 : active or inactive. */
#define ACTIVE_INACTIVE_Pos ((uint8_t)0) /*!< active or inactive field. */
#define ACTIVE_INACTIVE_Msk ((uint8_t)0x1 << ACTIVE_INACTIVE_Pos) /*!< Bit mask of active or inactive. */
#define ACTIVE_INACTIVE_0 ((uint8_t)0) /*!< inactive. */
#define ACTIVE_INACTIVE_1 ((uint8_t)1) /*!< active. */

/* Bits 2..1 :  obfuscating algorithms. */
#define OBFUSCATING_ALGORITHMS_Pos ((uint8_t)1) /*!< Position of obfuscating algorithms field. */
#define OBFUSCATING_ALGORITHMS_Msk ((uint8_t)0x3 << OBFUSCATING_ALGORITHMS_Pos) /*!< Bit mask of obfuscating algorithms field. */
#define OBFUSCATING_ALGORITHMS_0 ((uint8_t)0x00) /*!< No obfuscating algorithms. */
#define OBFUSCATING_ALGORITHMS_1 ((uint8_t)0x01) /*!< obfuscating algorithms 1. */
#define OBFUSCATING_ALGORITHMS_2 ((uint8_t)0x02) /*!< obfuscating algorithms 2. */
#define OBFUSCATING_ALGORITHMS_3 ((uint8_t)0x03) /*!< obfuscating algorithms 3. */

//For Tethys
/* Bit 7 : bind or not bind. */
#define ACTIVE_BIND_Pos ((uint8_t)7) /*!< bind or not bind field. */
#define ACTIVE_BIND_Msk ((uint8_t)0x1 << ACTIVE_BIND_Pos) /*!< Bit mask of bind or not bind. */
#define ACTIVE_BIND_0 ((uint8_t)0) /*!< not bind. */
#define ACTIVE_BIND_1 ((uint8_t)1) /*!< bind. */

/* Bits 6..0 :  battery. */
#define BATTERY_Pos ((uint8_t)0) /*!< Position of battery field. */
#define BATTERY_Msk ((uint8_t)0x7F << BATTERY_Pos) /*!< Bit mask of battery field. */


/////////////////////////////////////////////////////////////////////
#ifdef TETHYS_PAIR
extern uint8_t gPasskey[6];
#endif
#endif  //MIMAS_CONFIG_H__
