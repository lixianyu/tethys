#ifndef __TETHYS_LED_H__
#define __TETHYS_LED_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>

typedef void (*LED_DH_callback)(void);
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_LED(void);
extern void open_LEDs(void);
extern void open_LED(uint8_t led);
extern void open_LED_COL(uint8_t col);
extern void open_LED_ROL(uint8_t rol);
extern void close_LEDS(void);

extern void led_donghua_1(LED_DH_callback callback);
extern void led_DH_delete_start(void);
extern void led_DH_delete_stop(void);
extern void led_donghua_open_NiuBi(void);
extern void led_donghua_close_NiuBi(void);
extern void led_DH_factory_reset_start(void);
extern void led_DH_return_to_idle(void);
extern void led_blink(uint8_t lednumber);
extern void led_blink_lie(uint8_t lie);
extern void led_blink_two_lie(uint8_t lie1, uint8_t lie2);
extern void led_blink_hang(uint8_t hang);

extern void led_pwm_init(void);
extern void led_pwm_start(void);
extern void led_pwm_stop(void);
extern void led_pwm_init_1(void);
extern void led_pwm_start_1(void);
extern void led_pwm_stop_1(void);
#endif
