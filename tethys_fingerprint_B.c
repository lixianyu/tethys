/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_fingerprint.h"
#include "tethys_laba.h"
#include "tethys_led.h"
#include "tethys_ttp229bsf.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"
#include "bd_wall_clock_timer.h"
#include "tethys_moto.h"

typedef enum {
    FG_IDLE = 0,
    FG_BUSY,
}FG_state_t;

//Define cmd code
#define FG_REQ_CAPTURE_AND_EXTRACT 0x51
#define FG_REQ_ENROLL 0x7F
#define FG_REQ_MATCH_1N 0x71
#define FG_REQ_ERASE_ONE 0x73
#define FG_REQ_ERASE_ALL 0x54
#define FG_REQ_GET_USER_NUM 0x55
#define FG_REQ_GET_ID_AVAILABILITY 0x74
#define FG_REQ_CHANGE_BOARD_RATE 0x7E

extern uint8_t m_beacon_info[];
extern bool gYiKaiSuo;
void fingerprint_uart_uninit(void);
extern void ReqCaptureAndExtract(uint8_t param);
extern void ReqMatchFinger(void);
extern void fingerprint_please_input_again(void *data, uint16_t length);
extern void AckAnalyze(uint8_t *pRcvBuf);
void _FG_REQ_MATCH_1N(void);
void fingerprint_uart_init(void);
void fingerprint_luru_OK(void);

#define FINGERPRINT_UART_RX_BUF_SIZE    64
#define FINGERPRINT_UART_TX_BUF_SIZE    64
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
static FG_state_t gFingerprintState = FG_IDLE;
static uint8_t gFingerprintEnrollState = 0;
APP_TIMER_DEF(m_Fingerprint_time_id);
static bool gFPIfCanSleep = true;//只有这个标识为真时，才能给指纹模组断电
APP_TIMER_DEF(m_Fingerprint_Sleep_time_id);

APP_TIMER_DEF(m_Fingerprint_Delay_Sleep_time_id);
APP_TIMER_DEF(m_Fingerprint_uart_time_id);
static uint8_t gFGCmd;
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void testFingerprint(void)
{

}
//#define PIN_IS_ON(pins_mask) ((pins_mask) & (NRF_GPIO->OUT ^ LEDS_INV_MASK) )
__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}

static void Fingerprint_detect_pin_handler_LOWTOHIGH(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    static uint8_t flag = 0;
    if (ttp229_is_correct_state_for_fingerprint() != 0)
    {
        //open_LED(7);
        return;
    }
    if (gYiKaiSuo) //已开锁后，会有5s等待关锁，在这期间不响应指纹中断
    {
        //open_LED(8);
        return;
    }
    if (flag == 0)
    {
        flag = 1;
        open_LED(9);
    }
    else
    {
        open_LED('*');
        flag = 0;
    }
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        gFingerprintState = FG_BUSY;
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
        app_timer_start(m_Fingerprint_time_id, APP_TIMER_TICKS(FINGERPRINT_INT_DELAY, 0), NULL);
    }
    else
    {
        if (gFingerprintState == FG_IDLE)
        {
            gFingerprintState = FG_BUSY;
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqCaptureAndExtract);
        }
        else
        {
            ttp220_return_idle(15000);
        }
    }
}

static void preReqCaptureAndExtract(void)
{
    fingerprint_uart_init();
    //nrf_delay_ms(300);
    ReqCaptureAndExtract(0xFF);
}

static void fingerprint_timer_handler(void *p_context)
{
    #if 0
    fingerprint_uart_init();
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqCaptureAndExtract);
    #else
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)preReqCaptureAndExtract);
    #endif
}

static void fingerprint_sleep_timer_handler(void *p_context)
{
    gFPIfCanSleep = true;
}

static void fingerprint_delay_sleep_timer_handler(void *p_context)
{
    if (gFPIfCanSleep)
    {
        nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);
        gFingerprintState = FG_IDLE;
    }
    else
    {
        app_timer_start(m_Fingerprint_Delay_Sleep_time_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
}

static void fingerprint_uart_timer_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)fingerprint_uart_uninit);
}

static uint8_t index = 0;
static uint8_t data_array[16];
static uint8_t len_uart = 8;
__STATIC_INLINE void reset_uart_array(void)
{
    len_uart = 8;
    index = 0;
    memset(data_array, 0, sizeof(data_array));
}

void fingerprint_uart_handle(app_uart_evt_t *p_event)
{
    uint32_t err_code;

    switch (p_event->evt_type)
    {
    case APP_UART_DATA_READY:
        //UNUSED_VARIABLE(app_uart_get(&data_array[index]));
        app_uart_get(&data_array[index]);
        index++;
        if (index >= len_uart)
        {
#if 0
            memcpy(m_beacon_info + BEACON_UUID_OFFSET, data_array, 7);
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
#endif
            index = 0;
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)AckAnalyze);
        }
        break;

    case APP_UART_COMMUNICATION_ERROR:
        //APP_ERROR_HANDLER(p_event->data.error_communication);
        //playLaBa(LABA_2);
        //fingerprint_uart_init();
        //app_uart_close();
        open_LED(7);
        app_timer_start(m_Fingerprint_uart_time_id, APP_TIMER_TICKS(100, 0), NULL);
        //gFingerprintState = FG_IDLE;
        break;

    case APP_UART_FIFO_ERROR:
        //APP_ERROR_HANDLER(p_event->data.error_code);
        //playLaBa(LABA_3);
        open_LED(8);
        app_timer_start(m_Fingerprint_uart_time_id, APP_TIMER_TICKS(100, 0), NULL);
        break;

    default:
        break;
    }
}

void fingerprint_uart_init(void)
{
    uint32_t err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        FINGERPRINT_RX_PIN,
        FINGERPRINT_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT( &comm_params,
                        FINGERPRINT_UART_RX_BUF_SIZE,
                        FINGERPRINT_UART_TX_BUF_SIZE,
                        fingerprint_uart_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
    //APP_ERROR_CHECK(err_code);
#if 0
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}

void fingerprint_uart_uninit(void)
{
    app_uart_close();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
    app_timer_start(m_Fingerprint_time_id, APP_TIMER_TICKS(100, 0), NULL);
}

#if 0
void init_Fingerprint(void)
{
    nrf_gpio_cfg_output(FINGERPRINT_PW_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_EN_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_PW_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);

    nrf_gpio_cfg_output(FINGERPRINT_DETECT_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_PLUG_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_TX_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_RX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_TX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_RX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_DETECT_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_PLUG_PIN);

    nrf_delay_ms(2000);
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    nrf_gpio_cfg_input(FINGERPRINT_DETECT_PIN, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(FINGERPRINT_PLUG_PIN, NRF_GPIO_PIN_NOPULL);
    nrf_delay_ms(FINGERPRINT_INT_DELAY);
    nrf_gpio_pin_set(FINGERPRINT_PW_PIN);
    fingerprint_uart_init();
#if 1
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    err_code = nrf_drv_gpiote_in_init(FINGERPRINT_DETECT_PIN, &in_config, Fingerprint_detect_pin_handler_LOWTOHIGH);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(FINGERPRINT_DETECT_PIN, true);
#endif

    app_timer_create(&m_lock_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     lock_timeout_handler);
}
#else
void init_Fingerprint(void)
{
    gFingerprintState = FG_IDLE;
#ifdef FP_POWER_ALWAYS_ON
    fingerprint_uart_init();
#endif

    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    //in_config.pull = NRF_GPIO_PIN_PULLUP;
    in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    err_code = nrf_drv_gpiote_in_init(FINGERPRINT_DETECT_PIN, &in_config, Fingerprint_detect_pin_handler_LOWTOHIGH);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(FINGERPRINT_DETECT_PIN, true);

    app_timer_create(&m_Fingerprint_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     fingerprint_timer_handler);
    app_timer_create(&m_Fingerprint_Sleep_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     fingerprint_sleep_timer_handler);
    app_timer_create(&m_Fingerprint_Delay_Sleep_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     fingerprint_delay_sleep_timer_handler);
    app_timer_create(&m_Fingerprint_uart_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     fingerprint_uart_timer_handler);   
#if 0
    memcpy(m_beacon_info + BEACON_UUID_OFFSET, &gTethys, 4);
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    advertising_init_beacon();
#endif
}
#endif

void start_fingerprint(void)
{
#if 0
    NRF_UART0->ENABLE = 1;
    NRF_UART0->TASKS_STARTTX = 1;
    NRF_UART0->TASKS_STARTRX = 1;
#endif
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    nrf_delay_ms(FINGERPRINT_INT_DELAY);
    fingerprint_uart_init();
}

#ifndef FP_POWER_ALWAYS_ON
void sleep_fingerprint(void)
{
    #if 0
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        return;
    }
    #endif
    app_uart_close();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
    
    if (gFPIfCanSleep == true)
    {
        nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);
        gFingerprintState = FG_IDLE;
    }
    else
    {
        app_timer_start(m_Fingerprint_Delay_Sleep_time_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
}
#else
void sleep_fingerprint(void)
{
    gFingerprintState = FG_IDLE;
}
#endif

static void fingerprint_uart_send_data(uint8_t *p_data, uint8_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
}

//FG_REQ_CAPTURE_AND_EXTRACT
void ReqCaptureAndExtract(uint8_t param)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_CAPTURE_AND_EXTRACT;
    gFGCmd = cmd;

    //playLaBa(LABA_JIN_RU_CHU_SHI_HUA);
    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send index (optional)
    if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
        getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1 ||
        getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1 ||
        getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_2_1 ||
        getTTP_state() == TETHYS_SET_BLUETOOTH)
    {
        Sendcmd[4] = gFingerprintEnrollState;
    }
    else
    {
        Sendcmd[4] = 0;
    }
    nCheckSum = (nCheckSum + Sendcmd[4]) & 0xFF;

    Sendcmd[5] = 0;//reserve
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;
    
    // send check-sum
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

void _FG_REQ_ENROLL(void)
{
    uint8_t Sendcmd[12] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_ENROLL;
    gFGCmd = cmd;

    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send param
    Sendcmd[4] = getCurID();
    nCheckSum = (nCheckSum + Sendcmd[4]) & 0xFF;

    // send reserve
    Sendcmd[5] = 0;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;
    
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

void _FG_REQ_MATCH_1N(void)
{
    app_timer_stop(m_Fingerprint_Sleep_time_id);
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_MATCH_1N;
    gFGCmd = cmd;

    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send param
    Sendcmd[4] = 0;
    nCheckSum = (nCheckSum + 0) & 0xFF;

    // send reserve
    Sendcmd[5] = 0;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;

    // send check-sum
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

delete_one_fingerprint_cb gStorageCB;
void _FG_REQ_ERASE_ONE(uint16_t id, delete_one_fingerprint_cb callback)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_ERASE_ONE;
    gFGCmd = cmd;
    gStorageCB = callback;

    gFingerprintState = FG_BUSY;
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    }
    nrf_delay_ms(FINGERPRINT_INT_DELAY);
    fingerprint_uart_init();
    
    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send param
    Sendcmd[4] = id;
    nCheckSum = (nCheckSum + id) & 0xFF;
    
    // send reserve
    Sendcmd[5] = 0;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;

    // send check-sum
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

void _FG_REQ_ERASE_ALL(void)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_ERASE_ALL;
    gFGCmd = cmd;

    gFingerprintState = FG_BUSY;
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    }
    nrf_delay_ms(FINGERPRINT_INT_DELAY);
    fingerprint_uart_init();
    
    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send param
    Sendcmd[4] = 0;
    nCheckSum = (nCheckSum + 0) & 0xFF;
    
    // send reserve
    Sendcmd[5] = 0;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;

    // send check-sum
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

static void fp_blink_led(uint8_t ret)
{
    if (ret == 0) return;
    if (ret == 0xB2)
    {
        reset_uart_array();
        gFingerprintState = FG_IDLE;
        ttp220_return_idle(21000);
    }

    switch (ret)
    {
        case 0xB1://AckCaptureAndExtract
            led_blink(1);
            break;
        case 0xB2:
            #ifdef TETHYS_PWM
            led_pwm_start_1();
            #else
            //led_blink_two_lie(1, 2);
            led_blink(2);
            #endif
            break;
        case 0xB3:
            led_blink(3);
            break;
        case 0xB4:
            led_blink(4);
            break;
        case 0xB5:
            led_blink(5);
            break;
        case 0xB6:
            led_blink(6);
            break;
        case 0xB7:
            led_blink(7);
            break;
        case 0xB8:
            led_blink(8);
            break;
        case 0xC0:
            led_blink(9);
            break;
        case 0xC2:
            led_blink('*');
            break;
        case 0x92://AckMatch1N
            led_blink(0);
            break;
            
        case 0x83://AckEnroll
            led_blink('#');
            break;
        case 0x91:
            led_blink_hang(1);
            break;
        case 0x93:
            led_blink_hang(2);
            break;
        case 0x94:
            led_blink_hang(3);
            break;
            
//        case 0x83://AckEraseOne
//            break;
        case 0x90:
            led_blink_hang(4);
            break;
        case 0xFF:
            led_blink_lie(1);
            break;
        // Belong Tethys 
        case 0x01:
            led_blink_lie(2);
            break;
        case 0x02:
            led_blink_lie(3);
            break;
        case 0x03:
            led_blink_two_lie(1, 2);
            break;
        case 0x04:
            led_blink_two_lie(1, 3);
            break;
        
        default:
            led_blink_two_lie(2, 3);
            break;
    }
}

extern uint8_t g_pstorage_buffer[32];
extern uint16_t gNumber;
extern ble_nus_t m_nus;
void AckAnalyze(uint8_t *pRcvBuf)
{
    uint8_t addr = 0x01;
    uint8_t nCheckSum = 0;
    uint8_t checkSumRecv = 0;
    uint8_t cmd = 0;
    uint8_t reserve = 0;
    uint16_t param = 0;
    uint8_t ret = 0;
    pRcvBuf = data_array;
    // check start code
    if (pRcvBuf[0] != 0x6C)
    {
        addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        fp_blink_led(0x01);
        return;
    }

    // check oem address
    if (pRcvBuf[1] != 0x62)
    {
        addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        fp_blink_led(0x02);
        return;
    }
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // check host address
    if (pRcvBuf[2] != 0x63)
    {
        addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        fp_blink_led(0x03);
        return;
    }
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // check response to the command
    cmd = pRcvBuf[3];
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // check result
    ret = pRcvBuf[4];
    nCheckSum = (nCheckSum + ret) & 0xFF;

    param = pRcvBuf[5];
    nCheckSum = (nCheckSum + param) & 0xFF;

    reserve = pRcvBuf[6];
    nCheckSum = (nCheckSum + reserve) & 0xFF;
    
    // receive check-sum
    if (pRcvBuf[7] != nCheckSum)
    {
        addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        fp_blink_led(0x04);
        return;
    }

    switch (cmd)
    {
    case FG_REQ_CAPTURE_AND_EXTRACT:
        if (ret == 0x00) //
        {
            open_LED(1);
            if (getTTP_state() == TETHYS_SET_BLUETOOTH)
            {
                gFingerprintEnrollState++;
                if (gFingerprintEnrollState >= 3)
                {
                    gFingerprintEnrollState = 0;
                    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_ENROLL);
                }
                else
                {
                    g_pstorage_buffer[0] = 0xEA;
                    g_pstorage_buffer[1] = 0x12;
                    g_pstorage_buffer[2] = gNumber < 10 ? 0x01 : 0x02;
                    g_pstorage_buffer[3] = 0x02;
                    g_pstorage_buffer[4] = gNumber;
                    g_pstorage_buffer[5] = gFingerprintEnrollState;
                    ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
                    gFingerprintState = FG_IDLE;
                }
            }
            else if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_2_1)
            {
                ttp229_reset_timeout_timer();
                gFingerprintEnrollState++;
                if (gFingerprintEnrollState >= 3)
                {
                    gFingerprintEnrollState = 0;
                    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_ENROLL);
                }
                else
                {
                    playLaBa(LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN);
                    gFingerprintState = FG_IDLE;
                }
            }
            else if (getTTP_state() == TETHYS_SET_STATE_1)
            {
                open_LED(3);
                ttp229_reset_timeout_timer();
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_MATCH_1N);
            }
            else // TETHYS_SET_STATE_0
            {
                //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqMatchFinger);
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_MATCH_1N);
            }
        }
        else
        {
            if (ret != 0xB2)
            {
                addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
                app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            }
#if 0
            memcpy(m_beacon_info + BEACON_M_TX_OFFSET, &ret, 1);
            //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
            advertising_init_beacon();
#endif
            fp_blink_led(ret);
        }
        break;

    case FG_REQ_ENROLL:
        if (getTTP_state() == TETHYS_SET_BLUETOOTH)
        {
            if (ret != 0)
            {
                g_pstorage_buffer[0] = 0xEA;
                g_pstorage_buffer[1] = 0x12;
                g_pstorage_buffer[2] = gNumber < 10 ? 0x01 : 0x02;
                g_pstorage_buffer[3] = 0x02;
                g_pstorage_buffer[4] = gNumber;
                g_pstorage_buffer[5] = ret;
                ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
            }
        }
        if (ret == 0x00) //登录成功
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)fingerprint_luru_OK);
        }
        else if (ret == 0x83)//ID错误 (<0 或者 > 最大用户数)或者通信错误
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(1);
        }
        else if (ret == 0x94)//指纹萃取次数 < 3
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(2);
        }
        else if (ret == 0x93)//已经登录的 ID
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(3);
        }
        else if (ret == 0x91)//登录失败 (用户区域已满)
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(4);
        }
        else
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(5);
        }
        fp_blink_led(ret);
        break;
    case FG_REQ_MATCH_1N:
        if (ret != 0x00)
        {
            open_LED(3);
            #if 1
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            #else
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            fingerprint_please_input_again(&addr, 1);
            #endif
            fp_blink_led(ret);
        }
        else if (getTTP_state() == TETHYS_SET_STATE_0)
        {
            gFPIfCanSleep = false;
            open_LED(4);
            uint32_t vOLH[3];
            vOLH[0] = param;
            vOLH[1] = 2;
            vOLH[2] = SecondCountRTC;
            //app_sched_event_put(&vOLH, sizeof(vOLH), moto_open_lock);
            moto_open_lock(&vOLH, sizeof(vOLH));
            app_timer_start(m_Fingerprint_Sleep_time_id, APP_TIMER_TICKS(FP_CAN_SLEEP_TIME, 0), NULL);
        }
        else if (getTTP_state() == TETHYS_SET_STATE_1)
        {
            gFPIfCanSleep = false;
            if (param >= 1 && param <= 9) // If admin
            {
                ttp229_resolve_fingerprint_state_1(ret);
                app_sched_event_put(&addr, 1, (app_sched_event_handler_t)sleep_fingerprint);
                open_LED(4);
            }
            else
            {
                addr = LABA_SHU_RU_YOU_WU;
                app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
                open_LED(5);
            }
            app_timer_start(m_Fingerprint_Sleep_time_id, APP_TIMER_TICKS(FP_CAN_SLEEP_TIME, 0), NULL);
        }
        else
        {
            gFPIfCanSleep = false;
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(6);
            app_timer_start(m_Fingerprint_Sleep_time_id, APP_TIMER_TICKS(FP_CAN_SLEEP_TIME, 0), NULL);
        }
        break;
    
    case FG_REQ_ERASE_ONE:
        if (ret == 0x00)
        {
            open_LED(2);
        }
        else if (ret == 0x83) //ID错误
        {
            open_LED(3);
        }
        else if (ret == 0x90) //未登陆的用户
        {
            open_LED(4);
        }
        else if (ret == 0xFF) //写入ROM错误
        {
            open_LED(5);
        }
        if (gStorageCB)
        {
            gStorageCB(ret);
        }
        gFingerprintState = FG_IDLE;
        fp_blink_led(ret);
        break;
    case FG_REQ_GET_ID_AVAILABILITY:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_CHANGE_BOARD_RATE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_ERASE_ALL:
        if (ret == 0x00)
        {
            open_LED(2);
        }
        else if (ret == 0x90)
        {
            open_LED(3);
        }
        else
        {
            open_LED(4);
        }
        gFingerprintState = FG_IDLE;
        fp_blink_led(ret);
        break;
    case FG_REQ_GET_USER_NUM:
        gFingerprintState = FG_IDLE;
        break;
    default:
        gFingerprintState = FG_IDLE;
        break;
    }
    reset_uart_array();
}

void fingerprint_please_input_again(void *data, uint16_t length)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    uint8_t addr = *((uint8_t *)data);
    playLaBa(addr);
    reset_uart_array();
    gFingerprintState = FG_IDLE;
    ttp220_return_idle(15000);
    sd_nvic_critical_region_exit(is_nested_critical_region);
    //LEDS_OFF(BSP_LED_3_MASK);
}

void fingerprint_luru_OK(void)
{
    tethys_man_t aman;
    memset(&aman, 0xFF, sizeof(aman));
    if (getTTP_state() == TETHYS_SET_BLUETOOTH)
    {
        aman.admin = gNumber < 10 ? 0 : 1;
    }
    else if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
             getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1)
    {
        aman.admin = 0;
    }
    else
    {
        aman.admin = 1;
    }
    aman.mode = 1;
    aman.id = getCurID();
    //memcpy(aman.content, gInput, 6);

    if (getTTP_state() == TETHYS_SET_BLUETOOTH)
    {
        g_pstorage_buffer[0] = 0xEA;
        g_pstorage_buffer[1] = 0x12;
        g_pstorage_buffer[2] = gNumber < 10 ? 0x01 : 0x02;
        g_pstorage_buffer[3] = 0x02;
        g_pstorage_buffer[4] = gNumber;
        g_pstorage_buffer[5] = 0x03;
        ble_nus_string_send(&m_nus, g_pstorage_buffer, 6);
        uint8_t streams[16] = {LABA_LURU_CHENGGONG, 90};
        playLaBaMany(streams, 1);
    }
    else if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
             getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1)
    {
        uint8_t streams[16] = {LABA_LURU_CHENGGONG, 90};
        playLaBaMany(streams, 1);
    }
    else
    {
        uint8_t streams[16] = {LABA_XIUGAI_CHENGGONG, 90};
        playLaBaMany(streams, 1);
    }
    
    tethys_storage_save(&aman);
    if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1)
    {
        ttp229_return_re_enter_luru(0, 3000);
    }
    else if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1)
    {
        ttp229_return_re_enter_luru(1, 3000);
    }
    else
    {
        ttp220_return_idle(5000);
        setTTP_state(TETHYS_SET_STATE_0);
    }
    gFingerprintState = FG_IDLE;
}
