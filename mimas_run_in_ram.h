#ifndef __MIMAS_RUN_IN_RAM_H__
#define __MIMAS_RUN_IN_RAM_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>
#include "nrf_drv_gpiote.h"

#define BEACON_UUID_OFFSET 2
#define BEACON_MAJOR_OFFSET 18
#define BEACON_MINOR_OFFSET 20
#define BEACON_TEMP_OFFSET 21
#define BEACON_M_TX_OFFSET 22
#define BEACON_BINDING_OFFSET 22
#define BEACON_BATTERY_OFFSET 23
#define BEACON_STATE_OFFSET 23


#define APP_BEACON_INFO_LENGTH          0x18                              /**< Total length of information advertised by the Beacon. */
#define APP_ADV_DATA_LENGTH             0x15                              /**< Length of manufacturer specific data in the advertisement. */
#define APP_DEVICE_TYPE                 0x02                              /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xc5                              /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#if 0
#define APP_COMPANY_IDENTIFIER          0x0059                            /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#else
#define APP_COMPANY_IDENTIFIER          0x004C
#endif
#define APP_MAJOR_VALUE                 0x27, 0x49                        /**< Major value used to identify Beacons. */
#define APP_MINOR_VALUE                 0x2B, 0x05                        /**< Minor value used to identify Beacons. */
#define APP_BEACON_UUID                 0xFD, 0xA5, 0x06, 0x93, \
                                        0xA4, 0xE2, 0x4F, 0xB1, \
                                        0xAF, 0xCF, 0xC6, 0xEB, \
                                        0x07, 0x64, 0x78, 0x25            /**< Proprietary UUID for Beacon. */
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void mimas_loop(void);
extern void ADXL362_int1_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
extern void ADXL362_int2_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
extern void ADXL362_int2_pin_handler_toggle(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
extern void advertising_init_beacon(void);

#endif /* __MIMAS_RUN_IN_RAM_H__ */
