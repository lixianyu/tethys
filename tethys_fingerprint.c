/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_fingerprint.h"
#include "tethys_laba.h"
#include "tethys_led.h"
#include "tethys_ttp229bsf.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"

typedef enum {
    FG_IDLE = 0,
    FG_BUSY,
}FG_state_t;
//Define cmd code
#define FG_REQ_CAPTURE_AND_EXTRACT 0x51
#define FG_REQ_ENROLL_USER 0x52
#define FG_REQ_MATCH_FINGER 0x53
#define FG_REQ_DELETE_USER 0x54
#define FG_REQ_GET_USER_NUM 0x55
//V2.0 begins here.
#define FG_REQ_ENROLL 0x70
#define FG_REQ_MATCH_1N 0x71
#define FG_REQ_MATCH_11 0x72
#define FG_REQ_ERASE_ONE 0x73
#define FG_REQ_GET_ID_AVAILABILITY 0x74
#define FG_REQ_GET_USER_INFORMATION 0x75
#define FG_REQ_SET_USER_INFORMATION 0X76
#define FG_REQ_GET_ENROLLED_FP_FEATURE 0x77
#define FG_REQ_SET_ENROLLED_FP_FEATURE 0X78
#define FG_REQ_GET_EXTRACTED_FP_FEATURE 0X79
#define FG_REQ_SET_EXTRACTED_FP_FEATURE 0X7A
#define FG_REQ_CAPTURE_AND_CHECK 0x10
#define FG_REQ_RECEIVE_FP_IMAGE 0xD0
#define FG_REQ_SEND_FP_IMAGE_AND_EXTRACT 0x7B
#define FG_REQ_SET_OEM_INFORMATION 0x7C
#define FG_REQ_GET_OEM_INFORMATION 0x7D
#define FG_REQ_CHANGE_BOARD_RATE 0x7E
extern uint8_t m_beacon_info[];

extern void ReqCaptureAndExtract(uint8_t param);
extern void ReqMatchFinger(void);
extern void fingerprint_please_input_again(void *data, uint16_t length);
extern void fingerprint_had_open_the_lock(void);
extern void AckAnalyze(uint8_t *pRcvBuf);
void _FG_REQ_MATCH_1N(void);
void fingerprint_uart_init(void);
void fingerprint_luru_OK(void);

#define FINGERPRINT_UART_RX_BUF_SIZE    64
#define FINGERPRINT_UART_TX_BUF_SIZE    64
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
static FG_state_t gFingerprintState = FG_IDLE;
static uint8_t gFingerprintEnrollState = 0;
APP_TIMER_DEF(m_Fingerprint_time_id);
static uint8_t gFGCmd;
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void testFingerprint(void)
{

}
//#define PIN_IS_ON(pins_mask) ((pins_mask) & (NRF_GPIO->OUT ^ LEDS_INV_MASK) )
__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}

static void Fingerprint_detect_pin_handler_LOWTOHIGH(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    static uint8_t flag = 0;
    if (flag == 0)
    {
        flag = 1;
        open_LED(9);
    }
    else
    {
        open_LED('*');
        flag = 0;
    }
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        gFingerprintState = FG_BUSY;
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
        app_timer_start(m_Fingerprint_time_id, APP_TIMER_TICKS(300, 0), NULL);
    }
    else
    {
        if (gFingerprintState == FG_IDLE)
        {
            gFingerprintState = FG_BUSY;
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqCaptureAndExtract);
        }
    }
}

static void preReqCaptureAndExtract(void)
{
    fingerprint_uart_init();
    ReqCaptureAndExtract(0xFF);
}

static void fingerprint_timer_handler(void *p_context)
{
    #if 0
    fingerprint_uart_init();
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqCaptureAndExtract);
    #else
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)preReqCaptureAndExtract);
    #endif
}

static uint8_t index = 0;
static uint8_t data_array[16];
static uint8_t len_uart = 6;
__STATIC_INLINE void reset_uart_array(void)
{
    len_uart = 6;
    index = 0;
    memset(data_array, 0, sizeof(data_array));
}

void fingerprint_uart_handle(app_uart_evt_t *p_event)
{
    uint32_t err_code;

    switch (p_event->evt_type)
    {
    case APP_UART_DATA_READY:
        //UNUSED_VARIABLE(app_uart_get(&data_array[index]));
        app_uart_get(&data_array[index]);
        index++;
        if (index >= len_uart)
        {
#if 0
            memcpy(m_beacon_info + BEACON_UUID_OFFSET, data_array, 7);
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
#endif
            if (gFGCmd == FG_REQ_MATCH_1N)
            {
                if (data_array[4] != 0x00)
                {
                    index = 0;
                    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)AckAnalyze);
                }
                else
                {
                    if (index == 6)
                    {
                        len_uart = 8;
                    }
                    else
                    {
                        index = 0;
                        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)AckAnalyze);
                    }
                }
            }
            else
            {
                index = 0;
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)AckAnalyze);
            }
        }
        break;

    case APP_UART_COMMUNICATION_ERROR:
        //APP_ERROR_HANDLER(p_event->data.error_communication);
        playLaBa(LABA_2);
        //fingerprint_uart_init();
        break;

    case APP_UART_FIFO_ERROR:
        //APP_ERROR_HANDLER(p_event->data.error_code);
        playLaBa(LABA_3);
        break;

    default:
        break;
    }
}

void fingerprint_uart_init(void)
{
    uint32_t err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        FINGERPRINT_RX_PIN,
        FINGERPRINT_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT( &comm_params,
                        FINGERPRINT_UART_RX_BUF_SIZE,
                        FINGERPRINT_UART_TX_BUF_SIZE,
                        fingerprint_uart_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
    //APP_ERROR_CHECK(err_code);
#if 0
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}

#if 0
void init_Fingerprint(void)
{
    nrf_gpio_cfg_output(FINGERPRINT_PW_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_EN_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_PW_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);

    nrf_gpio_cfg_output(FINGERPRINT_DETECT_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_PLUG_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_TX_PIN);
    nrf_gpio_cfg_output(FINGERPRINT_RX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_TX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_RX_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_DETECT_PIN);
    nrf_gpio_pin_clear(FINGERPRINT_PLUG_PIN);

    nrf_delay_ms(2000);
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    nrf_gpio_cfg_input(FINGERPRINT_DETECT_PIN, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(FINGERPRINT_PLUG_PIN, NRF_GPIO_PIN_NOPULL);
    nrf_delay_ms(300);
    nrf_gpio_pin_set(FINGERPRINT_PW_PIN);
    fingerprint_uart_init();
#if 1
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    err_code = nrf_drv_gpiote_in_init(FINGERPRINT_DETECT_PIN, &in_config, Fingerprint_detect_pin_handler_LOWTOHIGH);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(FINGERPRINT_DETECT_PIN, true);
#endif

    app_timer_create(&m_lock_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     lock_timeout_handler);
}
#else
void init_Fingerprint(void)
{
    gFingerprintState = FG_IDLE;
#if 0
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    nrf_delay_ms(300);
    fingerprint_uart_init();
#endif
#if 1
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    //in_config.pull = NRF_GPIO_PIN_PULLUP;
    in_config.sense = NRF_GPIOTE_POLARITY_LOTOHI;
    err_code = nrf_drv_gpiote_in_init(FINGERPRINT_DETECT_PIN, &in_config, Fingerprint_detect_pin_handler_LOWTOHIGH);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(FINGERPRINT_DETECT_PIN, true);
#endif
    app_timer_create(&m_Fingerprint_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     fingerprint_timer_handler);
#if 0
    memcpy(m_beacon_info + BEACON_UUID_OFFSET, &gTethys, 4);
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    advertising_init_beacon();
#endif
}
#endif

void start_fingerprint(void)
{
#if 0
    NRF_UART0->ENABLE = 1;
    NRF_UART0->TASKS_STARTTX = 1;
    NRF_UART0->TASKS_STARTRX = 1;
#endif
    nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    nrf_delay_ms(300);
    fingerprint_uart_init();
}

void sleep_fingerprint(void)
{
    app_uart_close();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
    nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);
    gFingerprintState = FG_IDLE;
}

static void fingerprint_uart_send_data(uint8_t *p_data, uint8_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
}

//FG_REQ_CAPTURE_AND_EXTRACT
void ReqCaptureAndExtract(uint8_t param)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_CAPTURE_AND_EXTRACT;
    gFGCmd = cmd;

    //playLaBa(LABA_JIN_RU_CHU_SHI_HUA);
    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send index (optional)
    if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_2_1)
    {
        Sendcmd[4] = gFingerprintEnrollState;
    }
    else
    {
        Sendcmd[4] = 0;
    }
    nCheckSum = (nCheckSum + Sendcmd[4]) & 0xFF;
    // send check-sum
    Sendcmd[5] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 6);
}

//FG_REQ_ENROLL_USER
void ReqEnrollUser(void)
{
}

//FG_REQ_MATCH_FINGER
void ReqMatchFinger(void)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_MATCH_FINGER;
    gFGCmd = cmd;

    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;
#if 0
    // send index (optional)
    Sendcmd[4] = 0;
    nCheckSum = (nCheckSum + 0) & 0xFF;
#endif
    // send check-sum
    Sendcmd[4] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 5);
}

//FG_REQ_DELETE_USER
void ReqDeleteUser(void)
{
}

//FG_REQ_GET_USER_NUM
void ReqGetUserNum(void)
{
}

void _FG_REQ_ENROLL(void)
{
    uint8_t Sendcmd[64] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_ENROLL;
    gFGCmd = cmd;

    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // send index (optional)
#if 0    
    uint16_t curID = getCurID();
    uint16_t *pID = (uint16_t*)&Sendcmd[4];
    *pID = curID;
#else
    uint16_t curID = getCurID();
    Sendcmd[4] = curID >> 8;
    Sendcmd[5] = curID & 0x00FF;
#endif
    nCheckSum = (nCheckSum + Sendcmd[4]) & 0xFF;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;
    memset(&Sendcmd[6], 0x00, 32);

    // send check-sum
    Sendcmd[38] = nCheckSum;

    nrf_delay_ms(300);
    fingerprint_uart_send_data(Sendcmd, 39);
}

void _FG_REQ_MATCH_1N(void)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_MATCH_1N;
    gFGCmd = cmd;

    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;
#if 0
    // send index (optional)
    Sendcmd[4] = 0;
    nCheckSum = (nCheckSum + 0) & 0xFF;
#endif
    // send check-sum
    Sendcmd[4] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 5);
}

delete_one_fingerprint_cb gStorageCB;
void _FG_REQ_ERASE_ONE(uint16_t id, delete_one_fingerprint_cb callback)
{
    uint8_t Sendcmd[16] = {0};
    uint8_t nCheckSum = 0;
    uint8_t cmd = FG_REQ_ERASE_ONE;
    gFGCmd = cmd;
    gStorageCB = callback;

    gFingerprintState = FG_BUSY;
    if (0 == nrf_gpio_pin_state(FINGERPRINT_EN_PIN))
    {
        nrf_gpio_pin_set(FINGERPRINT_EN_PIN);
    }
    nrf_delay_ms(300);
    fingerprint_uart_init();
    
    // start code
    Sendcmd[0] = 0x6C;

    // host address send
    Sendcmd[1] = 0x63;
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // oem address send
    Sendcmd[2] = 0x62;
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // send command code
    Sendcmd[3] = cmd;
    nCheckSum = (nCheckSum + cmd) & 0xFF;
#if 1
    // send index (optional)
    Sendcmd[4] = id >> 8;
    Sendcmd[5] = id & 0x00FF;
    nCheckSum = (nCheckSum + Sendcmd[4]) & 0xFF;
    nCheckSum = (nCheckSum + Sendcmd[5]) & 0xFF;
#endif
    // send check-sum
    Sendcmd[6] = nCheckSum;

    fingerprint_uart_send_data(Sendcmd, 7);
}

void AckAnalyze(uint8_t *pRcvBuf)
{
    uint8_t addr = 0x01;
    uint8_t nCheckSum = 0;
    uint8_t checkSumRecv = 0;
    uint8_t cmd = 0;
    uint16_t param = 0;
    pRcvBuf = data_array;
    // check start code
    if (pRcvBuf[0] != 0x6C)
    {
        addr = LABA_AN_HUICHEJIAN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        return;
    }

    // check oem address
    if (pRcvBuf[1] != 0x62)
    {
        addr = LABA_QING_SHU_RU_ZHI_WEN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        return;
    }
    nCheckSum = (nCheckSum + 0x62) & 0xFF;

    // check host address
    if (pRcvBuf[2] != 0x63)
    {
        addr = LABA_QING_BA_KA_PIAN_KAO_JIN;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        return;
    }
    nCheckSum = (nCheckSum + 0x63) & 0xFF;

    // check response to the command
    cmd = pRcvBuf[3];
    nCheckSum = (nCheckSum + cmd) & 0xFF;

    // check result
    uint8_t ret = pRcvBuf[4];
    nCheckSum = (nCheckSum + ret) & 0xFF;

    // check parameter
    if (gFGCmd == FG_REQ_MATCH_1N)
    {
        if (ret == 0x00)
        {
            //param = (uint16_t)(pRcvBuf[5] | pRcvBuf[6]);
            param = pRcvBuf[6]; // we got the ID.
            nCheckSum = (nCheckSum + pRcvBuf[5]) & 0xFF;
            nCheckSum = (nCheckSum + pRcvBuf[6]) & 0xFF;
            checkSumRecv = pRcvBuf[7];
        }
        else
        {
            checkSumRecv = pRcvBuf[5];
        }
        if (cmd == FG_REQ_MATCH_1N)
        {
            open_LED(2);
        }
    }
    else
    {
        checkSumRecv = pRcvBuf[5];
    }

    // receive check-sum
    if (checkSumRecv != nCheckSum)
    {
        addr = LABA_QING_SHU_RU_KAI_SUO_XIN_XI;
        app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        return;
    }

    switch (cmd)
    {
    case FG_REQ_CAPTURE_AND_EXTRACT:
        if (ret == 0x00) //
        {
            open_LED(1);
            if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1 ||
                getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_2_1)
            {
                ttp229_reset_timeout_timer();
                gFingerprintEnrollState++;
                if (gFingerprintEnrollState >= 5)
                {
                    gFingerprintEnrollState = 0;
                    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_ENROLL);
                }
                else
                {
                    playLaBa(LABA_QING_ZAI_CI_SHU_RU_ZHI_WEN);
                    gFingerprintState = FG_IDLE;
                }
            }
            else if (getTTP_state() == TETHYS_SET_STATE_1)
            {
                open_LED(3);
                ttp229_reset_timeout_timer();
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_MATCH_1N);
            }
            else // TETHYS_SET_STATE_0
            {
                //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)ReqMatchFinger);
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)_FG_REQ_MATCH_1N);
            }
        }
        else
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        }
        break;
    case FG_REQ_ENROLL_USER:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_MATCH_FINGER:
        if (ret == 0x00)// OMD, we got the right fingerprint!!!
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)fingerprint_had_open_the_lock);
        }
        else if (ret == 0x90)
        {
            addr = LABA_JIN_RU_CHU_SHI_HUA;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        }
        else if (ret == 0x92)
        {
            open_LED(2);
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        }
        else
        {
            addr = LABA_DING_DONG;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
        }
        break;
    case FG_REQ_DELETE_USER:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_USER_NUM:
        gFingerprintState = FG_IDLE;
        break;

    //V2.0 begins here.
    case FG_REQ_ENROLL:
        if (ret == 0x00) //登录成功
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)fingerprint_luru_OK);
        }
        else if (ret == 0x83)//ID错误 (<0 或者 > 最大用户数)或者通信错误
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(1);
        }
        else if (ret == 0x94)//指纹萃取次数 < 5
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(2);
        }
        else if (ret == 0x93)//已经登录的 ID
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(3);
        }
        else if (ret == 0x91)//登录失败 (用户区域已满)
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(4);
        }
        else
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(5);
        }
        break;
    case FG_REQ_MATCH_1N:
        if (ret != 0x00)
        {
            open_LED(2);
            #if 1
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            #else
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            fingerprint_please_input_again(&addr, 1);
            #endif
        }
        else if (getTTP_state() == TETHYS_SET_STATE_0)
        {
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)fingerprint_had_open_the_lock);
            open_LED(3);
        }
        else if (getTTP_state() == TETHYS_SET_STATE_1)
        {
            if (param >= 1 && param <= 9) // If admin
            {
                ttp229_resolve_fingerprint_state_1(ret);
                app_sched_event_put(&addr, 1, (app_sched_event_handler_t)sleep_fingerprint);
                open_LED(4);
            }
            else
            {
                addr = LABA_SHU_RU_YOU_WU;
                app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
                open_LED(5);
            }
        }
        else
        {
            addr = LABA_QING_CHONG_XIN_SHU_RU_ZHI_WEN;
            app_sched_event_put(&addr, 1, (app_sched_event_handler_t)fingerprint_please_input_again);
            open_LED(6);
        }
        break;
    case FG_REQ_MATCH_11:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_ERASE_ONE:
        if (ret == 0x00)
        {
            open_LED(2);
        }
        else if (ret == 0x83) //ID错误
        {
            open_LED(3);
        }
        else if (ret == 0x90) //未登陆的用户
        {
            open_LED(4);
        }
        else if (ret == 0xFF) //写入ROM错误
        {
            open_LED(5);
        }
        if (gStorageCB)
        {
            gStorageCB(ret);
        }
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_ID_AVAILABILITY:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_USER_INFORMATION:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_SET_USER_INFORMATION:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_ENROLLED_FP_FEATURE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_SET_ENROLLED_FP_FEATURE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_EXTRACTED_FP_FEATURE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_SET_EXTRACTED_FP_FEATURE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_CAPTURE_AND_CHECK:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_RECEIVE_FP_IMAGE:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_SEND_FP_IMAGE_AND_EXTRACT:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_SET_OEM_INFORMATION:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_GET_OEM_INFORMATION:
        gFingerprintState = FG_IDLE;
        break;
    case FG_REQ_CHANGE_BOARD_RATE:
        gFingerprintState = FG_IDLE;
        break;
    default:
        gFingerprintState = FG_IDLE;
        break;
    }
    reset_uart_array();
}

void fingerprint_please_input_again(void *data, uint16_t length)
{
    uint8_t is_nested_critical_region;
    sd_nvic_critical_region_enter(&is_nested_critical_region);
    uint8_t addr = *((uint8_t *)data);
    playLaBa(addr);
    reset_uart_array();
    gFingerprintState = FG_IDLE;
    ttp220_return_idle(15000);
    sd_nvic_critical_region_exit(is_nested_critical_region);
    //LEDS_OFF(BSP_LED_3_MASK);
}

void fingerprint_had_open_the_lock(void)
{
    //nrf_gpio_pin_clear(FINGERPRINT_EN_PIN);
    //LEDS_OFF(BSP_LED_3_MASK);
    moto_open_lock();
}

void fingerprint_luru_OK(void)
{
    tethys_man_t aman;
    memset(&aman, 0xFF, sizeof(aman));
    if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
        getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_2_1)
    {
        aman.admin = 0;
    }
    else
    {
        aman.admin = 1;
    }
    aman.mode = 1;
    aman.id = getCurID();
    //memcpy(aman.content, gInput, 6);

    if (getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_1_1_1 ||
        getTTP_state() == TETHYS_SET_STATE_SETTING_MAIN_2_1_1)
    {
        uint8_t streams[16] = {LABA_LURU_CHENGGONG, 90};
        playLaBaMany(streams, 1);
    }
    else
    {
        uint8_t streams[16] = {LABA_XIUGAI_CHENGGONG, 90};
        playLaBaMany(streams, 1);
    }
    
    tethys_storage_save(&aman);
    ttp220_return_idle(5000);
    setTTP_state(TETHYS_SET_STATE_0);
    gFingerprintState = FG_IDLE;
}
