基于nRF51 SDK v10.0.0；
Base ble_app_proximity
----------------------------------------------------------------------------------------------------
If there is a bootloader:
bootloader start address : 3C000, and size is 16KB
// swap
nrfjprog --memrd 0x0003BC00 --n 1024
// g_storage_license_handle
nrfjprog --memrd 0x0003B800 --n 1024
// g_storage_open_lock_times_handle
nrfjprog --memrd 0x0003B400 --n 1024
// g_storage_Elara_handle
nrfjprog --memrd 0x0003B000 --n 1024

// g_storage_lock_handle
nrfjprog --memrd 0x0003AC00 --n 1024

// g_storage_lock_handle0
nrfjprog --memrd 0x0003A800 --n 1024

// g_storage_handle_system_time
nrfjprog --memrd 0x0003A400 --n 1024

// g_storage_handle_open_lock_history_pos
nrfjprog --memrd 0x0003A000 --n 1024

// g_storage_handle_open_lock_history
nrfjprog --memrd 0x00039C00 --n 1024

// g_storage_lock_handle5
nrfjprog --memrd 0x00039800 --n 1024
// g_storage_lock_handle4
nrfjprog --memrd 0x00039400 --n 1024
// g_storage_lock_handle3
nrfjprog --memrd 0x00039000 --n 1024
// g_storage_lock_handle2
nrfjprog --memrd 0x00038C00 --n 1024
// g_storage_lock_handle1
nrfjprog --memrd 0x00038800 --n 1024
// g_storage_handle
nrfjprog --memrd 0x00038400 --n 1024
// Device manager: m_storage_handle
nrfjprog --memrd 0x00038000 --n 1024

/////////////////////////////////////////////////////////////////////////////////////////////////////
If there is no bootloader:
nrfjprog --memrd 0x0003FC00 --n 1024 // swap
nrfjprog --memrd 0x0003F800 --n 1024 // g_storage_lock_handle
nrfjprog --memrd 0x0003F400 --n 1024 // g_storage_lock_handle0
nrfjprog --memrd 0x0003F000 --n 1024 // g_storage_handle_system_time
nrfjprog --memrd 0x0003EC00 --n 1024 // g_storage_handle_open_lock_history_pos
nrfjprog --memrd 0x0003E800 --n 1024 // g_storage_handle_open_lock_history
nrfjprog --memrd 0x0003E400 --n 1024 // g_storage_lock_handle5
nrfjprog --memrd 0x0003E000 --n 1024 // g_storage_lock_handle4
nrfjprog --memrd 0x0003DC00 --n 1024 // g_storage_lock_handle3
nrfjprog --memrd 0x0003D800 --n 1024 // g_storage_lock_handle2
nrfjprog --memrd 0x0003D400 --n 1024 // g_storage_lock_handle1
nrfjprog --memrd 0x0003D000 --n 1024 // g_storage_handle
nrfjprog --memrd 0x0003CC00 --n 1024 // Device manager: m_storage_handle

----------------------------------------------------------------------------------------------------
说明：
* 未注册的Tethys，只能开锁100次；
*
----------------------------------------------------------------------------------------------------
一些调试用的蓝牙MAC地址：
0xEAD50CA2DEB6
0xE98516EE211E
0xD8E507551E04
----------------------------------------------------------------------------------------------------
1675mv
1297mv
----------------------------------------------------------------------------------------------------
FDA50693-A4E2-4FB1-AFCF-C6EB07647825
FDA50693-A4E2-4FB1-AFCF-C6EB07647825
----------------------------------------------------------------------------------------------------
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 1 Tethys_0.0.826.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 2 Tethys_0.0.826.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 3 Tethys_0.0.831.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 4 Tethys_0.0.902.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 5 Tethys_0.0.903.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 6 Tethys_0.0.916.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 7 Tethys_0.0.919.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 8 Tethys_0.0.923.02.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 9 Tethys_0.0.A08.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 10 Tethys_0.0.A17.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 11 Tethys_0.0.A24.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 12 Tethys_0.0.A24.04.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 13 Tethys_0.0.A24.05.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 14 Tethys_0.0.A24.06.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 15 Tethys_0.0.A25.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 16 Tethys_0.0.1027.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 17 Tethys_0.0.1027.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 18 Tethys_0.0.1030.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 19 Tethys_0.0.1030.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 19 Tethys_0.0.1030.03.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 20 Tethys_0.0.1031.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 21 Tethys_0.0.1031.05.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 22 Tethys_0.0.1101.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 23 Tethys_0.0.1103.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 24 Tethys_0.0.1104.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 25 Tethys_0.0.1105.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 26 Tethys_0.0.1108.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 27 Tethys_0.0.1108.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 28 Tethys_0.0.1109.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 29 Tethys_0.0.1111.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 29 Tethys_0.0.1111.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 30 Tethys_0.0.1111.02.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 31 Tethys_0.0.1112.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 32 Tethys_0.0.1115.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 33 Tethys_0.0.1115.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 33 Tethys_0.0.1118.02.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 34 Tethys_0.0.1120.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 35 Tethys_0.0.1121.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 36 Tethys_0.0.1122.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 37 Tethys_0.0.1122.01.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 38 Tethys_0.0.1123.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 39 Tethys_0.0.1124.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 40 Tethys_0.0.1226.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 41 Tethys_0.0.1228.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 42 Tethys_0.1.0106.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 43 Tethys_0.1.0108.00.DB964279CC78.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 44 Tethys_0.1.0109.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 45 Tethys_0.1.0113.00.zip
nrfutil dfu genpkg --application Tethys_nrf51822_xxac_s110.hex --application-version 46 Tethys_0.1.0205.00.zip